@file:Suppress("DEPRECATION")

package com.panky.newsincards.Utils

import android.content.Context
import android.net.ConnectivityManager

object ConnectionStatus {

    private var cm: ConnectivityManager? = null

    fun CheckInternetConnection(context: Context?): Boolean {

        cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val netInfoMobile = cm!!.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        val netInfoWifi = cm!!.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

        return if (netInfoMobile != null && netInfoMobile.isConnectedOrConnecting() || netInfoWifi != null && netInfoWifi.isConnectedOrConnecting()) {
            true

        } else cm!!.activeNetworkInfo != null && cm!!.activeNetworkInfo.isAvailable
                && cm!!.activeNetworkInfo.isConnected
    }
}
