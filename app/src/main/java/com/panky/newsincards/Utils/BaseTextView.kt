package com.panky.newsincards.Utils

import android.content.Context
import android.widget.TextView
import com.panky.newsincards.SharedPreference.SharedPrefs

class BaseTextView(val mContext:Context) : TextView(mContext) {

    private var sharedPrefs: SharedPrefs? = null

    init {
        sharedPrefs = SharedPrefs(mContext)
    }
    override fun setTextSize(size: Float) {

//        size = sharedPrefs!!.MainGuiderShow
        super.setTextSize(size)
    }
}