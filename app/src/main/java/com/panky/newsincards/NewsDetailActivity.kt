package com.panky.newsincards

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.Utility.Utils

class NewsDetailActivity : AppCompatActivity(), AppBarLayout.OnOffsetChangedListener {

    private var newsIMgView: ImageView? = null
    private var appBar_title: TextView? = null
    private var appBar_subtitle: TextView? = null
    private var date: TextView? = null
    private var time: TextView? = null
    private var title: TextView? = null
    private val isHideToolbarView = true
    private var date_behaviour: FrameLayout? = null
    private var titleAppBar: LinearLayout? = null
    private var appBarLayout: AppBarLayout? = null
    private var mToolbar: Toolbar? = null
    private var mUrl: String? = null
    private var mImg: String? = null
    private var mTitle: String? = null
    private var mDate: String? = null
    private var mSourceName: String? = null
    private var mAuthor: String? = null
    private var sharedPrefs: SharedPrefs? = null


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        sharedPrefs = SharedPrefs(this@NewsDetailActivity)

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            val window = window
//            requestWindowFeature(Window.FEATURE_NO_TITLE)
////            this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN)
//
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
////            window.statusBarColor = Color.parseColor("#88adadad")
//        }

        if (sharedPrefs!!.isNightModeEnabled) {
            setTheme(R.style.AppTheme_Base_Night)
        } else {
            setTheme(R.style.AppTheme_Base_Light)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)

        initViews()
        setSupportActionBar(mToolbar)
        if (supportActionBar != null) {
            supportActionBar!!.title = ""
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        val collapsingToolbarLayout = findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar)
        collapsingToolbarLayout.title = ""

        appBarLayout!!.addOnOffsetChangedListener(this)
        date_behaviour!!.visibility = View.VISIBLE

        val getIntent = intent
        mUrl = getIntent.getStringExtra("url")
        mImg = getIntent.getStringExtra("news_img")
        mTitle = getIntent.getStringExtra("title")
        mDate = getIntent.getStringExtra("date")
        mSourceName = getIntent.getStringExtra("source_name")
        mAuthor = getIntent.getStringExtra("author")

        val utils = Utils(this@NewsDetailActivity)
        val requestOptions = RequestOptions()
        requestOptions.error(utils.randomDrawbleColor)

        Glide.with(this)
                .load(mImg)
                .apply(requestOptions)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(newsIMgView!!)

        appBar_title!!.text = mSourceName
        appBar_subtitle!!.text = mUrl
        date!!.text = utils.DateFormat(mDate.toString())
        title!!.text = mTitle

        Log.e("mAuth, mDate", " :: $mAuthor, :: $mDate")

        var author: String? = null
        if (mAuthor != null && mAuthor == "") {
            author = " \u2022 " + mAuthor!!
        } else {
            author = ""
        }

        time!!.text = mSourceName + author + " \u2022  " + utils.DateToTimeFormat(mDate.toString())
        initWebView(mUrl)
    }

    private fun initViews() {
        mToolbar = findViewById(R.id.toolbar)
        appBarLayout = findViewById(R.id.appbarLayout)
        date_behaviour = findViewById(R.id.date_behavior)
        titleAppBar = findViewById(R.id.title_appbar)
        appBar_title = findViewById(R.id.title_on_appbar)
        appBar_subtitle = findViewById(R.id.subtitle_on_appbar)
        date = findViewById(R.id.date)
        time = findViewById(R.id.time)
        title = findViewById(R.id.title)

        newsIMgView = findViewById(R.id.backdrop)
    }

    private fun initWebView(url: String?) {
        val webView = findViewById<WebView>(R.id.webView)
        webView.settings.loadsImagesAutomatically = true
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        webView.settings.setSupportZoom(true)
        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = false
        webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webView.webViewClient = WebViewClient()
        webView.loadUrl(url)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_news, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.shareNews -> try {
                val i = Intent(Intent.ACTION_SEND)
                i.type = "text/plain"
                i.putExtra(Intent.EXTRA_SUBJECT, mSourceName)
                val body = "$mTitle\n$mUrl\n\nShare from News In Card App"
                i.putExtra(Intent.EXTRA_TEXT, body)
                startActivity(Intent.createChooser(i, "Share News with :- "))
            } catch (e: Exception) {
                Toast.makeText(this@NewsDetailActivity, "Hmm...  Sorry \n Cannot be Share", Toast.LENGTH_SHORT).show()
            }

            R.id.webOptions -> {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(mUrl)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        supportFinishAfterTransition()
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffSet: Int) {

        val maxScroll = appBarLayout.totalScrollRange
        val percentage = Math.abs(verticalOffSet).toFloat() / maxScroll.toFloat()
        Log.e("onOffSet value", " :: $maxScroll, per :: $percentage")
        Log.e("isHideToolbar", " :: $isHideToolbarView")

        if (percentage == 1f && isHideToolbarView) {
            date_behaviour!!.visibility = View.GONE
            titleAppBar!!.visibility = View.VISIBLE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = window
//                requestWindowFeature(Window.FEATURE_NO_TITLE)
//            this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN)

                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//            window.statusBarColor = Color.parseColor("#88adadad")
            }

            //            isHideToolbarView = !isHideToolbarView;
        } else if (percentage < 1f && isHideToolbarView) {
            date_behaviour!!.visibility = View.VISIBLE
            titleAppBar!!.visibility = View.GONE

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = window
//                requestWindowFeature(Window.FEATURE_NO_TITLE)
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN)

//                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//            window.statusBarColor = Color.parseColor("#88adadad")
            }
            //            isHideToolbarView = !isHideToolbarView;
        }
    }
}
