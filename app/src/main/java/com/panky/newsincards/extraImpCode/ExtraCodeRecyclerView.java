package com.panky.newsincards.extraImpCode;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;

//import com.panky.newspro.Adapter.NewsAdapter;
//import com.panky.newsincards.FavouriteActivity;
//import com.panky.newsincards.view.MainPage.view.MainActivity;
//import com.panky.newsincards.SharedPreference.SharedPrefs;
//import com.panky.newsincards.Utility.Utils;
//import com.panky.newspro.models.Article;
//import com.panky.newspro.models.News;

//import cn.pedant.SweetAlert.SweetAlertDialog;


public class ExtraCodeRecyclerView {}
    /*
    package com.panky.newspro;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.panky.newspro.Adapter.NewsAdapter;
import com.panky.newspro.Adapter.NewsSwipeStackAdapter;
import com.panky.newspro.Constants.Constant;
import com.panky.newspro.DBHelper.NewsDbHelper;
import com.panky.newspro.SharedPreference.SharedPrefs;
import com.panky.newspro.Utility.Utils;
import com.panky.newspro.api.ApiClient;
import com.panky.newspro.api.ApiInterface;
import com.panky.newspro.models.Article;
import com.panky.newspro.models.News;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import link.fls.swipestack.SwipeStack;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;*/

/*public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, Constant

    {

        private String TAG = MainActivity.class.getSimpleName();
        private RecyclerView mRecyclerView newsCategoryRV;
        private RecyclerView.LayoutManager layoutManager, layoutManagerCategory;
        private List<Article> articleList;
        private NewsAdapter newsAdapter;
        private MainActivity.NewsCategoryAdapter categoryAdapter;

    private SwipeRefreshLayout swipeRefreshLayout;
        private TextView topHeadLine;
//        private RelativeLayout errorLayout;
//        private ImageView errorImageView;
//        private TextView errorTitle, errorMessage, tvdataNotFound, tvCountFavNews;
//        private Button retryButton, loadMoreButton;
//        private List<String> categoryList;

        private SweetAlertDialog pDialog;

//    Shared preference object created here....
        private SharedPrefs sharedPrefs;
        private NewsDbHelper dbHelper;
        private static int displayedposition = 0;
        private FloatingActionButton fabSwipeLeft, fabSwipeRight;

        private SwipeStack swipeStackLoader;
        private NewsSwipeStackAdapter swipeStackAdapter;
        private CoordinatorLayout mainNewsCoLayt;
        private NewsDbHelper newsDbHelper;
        private boolean isDuplicateEntries = true;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

//        Here 'this' is a MainActivity Context....
        newsDbHelper = new NewsDbHelper(MainActivity.this);
        dbHelper = new NewsDbHelper(this);
        sharedPrefs = new SharedPrefs(this);

        Log.e("getCategory val+Pos :: ", " :: "+sharedPrefs.getCategory()+" :: "+ sharedPrefs.getCategoryPos());

//        swipeRefreshLayout.setOnRefreshListener(this);
//        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        pDialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        categoryList = new ArrayList<>();
        *//*
         * Sports
         * Entertainment
         * business
         * health
         * science
         * technology

        /*country code for news from country....
         * us- United States
         * in-India
         * ua-ukraine
         * tw-Taiwan
         * th-Thailand
         * tr-Turkey
        categoryList.add("business");
        categoryList.add("politics");
        categoryList.add("health");
        categoryList.add("science");
        categoryList.add("entertainment");
        categoryList.add("sports");
        categoryList.add("technology");

        layoutManagerCategory = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        newsCategoryRV.setLayoutManager(layoutManagerCategory);
        newsCategoryRV.setHasFixedSize(true);
        newsCategoryRV.setItemAnimator(new DefaultItemAnimator());
        categoryAdapter = new MainActivity.NewsCategoryAdapter(this, categoryList);
        newsCategoryRV.setAdapter(categoryAdapter);

        layoutManager = new LinearLayoutManager(getApplicationContext());
//        snappingLinearLayoutManager = new SnappingLinearLayoutManager(MainActivity.this,
//                LinearLayoutManager.VERTICAL, false);
//        snappingLinearLayoutManager.smoothScrollToPosition(mRecyclerView, (RecyclerView.State) layoutManager.onSaveInstanceState(), 0);
//        mRecyclerView.setLayoutManager(layoutManager);
//
//        mRecyclerView.setHasFixedSize(true);
//        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        mRecyclerView.setNestedScrollingEnabled(false);


        onLoadingSwipeRefresh("");

       /* fabSwipeLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipeStackLoader.onViewSwipedToLeft();
            }
        });*//*

        fabSwipeRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvCountFavNews.setVisibility(View.GONE);
                Intent intentFavourite = new Intent(MainActivity.this, FavouriteActivity.class);
                startActivity(intentFavourite);
            }
        });

        swipeStackLoader.setSwipeProgressListener(new SwipeStack.SwipeProgressListener() {
            @Override
            public void onSwipeStart(int position) {
                fabSwipeRight.hide();
                tvCountFavNews.setVisibility(View.GONE);
            }

            @Override
            public void onSwipeProgress(int position, float progress) {
                Log.e("swipe prog. ", position+" :: "+progress);
            }

            @Override
            public void onSwipeEnd(int position) {
                fabSwipeRight.show();

               *//* if(dbHelper.getCountDBTable()>0){
                    Log.e("cursor count :: ", ""+dbHelper.getCountDBTable()+1);
                    tvCountFavNews.setVisibility(View.VISIBLE);
                    tvCountFavNews.setText(String.valueOf(dbHelper.getCountDBTable()+1));
                }else {
                    tvCountFavNews.setVisibility(View.GONE);
                }*//*
            }
        });

        swipeStackLoader.setListener(new SwipeStack.SwipeStackListener() {
            @Override
            public void onViewSwipedToLeft(int position) {

                Log.e("swipe dirn. ", " :: "+swipeStackLoader.getCurrentPosition());
//                fabSwipeLeft.hide();
                fabSwipeRight.hide();
                tvCountFavNews.setVisibility(View.GONE);
            }

            @Override
            public void onViewSwipedToRight(int position) {

                Log.e("swipe dirn. ", " :: "+swipeStackLoader.getCurrentPosition());

                final Article articleModel = articleList.get(position);
                boolean result = false;

                Log.e("cursord datachk.1 :: ", ""+isDuplicateEntries);
                // Select All Query
                String selectQuery = "SELECT  * FROM " + TABLE_NAME;

                SQLiteDatabase db = dbHelper.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);

                if(cursor.getCount()>0){
                    if(cursor.moveToFirst()){
                        do {

                            Log.e("cursord datachk.2 :: ", ""+isDuplicateEntries);
                            if(!cursor.getString(cursor.getColumnIndex(NEWS_TITLE)).equals(articleModel.getTitle())){
                                result = dbHelper.insertFavouriteNewsData(articleModel.getTitle(), articleModel.getDescription(),
                                        articleModel.getUrl(), articleModel.getUrlToImage(),
                                        articleModel.getSource().getName(), articleModel.getAuthor(), articleModel.getPublishedAt());

                                Log.e("cursor count :: ", ""+(cursor.getCount()+1));
                                tvCountFavNews.setVisibility(View.VISIBLE);
                                tvCountFavNews.setText(String.valueOf(cursor.getCount()+1));
                                isDuplicateEntries = false;
                                if(isDuplicateEntries){
                                    isDuplicateEntries = false;
                                }else {
                                    isDuplicateEntries = true;
                                }

                                Log.e("cursord datachk.3 :: ", ""+isDuplicateEntries);

                            }else {
                                isDuplicateEntries = true;
                                Log.e("cursor comp. :: ", "data is not inserted Successfully !!");

                                isDuplicateEntries = true;
                                Log.e("cursord datachk.5 :: ", ""+isDuplicateEntries);
                            }
                        }while (cursor.moveToNext());

                        Log.e("cursord datachk.6 :: ", ""+isDuplicateEntries);
                        if(isDuplicateEntries){

                            Log.e("cursor count :: ", ""+(cursor.getCount()+1));
                            Snackbar.make(mainNewsCoLayt, "Add News in Favourites :: "+(cursor.getCount()+1), Snackbar.LENGTH_SHORT).show();
                        }else {
                            Snackbar.make(mainNewsCoLayt, "News is already available in Favourites !!", Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    tvCountFavNews.setVisibility(View.VISIBLE);
                    tvCountFavNews.setText(String.valueOf(cursor.getCount()+1));
                    result = dbHelper.insertFavouriteNewsData(articleModel.getTitle(), articleModel.getDescription(),
                            articleModel.getUrl(), articleModel.getUrlToImage(),
                            articleModel.getSource().getName(), articleModel.getAuthor(), articleModel.getPublishedAt());

                    Log.e("cursor count :: ", ""+(cursor.getCount()+1));
                    Snackbar.make(mainNewsCoLayt, "Add News in Favourites :: "+(cursor.getCount()+1), Snackbar.LENGTH_SHORT).show();
                }

                if(result){
                    Log.e("result :: ", "data is inserted Successfully !!");
                }else {
                    Log.e("result :: ", "data is not inserted !!");
                }
            }

            @Override
            public void onStackEmpty() {

                Log.e("swipe dirn. ", " :: "+swipeStackLoader.getCurrentPosition());
                loadMoreButton.setVisibility(View.VISIBLE);
                tvdataNotFound.setVisibility(View.VISIBLE);
                fabSwipeRight.hide();
            }
        });

        loadMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoadingSwipeRefresh("");
                loadMoreButton.setVisibility(View.GONE);
                tvdataNotFound.setVisibility(View.GONE);
            }
        });
    }

        private void initViews() {
        articleList = new ArrayList<>();
//        topHeadLine = findViewById(R.id.topHeadLine);
//        swipeRefreshLayout = findViewById(R.id.swipeRefLayout);
//        mRecyclerView = findViewById(R.id.newsArticleRV);

        errorLayout = findViewById(R.id.errorLayout);
        errorImageView = findViewById(R.id.errorImage);
        errorTitle = findViewById(R.id.errorTitle);
        errorMessage = findViewById(R.id.errorMessage);
        retryButton = findViewById(R.id.btnRetry);
        newsCategoryRV = findViewById(R.id.newsCategoryRV);

        swipeStackLoader = findViewById(R.id.swipeStack);
        tvdataNotFound = findViewById(R.id.TextNoFound);
        loadMoreButton = findViewById(R.id.btnLoadMore);
        fabSwipeLeft = findViewById(R.id.fabSwipeLeft);
        fabSwipeRight = findViewById(R.id.fabSwipeRight);
        mainNewsCoLayt = findViewById(R.id.mainNewsCoLayt);
        tvCountFavNews = findViewById(R.id.tvCountFavNews);
    }

//    When Internet is not Available then this layout is appear ....
        private void showErrorMessage(int ImgId, String title, String msg){

        if(errorLayout.getVisibility() == View.GONE){
            errorLayout.setVisibility(View.VISIBLE);
        }

        errorImageView.setImageResource(ImgId);
        errorTitle.setText(title);
        errorMessage.setText(msg);

        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoadingSwipeRefresh("");
            }
        });
    }*/

        /*private void loadJsonData(String keyword, String catValue){

//        layoutManager.scrollToPositionWithOffset(pos, 0);
        errorLayout.setVisibility(View.GONE);
        Log.e("adapter size :: ", ""+articleList.size());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

//        mRecyclerView.scrollToPosition(0);
//        swipeRefreshLayout.setRefreshing(true);

        String country = Utils.getCountry();
        String language = Utils.getLanguage();
        Log.e("lang :: ", language);

//        This call is used for calling RESTFUL API HERE.....
        Call<News> call;

        if(keyword.length()>0){
            call = apiInterface.getNewsSearch(keyword,"hi","publishedAt",API_KEY);
        }else {
            call = apiInterface.getNews("in",catValue,API_KEY);
        }

        Log.e("call :: ", ""+call);
        Log.e("country :: ", ""+country);
        Log.e("category :: ", ""+catValue);
        Log.e("api_key :: ", ""+API_KEY);
        call.enqueue(new Callback<News>() {
            @Override
            public void onResponse(@NonNull Call<News> call, @NonNull Response<News> response) {

                swipeStackLoader.resetStack();
                Log.e("response :: ", ""+response);
                if(response.isSuccessful() && response.body().getArticleList() != null){

                    if(!articleList.isEmpty()){
                        articleList.clear();
                    }

                    articleList = response.body().getArticleList();
                    swipeStackAdapter = new NewsSwipeStackAdapter(MainActivity.this, articleList);
                    swipeStackLoader.setAdapter(swipeStackAdapter);
                    swipeStackAdapter.notifyDataSetChanged();

//                    newsAdapter = new NewsAdapter(MainActivity.this, articleList, mRecyclerView);
//                    mRecyclerView.setAdapter(newsAdapter);
//                    newsAdapter.notifyDataSetChanged();
//                    mRecyclerView.smoothScrollToPosition(0);
//                    initListener();

                    if(pDialog!= null){
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.dismiss();
                    }
//                    topHeadLine.setVisibility(View.VISIBLE);
//                    swipeRefreshLayout.setRefreshing(false);
                }else {
//                    swipeRefreshLayout.setRefreshing(false);
//                    topHeadLine.setVisibility(View.INVISIBLE);
//                    Toast.makeText(MainActivity.this, "", Toast.LENGTH_SHORT).show();

                    String errorCode;
                    Log.e("Error Code :: ", ""+response.code());
                    switch (response.code()){

                        case 404:
                            errorCode = "404 not found";

                        case 500:
                            errorCode = "500 server broken";

                        default:
                            errorCode = "Unknown error";
                    }

                    showErrorMessage(R.drawable.no_result, "No Result", "Please Try Again \n"+errorCode);
                }
            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {
                Log.e("Error :: ", ""+t.toString());
//                topHeadLine.setVisibility(View.INVISIBLE);
//                swipeRefreshLayout.setRefreshing(false);
                showErrorMessage(R.drawable.oops, "Oops...", "Network Failure, \nPlease Try Again ");
            }
        });
    }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView)menu.findItem(R.id.searchView).getActionView();
        MenuItem menuItem = menu.findItem(R.id.searchView);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Search Latest News....");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query.length()>2){
//                    loadJsonData(query);
                    onLoadingSwipeRefresh(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                loadJsonData(newText, sharedPrefs.getCategory());
                return false;
            }
        });

        menuItem.getIcon().setVisible(false, false);

        return super.onCreateOptionsMenu(menu);
    }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.setting:
                Intent intentSetting = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intentSetting);
                break;

            case R.id.news_notification:
                Intent intentNotification = new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(intentNotification);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

        private void initListener(){
        newsAdapter.setOnItemClickListener(new NewsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                final Article articleModel = articleList.get(position);
//
//                ImageView imageView = view.findViewById(R.id.newsImgView);
//                TextView textView = view.findViewById(R.id.title);
//                final ImageView favImageView = view.findViewById(R.id.favNewsImgV);
//                favImageView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if(sharedPrefs.getFavouriteNews()){
//
//                             boolean result = dbHelper.insertFavouriteNewsData(articleModel.getTitle(), articleModel.getDescription(),
//                                     articleModel.getUrl(), articleModel.getUrlToImage(),
//                                     articleModel.getSource().getName(), articleModel.getAuthor(), articleModel.getPublishedAt());
//
//                             if(result){
//                                 Log.e("result :: ", "data is inserted Successfully !!");
//                             }else {
//                                 Log.e("result :: ", "data is not inserted !!");
//                             }
//
//                            favImageView.setImageResource(R.drawable.ic_favorite);
//                            sharedPrefs.setFavouriteNews(false);
//                        }else {
//                            favImageView.setImageResource(R.drawable.ic_favorite_border);
//                            sharedPrefs.setFavouriteNews(true);
//                        }
//                    }
//                });
//
//                Intent intent= new Intent(MainActivity.this, NewsDetailActivity.class);
//                intent.putExtra("url", articleModel.getUrl());
//                intent.putExtra("title", articleModel.getTitle());
//                intent.putExtra("news_img", articleModel.getUrlToImage());
//                intent.putExtra("date", articleModel.getPublishedAt());
//                intent.putExtra("source_name",articleModel.getSource().getName());
//                intent.putExtra("author",articleModel.getAuthor());
//
//
//                Pair<View, String> pair = Pair.create((View)imageView, ViewCompat.getTransitionName(imageView));
//                Pair<View, String> pair2 = Pair.create((View)textView, ViewCompat.getTransitionName(textView));
//                ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, pair, pair2);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                    startActivity(intent, optionsCompat.toBundle());
//                }else {
//                    startActivity(intent);
//                }
            }
        });
    }

    *//*@Override
    public void onRefresh() {

        loadJsonData("",sharedPrefs.getCategory());
    }*//*

        private void onLoadingSwipeRefresh(final String keyword){
        loadJsonData(keyword,sharedPrefs.getCategory());
        *//*swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {

            }
        });*//*
    }

        private boolean doubleBackToExitPressedOnce = false;

        @Override
        public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }


//    This adapter is here is inner to MainClass for updating news category according to category value...
        class NewsCategoryAdapter extends RecyclerView.Adapter<MainActivity.NewsCategoryAdapter.CategoryViewHolder> {

            private List<String> categoryList;
            private Context mContext;
            int selectedPos = sharedPrefs.getCategoryPos();

            private NewsCategoryAdapter(Context mContext, List<String> categoryList) {
                this.categoryList = categoryList;
                this.mContext = mContext;
            }

            @NonNull
            @Override
            public MainActivity.NewsCategoryAdapter.CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(mContext).inflate(R.layout.news_category_tab, viewGroup, false);
                return new MainActivity.NewsCategoryAdapter.CategoryViewHolder(view);
            }

            @Override
            public void onBindViewHolder(@NonNull final MainActivity.NewsCategoryAdapter.CategoryViewHolder categoryViewHolder, final int position) {

                final String catValue = categoryList.get(position);
                if (selectedPos == position) {
                    categoryViewHolder.itemView.setSelected(true);
                    categoryViewHolder.categoryTV.setTextColor(Color.parseColor("#cf2a2d"));
                } else {
                    categoryViewHolder.itemView.setSelected(false);
                    categoryViewHolder.categoryTV.setTextColor(Color.WHITE);
                }

                categoryViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        notifyItemChanged(selectedPos);
                        selectedPos = position;
                        notifyItemChanged(selectedPos);

                        sharedPrefs.setCategory(catValue);
                        sharedPrefs.setCategoryPos(selectedPos);

                        loadMoreButton.setVisibility(View.GONE);
                        tvdataNotFound.setVisibility(View.GONE);
                        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                        pDialog.setTitleText("Loading " + catValue + "data....");
                        pDialog.setCancelable(false);
//                    pDialog.show();
                        Log.e("catValue ", " :: "+catValue);
                        loadJsonData("", catValue);
                    }
                });

                String str = categoryList.get(position);
                String cap = str.substring(0, 1).toUpperCase() + str.substring(1);
                categoryViewHolder.categoryTV.setText(cap);
            }

            @Override
            public int getItemCount() {
                return categoryList.size();
            }

            private class CategoryViewHolder extends RecyclerView.ViewHolder{

                private TextView categoryTV;
                private CategoryViewHolder(@NonNull View itemView) {
                    super(itemView);

                    categoryTV = itemView.findViewById(R.id.categoryTV);
                }
            }
        }
    }
}
*/