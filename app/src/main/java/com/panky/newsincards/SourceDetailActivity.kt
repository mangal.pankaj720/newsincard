package com.panky.newsincards

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.annotation.StyleRes
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.widget.Toolbar
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.gms.ads.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

import com.panky.newsincards.Adapter.NewsSwipeStackAdapter
import com.panky.newsincards.Constants.Constant
import com.panky.newsincards.Constants.ShareNewsListener
import com.panky.newsincards.DBHelper.NewsDbHelper
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.Utility.Utils
import com.panky.newsincards.api.ApiClient
import com.panky.newsincards.api.ApiInterface
import com.panky.newsincards.models.Article
import com.panky.newsincards.models.News
import com.varunest.sparkbutton.SparkButton
import kotlinx.android.synthetic.main.error_message_layout.*

import link.fls.swipestack.SwipeStack
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class SourceDetailActivity : AppCompatActivity(), Constant, View.OnClickListener, ShareNewsListener {
    override fun getShareNewsPosition(position: Int) {
        Log.e("Share Pos ", " :: $position")

        fabNewsDetailSources.hide()
        rlScreenshotShare.visibility = View.VISIBLE
        val utils = Utils(this@SourceDetailActivity)
        val articleModel = newsarticleList!![position]

        val requestOptions = RequestOptions()
        requestOptions.placeholder(utils.randomDrawbleColor)
        requestOptions.error(utils.randomDrawbleColor)
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)
        requestOptions.skipMemoryCache(true)
        requestOptions.centerCrop()

        Log.e("urlToImage ", " :: "+articleModel.urlToImage)

        Glide.with(this@SourceDetailActivity)
            .load(articleModel.urlToImage)
            .apply(requestOptions)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    mProgBar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any?, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    mProgBar.visibility = View.GONE
                    return false
                }
            })
            .error(R.drawable.new_pic)
            .thumbnail(0.6f)
            .dontAnimate()
            .dontTransform()
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(newsImgView)


        Log.e("DB Data :: ","")
        title.text = articleModel.title

        if(!articleModel.description.isNullOrEmpty()){

            val result = Html.fromHtml(articleModel.description!!).toString().replace("\n", "").trim()
            desc.text = result
        }else{
            desc.visibility = View.GONE
        }

        source.text = articleModel.source?.name
        val outputDateFormat = articleModel.publishedAt?.subSequence(0, 19)
        Log.e("date value :: ", outputDateFormat as String?)
        time.text = String.format(" \u2022 "+utils.DateToTimeFormat(outputDateFormat.toString()))
        publishedAt.text = utils.DateFormat(outputDateFormat.toString())
        author.text = articleModel.author
        urlLink.movementMethod = LinkMovementMethod.getInstance()
        urlLink.text = articleModel.url

        tvShareScreenCancel.setOnClickListener {
            rlScreenshotShare.visibility = View.GONE
            fabNewsDetailSources.show()
        }

        tvShareScreenShot.setOnClickListener {
            rlScreenshotShare.visibility = View.GONE
            fabNewsDetailSources.show()
            if(checkPermission()){
                val view:View = findViewById(R.id.rlShareCardNews)
                val androidColors = resources.getIntArray(R.array.androidcolors)
                val randomAndroidColor = androidColors[Random().nextInt(androidColors.size)]
                view.setBackgroundColor(randomAndroidColor)

                val bitmap = takeScreenshot(view)
                saveBitmap(bitmap)
                shareIt(imageFile!!)
            }else {
                requestPermission()
            }

        }

    }

    //Add item Views here...
    private lateinit var btnSparkAnimate: SparkButton
    private var tvCountFavNews: TextView? = null
    private lateinit var rlFavouriteFab:RelativeLayout
    private lateinit var fabAddNewsItem: FloatingActionButton

    private var newsarticleList: MutableList<Article>? = null
    private var sourceDetailToolbar: Toolbar? = null
    private var sName: String? = null
    private var sID: String? = null
    private var errorLayout: RelativeLayout? = null
    private var errorImageView: ImageView? = null
    private var errorTitle: TextView? = null
    private var errorMessage: TextView? = null
    private lateinit var retryButton: Button
    private lateinit var btnLoadMore:Button

    private lateinit var fabNewsDetailSources: FloatingActionButton
    private lateinit var tvSourceHeadline:TextView
    private lateinit var rlLoadMoreContent:RelativeLayout

//    private lateinit var adViewBannerSource: AdView
    private var swipeStackSource: SwipeStack? = null
    private var swipeStackAdapter: NewsSwipeStackAdapter? = null
    private var sharedPrefs: SharedPrefs? = null

    //Share News Views instance here...
    private lateinit var tvShareScreenCancel:TextView
    private lateinit var tvShareScreenShot:TextView
    private lateinit var rlScreenshotShare:RelativeLayout
    lateinit var title: TextView
    lateinit var desc: TextView
    lateinit var author: TextView
    lateinit var publishedAt: TextView
    lateinit var time: TextView
    lateinit var urlLink: TextView
    lateinit var newsImgView: ImageView
    lateinit var mProgBar: ProgressBar
    lateinit var source : TextView

    companion object{
        private var pageNo:Int = 1
    }

//    private var mInterstitialAd : InterstitialAd? =null
    private lateinit var dbHelper: NewsDbHelper

    //Firebase instance here....
    private var dbReference : DatabaseReference?= null
    private var mAuth: FirebaseAuth? = null
    private var user: FirebaseUser?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        sharedPrefs = SharedPrefs(this@SourceDetailActivity)
        if (sharedPrefs!!.isNightModeEnabled) {
            setAppTheme(R.style.AppTheme_Base_Night)
        } else {
            setAppTheme(R.style.AppTheme_Base_Light)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_source_detail)
        initViews()
        initializeBannerAds()

        dbHelper = NewsDbHelper(this@SourceDetailActivity)
        //Firebase instance initialize here....
        dbReference = FirebaseDatabase.getInstance().reference.child("UserFavouriteNews")
        mAuth = FirebaseAuth.getInstance()
        user = mAuth!!.currentUser

      /*  mInterstitialAd = InterstitialAd(this)
        mInterstitialAd!!.adUnitId = resources.getString(R.string.interstitial_ad_unit_id)
        mInterstitialAd!!.loadAd(AdRequest.Builder().build())*/
        pageNo = 1
        val intent = intent
        sID = intent.getStringExtra(Constant.SOURCE_ID)
        sName = intent.getStringExtra(Constant.SOURCE_NAME)

        newsarticleList = ArrayList()
        setSupportActionBar(sourceDetailToolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            tvSourceHeadline.text = sName
        }

        loadSourcesFromRetrofit(sID, pageNo)

        swipeStackSource!!.setSwipeProgressListener(object : SwipeStack.SwipeProgressListener {
            override fun onSwipeStart(position: Int) {
                fabNewsDetailSources.show()
            }

            override fun onSwipeProgress(position: Int, progress: Float) {
                Log.e("swipe prog. ", "$position :: $progress")

//                fabNewsDetail.show()
                if(progress > 0.2){
                    if(user != null){
                        swipeStackSource!!.allowedSwipeDirections = SwipeStack.SWIPE_DIRECTION_BOTH

                    }else{
                        swipeStackSource!!.allowedSwipeDirections = SwipeStack.SWIPE_DIRECTION_ONLY_LEFT
                        val snackbar = Snackbar.make(findViewById(R.id.rlAddFavouriteNews), "If you add News Article in Favourites. " +
                                "So, please login to access this feature.", Snackbar.LENGTH_LONG)
                        snackbar.setAction("OK") {
                            snackbar.dismiss()
                        }
                        snackbar.show()
                    }
                }

                //                btnSparkAnimate.setInactiveImage(R.drawable.ic_favorite_border);
            }

            override fun onSwipeEnd(position: Int) {
//                fabSwipeRight!!.show()
//                btnSparkAnimate!!.visibility = View.VISIBLE


                /* if(dbHelper.getCountDBTable()>0){
                    Log.e("cursor count :: ", ""+dbHelper.getCountDBTable()+1);
                    tvCountFavNews.setVisibility(View.VISIBLE);
                    tvCountFavNews.setText(String.valueOf(dbHelper.getCountDBTable()+1));
                }else {
                    tvCountFavNews.setVisibility(View.GONE);
                }*/
            }
        })

        swipeStackSource!!.setListener(object : SwipeStack.SwipeStackListener {
            override fun onViewSwipedToLeft(position: Int) {

                Log.e("swipe dirn. ", " :: " + swipeStackSource!!.currentPosition)
                //                fabSwipeLeft.hide();
//                fabSwipeRight!!.hide()
//                tvCountFavNews!!.visibility = View.GONE
//                btnSparkAnimate!!.visibility = View.GONE
            }

            override fun onViewSwipedToRight(position: Int) {
                Log.e("swipe dirn. ", " :: " + swipeStackSource!!.currentPosition)
                addFavouritesDataSource(position)
            }

            override fun onStackEmpty() {
                fabNewsDetailSources.hide()
                rlFavouriteFab.visibility = View.GONE
                Log.e("swipe dirn. ", " :: " + swipeStackSource!!.currentPosition)
                rlLoadMoreContent.visibility = View.VISIBLE
                /*mInterstitialAd!!.loadAd(AdRequest.Builder().build())

                mInterstitialAd!!.adListener = object : AdListener(){
                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        showInterstitialAd()
                    }
                }*/
            }
        })
        val array = resources.getStringArray(R.array.rainbow)
        val randomStr = array[Random().nextInt(array.size)]

        //here you define your layout `
        //        LinearLayout myLayout = (LinearLayout) findViewById(R.id.that_linear);
        //        myLayout.setBackgroundColor(Color.parseColor(randomStr));

        //        Random rnd = new Random();
        //        currentStrokeColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

        val androidColors = resources.getIntArray(R.array.androidcolors)
//        val randomAndroidColor = androidColors[Random().nextInt(androidColors.size)]
        //        view.setBackgroundColor(randomAndroidColor);

        btnLoadMore.setOnClickListener(this)
        retryButton.setOnClickListener(this)
        fabNewsDetailSources.setOnClickListener(this)
        fabAddNewsItem.setOnClickListener(this)
    }

    private fun showInterstitialAd() {
        /*if (mInterstitialAd!!.isLoaded) {
            mInterstitialAd!!.show()
        }*/
    }

    private fun initializeBannerAds(){
        //Banner ads initialize here....
       /* MobileAds.initialize(this, getString(R.string.banner_search_unit));
        val adRequest = AdRequest.Builder().build()
        adViewBannerSource.loadAd(adRequest)*/
    }

    override fun onClick(v: View?) {

        when(v!!.id){

            R.id.btnLoadMore ->{
                pageNo++
                loadSourcesFromRetrofit(sID, pageNo)
                rlLoadMoreContent.visibility = View.GONE
            }

            R.id.btnRetry -> {
                if(btnRetry.text.equals("Choose Source")){
                    val intentSource = Intent(this@SourceDetailActivity, SourcesActivity::class.java)
                    intentSource.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intentSource)
                    finish()
                }else {
                    loadSourcesFromRetrofit(sID, pageNo)
                }
            }

            R.id.fabNewsDetailSources -> {
                Log.e("topview curntPos ", " :: "+swipeStackSource!!.currentPosition)

                val currentPos = swipeStackSource!!.currentPosition
                val articleModel = newsarticleList!![currentPos]

                val intent =  Intent(this@SourceDetailActivity, NewsDetailActivity::class.java)
                intent.putExtra("url", articleModel.url)
                intent.putExtra("title", articleModel.title)
                intent.putExtra("news_img", articleModel.urlToImage)
                intent.putExtra("date", articleModel.publishedAt)
                intent.putExtra("source_name",articleModel.source!!.name)
                intent.putExtra("author",articleModel.author)
                startActivity(intent)
            }

            R.id.fabAddNewsItem ->{
                tvCountFavNews!!.visibility = View.GONE
                val intentFavourite = Intent(this@SourceDetailActivity, FavouriteActivity::class.java)
                startActivity(intentFavourite)
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.news_detail_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.resetStack -> {
                swipeStackSource!!.resetStack()
                rlLoadMoreContent.visibility = View.GONE
                errorLayout!!.visibility = View.GONE
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addFavouritesDataSource(position: Int){

        val articleModel = newsarticleList!![position]
        var result = false
        var isDuplicate = false

//        btnSparkAnimate!!.visibility = View.GONE
//        fabSwipeRight!!.setImageResource(R.drawable.ic_favorite_border)

        val selectQuery = "SELECT  * FROM " + Constant.TABLE_NAME
        val db = dbHelper.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        Log.e("total count :: ", "${cursor.count}")

        if (cursor.count > 0) {
            if (cursor.moveToFirst()) {
                outer@do {
                    if ((cursor.getString(cursor.getColumnIndex(Constant.NEWS_TITLE))).equals(articleModel.title, true)) {
                        isDuplicate = true
                        break@outer
                    } else {
                        Log.e("cursor comp. :: ", "data is not inserted Successfully !!")
                    }
                } while (cursor.moveToNext())

                if (!isDuplicate) {

                    result = dbHelper.insertFavouriteNewsData(articleModel.title?:"", articleModel.description?:"",
                        articleModel.url?:"", articleModel.urlToImage?:"",
                        articleModel.source!!.name?:"", articleModel.author?:"",
                        articleModel.publishedAt?:"", articleModel.source!!.id?:"")

                    val userFavData : HashMap<String, String> = HashMap()
                    userFavData.set("Title", articleModel.title?:"")
                    userFavData.set("Desc", articleModel.description?:"")
                    userFavData.set("Url", articleModel.url?:"")
                    userFavData.set("UrlToImage", articleModel.urlToImage?:"")
                    userFavData.set("Author", articleModel.author?:"")
                    userFavData.set("PublishedAt", articleModel.publishedAt?:"")
                    userFavData.set("SourceName", articleModel.source!!.name?:"")
                    userFavData.set("SourceId", articleModel.source!!.id?:"")

                    rlFavouriteFab.visibility = View.VISIBLE
                    tvCountFavNews!!.visibility = View.VISIBLE
                    btnSparkAnimate!!.visibility = View.VISIBLE

                    btnSparkAnimate!!.playAnimation()
                    btnSparkAnimate!!.setAnimationSpeed(1.5f)
//        btnSparkAnimate!!.pressOnTouch(false)

                    if (btnSparkAnimate!!.isChecked) {
                        Log.e("btnSpark :: ", "" + btnSparkAnimate!!.isChecked)
                        btnSparkAnimate!!.setActiveImage(R.drawable.ic_favorite_border)
                        btnSparkAnimate!!.setInactiveImage(R.drawable.ic_favorite)
                        btnSparkAnimate!!.isChecked = false
                    } else {
                        Log.e("btnSpark :: ", "" + btnSparkAnimate!!.isChecked)
                        btnSparkAnimate!!.isChecked = true
                        btnSparkAnimate!!.setActiveImage(R.drawable.ic_favorite)
                        btnSparkAnimate!!.setInactiveImage(R.drawable.ic_favorite_border)
                    }

                    tvCountFavNews!!.visibility = View.GONE
                    btnSparkAnimate!!.visibility = View.GONE


                    Handler().postDelayed({
                        rlFavouriteFab.visibility = View.GONE

                    }, 2000)

                    dbReference!!.child(user!!.uid).push().setValue(userFavData).addOnCompleteListener { task ->
                        if(task.isSuccessful){
                            Log.e("addValueDbError ", " :: task is complete")
                        }
                    }
                    Log.e("cursor count :: ", "" + (cursor.count + 1))
                    tvCountFavNews!!.visibility = View.VISIBLE
                    tvCountFavNews!!.text = (cursor.count + 1).toString()

                    Log.e("cursor count :: ", "" + (cursor.count + 1))
                    Snackbar.make(findViewById(R.id.rlAddFavouriteNews), "Add News in Favourites :: " + (cursor.count + 1), Snackbar.LENGTH_SHORT).show()
                } else {
                    Snackbar.make(findViewById(R.id.rlAddFavouriteNews), "News is already available in Favourites !!", Snackbar.LENGTH_SHORT).show()
                }
            }
        } else {
            tvCountFavNews!!.visibility = View.VISIBLE
            tvCountFavNews!!.text = (cursor.count + 1).toString()
            result = dbHelper.insertFavouriteNewsData(articleModel.title?:"", articleModel.description?:"",
                articleModel.url?:"", articleModel.urlToImage?:"",
                articleModel.source!!.name?:"", articleModel.author?:"",
                articleModel.publishedAt?:"", articleModel.source!!.id?:"")

            Log.e("cursor count :: ", "" + (cursor.count + 1))
            Snackbar.make(findViewById(R.id.rlAddFavouriteNews), resources.getString(R.string.addNewsInFav)+" :: " + (cursor.count + 1), Snackbar.LENGTH_SHORT).show()
        }

        if (result) {
            Log.e("result :: ", "data is inserted Successfully !!")
        } else {
            Log.e("result :: ", "data is not inserted !!")
        }

        cursor.close()
    }

    public override fun onPause() {
//        adViewBannerSource.pause()
        super.onPause()
    }

    public override fun onResume() {
        super.onResume()
//        adViewBannerSource.resume()
    }

    public override fun onDestroy() {
//        adViewBannerSource.destroy()
        super.onDestroy()
    }

    private fun setAppTheme(@StyleRes style: Int) {
        setTheme(style)
    }

    fun takeScreenshot(view : View): Bitmap {
        view.isDrawingCacheEnabled = true
        return Bitmap.createBitmap(view.drawingCache)
    }

    private var imagePath: String? = null
    private var imageFile: File?=null

    fun saveBitmap(bitmap: Bitmap) {

        val now = Date()
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now)
        imagePath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg"

        Log.e("image path ", " :: $imagePath")
        imageFile = File(imagePath)
        val fos: FileOutputStream
        try {
            fos = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            fos.flush()
            fos.close()
        } catch (e: FileNotFoundException) {
            Log.e("GREC", e.message, e)
        } catch (e: IOException) {
            Log.e("GREC", e.message, e)
        }
    }

    private fun shareIt(imageFile: File) {

        Log.e("image File ", " :: $imageFile")
        val uri = Uri.fromFile(imageFile)
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "image/*"
//        val shareBody = "In Tweecher, My highest score with screen shot"
//        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "My Tweecher score")
//        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri)

        try {
            startActivity(Intent.createChooser(intent, "Share Screenshot via "))
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this@SourceDetailActivity, "No App Available", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkPermission() : Boolean{
        val result = ContextCompat.checkSelfPermission(this@SourceDetailActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() =
        if(ActivityCompat.shouldShowRequestPermissionRationale(this@SourceDetailActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            ActivityCompat.requestPermissions(this@SourceDetailActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                Constant.PERMISSION_REQUEST_CODE
            )
        }else {
            ActivityCompat.requestPermissions(this@SourceDetailActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                Constant.PERMISSION_REQUEST_CODE
            )
        }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode == Constant.PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Bitmap bitmap = takeScreenshot();
//                saveBitmap(bitmap);
//                shareIt();
            } else {
                Log.e("value", "Permission Denied, You cannot use Storage Local Drive. ");
            }
        }
    }

    private fun initViews() {
        sourceDetailToolbar = findViewById(R.id.sourceDetailToolbar)

        errorLayout = findViewById(R.id.errorLayout)
        errorImageView = findViewById(R.id.errorImage)
        errorTitle = findViewById(R.id.errorTitle)
        errorMessage = findViewById(R.id.errorMessage)
        retryButton = findViewById(R.id.btnRetry)

        swipeStackSource = findViewById(R.id.swipeStackSource)
//        adViewBannerSource = findViewById(R.id.adViewBannerSource)
        btnLoadMore = findViewById(R.id.btnLoadMore)

        fabNewsDetailSources = findViewById(R.id.fabNewsDetailSources)
        tvSourceHeadline = findViewById(R.id.tvSourceHeadline)
        rlLoadMoreContent = findViewById(R.id.rlLoadMoreContent)

        //Share View News initialization here.....
        title = findViewById(R.id.titleShare)
        desc = findViewById(R.id.tvDescptShare)
        source = findViewById(R.id.sourceShare)
        author = findViewById(R.id.authorTV)
        publishedAt = findViewById(R.id.publishedAt)
        time = findViewById(R.id.timeShare)
        urlLink = findViewById(R.id.urlLinkShare)

        newsImgView = findViewById(R.id.newsImgViewShare)
        mProgBar = findViewById(R.id.progress_photo_load)
        rlScreenshotShare = findViewById(R.id.rlScreenshotShare)
        tvShareScreenShot = findViewById(R.id.tvShareScreenShot)
        tvShareScreenCancel = findViewById(R.id.tvShareScreenCancel)

        //Add News Item View initialize here....
       btnSparkAnimate = findViewById(R.id.btnSparkAnimateSource)
        tvCountFavNews = findViewById(R.id.tvCountFavNewsSource)
        rlFavouriteFab = findViewById(R.id.rlFavouriteFabSource)
        fabAddNewsItem = findViewById(R.id.fabAddNewsItem)

    }

    //    When Internet is not Available then this layout is appear ....
    private fun showErrorMessage(ImgId: Int, title: String, msg: String) {

        if (errorLayout!!.visibility == View.GONE) {
            errorLayout!!.visibility = View.VISIBLE
        }

        errorImageView!!.setImageResource(ImgId)
        errorTitle!!.text = title
        errorMessage!!.text = msg

        if(title.equals("No News Article available", true)){
            retryButton.text = "Choose Source"
        }
    }

    private fun onShowLoading() {
        findViewById<View>(R.id.shimmerCardLoading).visibility = View.VISIBLE
    }

    private fun onHideLoading() {
        findViewById<View>(R.id.shimmerCardLoading).visibility = View.GONE

    }

    private fun loadSourcesFromRetrofit(source_name: String?, pageNo : Int) {

        onShowLoading()
        val apiInterface = ApiClient.apiClient.create<ApiInterface>(ApiInterface::class.java)
//        val sourcesCall = apiInterface.getNewsBySources(source_name!!, pageNo,10,Constant.API_KEY)

        /*sourcesCall.enqueue(object : Callback<News> {
            override fun onResponse(call: Call<News>, response: Response<News>) {

                if (response.isSuccessful && response.body()!!.articleList != null) {

                    Log.e("Total result "," :: "+response.body()!!.totalResults)
                    onHideLoading()
                    Log.e("respose :: ", "" + response)
                    if (!newsarticleList!!.isEmpty()) {
                        newsarticleList!!.clear()
                    }

                    errorLayout!!.visibility = View.GONE

                    newsarticleList = response.body()!!.articleList
                    swipeStackAdapter = NewsSwipeStackAdapter(this@SourceDetailActivity,
                        newsarticleList!!, "SourceDetailActivity")

                    swipeStackSource!!.adapter = swipeStackAdapter
                    swipeStackAdapter!!.notifyDataSetChanged()
                    fabNewsDetailSources.show()

                    swipeStackAdapter!!.setShareNewsListener(this@SourceDetailActivity)
                    if(swipeStackAdapter!!.count == 0){
                        showErrorMessage(R.drawable.no_result, "No News Article available", "Please choose another source")
                        fabNewsDetailSources.hide()
                    }


                    //                    sourcesRecyclerAdapter = new NewsSourcesRecyclerAdapter(SourcesActivity.this, newsSourcesList);
                    //                    newsSourcesNameRV.setAdapter(sourcesRecyclerAdapter);
                    //                    sourcesRecyclerAdapter.notifyDataSetChanged();
                } else {
                    val errorCode: String
                    Log.e("Error Code :: ", "" + response.code())
                    when (response.code()) {
                        404 -> {
                            errorCode = "404 not found"
                        }
                        500 -> {
                            errorCode = "500 server broken"
                        }
                        else -> errorCode = "Unknown error"
                    }

                    fabNewsDetailSources.hide()
                                        showErrorMessage(R.drawable.no_result, "No Result", "Please Try Again \n"+errorCode)
                }
            }

            override fun onFailure(call: Call<News>, t: Throwable) {

                Log.e("Error :: ", "" + t.toString())
                fabNewsDetailSources.hide()
                showErrorMessage(R.drawable.oops, "Oops...", "Network Failure, \nPlease Try Again ")
            }
        })*/
    }


    override fun onBackPressed() {

        if(rlScreenshotShare.visibility == View.VISIBLE){
            rlScreenshotShare.visibility = View.GONE
            fabNewsDetailSources.show()
        }else{
            super.onBackPressed()
        }

    }
    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.news_forall_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.settingNews){

            startActivity(new Intent(SourceDetailActivity.this, SettingActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }*/
}




/*public class ScreenshotUtils {

  Method which will return Bitmap after taking screenshot. We have to pass the view which we want to take screenshot.
public static Bitmap getScreenShot(View view) {
    View screenView = view.getRootView();
    screenView.setDrawingCacheEnabled(true);
    Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
    screenView.setDrawingCacheEnabled(false);
    return bitmap;
}


  Create Directory where screenshot will save for sharing screenshot
public static File getMainDirectoryName(Context context) {
    //Here we will use getExternalFilesDir and inside that we will make our Demo folder
    //benefit of getExternalFilesDir is that whenever the app uninstalls the images will get deleted automatically.
    File mainDir = new File(
            context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "Demo");

    //If File is not present create directory
    if (!mainDir.exists()) {
        if (mainDir.mkdir())
            Log.e("Create Directory", "Main Directory Created : " + mainDir);
    }
    return mainDir;
}*/

/*  Store taken screenshot into above created path
public static File store(Bitmap bm, String fileName, File saveFilePath) {
    File dir = new File(saveFilePath.getAbsolutePath());
    if (!dir.exists())
        dir.mkdirs();
    File file = new File(saveFilePath.getAbsolutePath(), fileName);
    try {
        FileOutputStream fOut = new FileOutputStream(file);
        bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
        fOut.flush();
        fOut.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
    return file;
}
}*/

/*  Show screenshot Bitmap
private void showScreenShotImage(Bitmap b) {
    imageView.setImageBitmap(b);
}*/

/*  Share Screenshot
private void shareScreenshot(File file) {
    Uri uri = Uri.fromFile(file);//Convert file path into Uri for sharing
    Intent intent = new Intent();
    intent.setAction(Intent.ACTION_SEND);
    intent.setType("image/*");
    intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
    intent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.sharing_text));
    intent.putExtra(Intent.EXTRA_STREAM, uri);//pass uri here
    startActivity(Intent.createChooser(intent, getString(R.string.share_title)));
}
*/