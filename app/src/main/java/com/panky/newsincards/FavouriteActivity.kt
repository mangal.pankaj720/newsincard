package com.panky.newsincards

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
//import com.google.android.gms.ads.AdRequest
//import com.google.android.gms.ads.AdView
//import com.google.android.gms.ads.MobileAds

import com.panky.newsincards.Adapter.NewsSwipeStackAdapter
import com.panky.newsincards.DBHelper.NewsDbHelper
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.models.Article
import link.fls.swipestack.SwipeStack
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import android.os.Handler
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.text.Html
import android.text.method.LinkMovementMethod
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.panky.newsincards.Constants.Constant
import com.panky.newsincards.Constants.ShareNewsListener
import com.panky.newsincards.Utility.Utils
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


class FavouriteActivity : AppCompatActivity(), ShareNewsListener {

    override fun getShareNewsPosition(position: Int) {
        Log.e("Share Pos ", " :: $position")

        fabNewsDetailFav.hide()
        rlScreenshotShare.visibility = View.VISIBLE
        val utils = Utils(this@FavouriteActivity)
        val articleModel = articleList!![position]

        val requestOptions = RequestOptions()
        requestOptions.placeholder(utils.randomDrawbleColor)
        requestOptions.error(utils.randomDrawbleColor)
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)
        requestOptions.skipMemoryCache(true)
        requestOptions.centerCrop()

        Log.e("urlToImage ", " :: "+articleModel.urlToImage)

        Glide.with(this@FavouriteActivity)
            .load(articleModel.urlToImage)
            .apply(requestOptions)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    mProgBar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any?, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    mProgBar.visibility = View.GONE
                    return false
                }
            })
            .error(R.drawable.new_pic)
            .thumbnail(0.6f)
            .dontAnimate()
            .dontTransform()
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(newsImgView)


        Log.e("DB Data :: ","")
        title.text = articleModel.title

        if(!articleModel.description.isNullOrEmpty()){

            val result = Html.fromHtml(articleModel.description!!).toString().replace("\n", "").trim()
            desc.text = result
        }else{
            desc.visibility = View.GONE
        }

        source.text = articleModel.source?.name
        val outputDateFormat = articleModel.publishedAt?.subSequence(0, 19)
        Log.e("date value :: ", outputDateFormat as String?)
        time.text = String.format(" \u2022 "+utils.DateToTimeFormat(outputDateFormat.toString()))
        publishedAt.text = utils.DateFormat(outputDateFormat.toString())
        author.text = articleModel.author
        urlLink.movementMethod = LinkMovementMethod.getInstance()
        urlLink.text = articleModel.url

        tvShareScreenCancel.setOnClickListener {
            rlScreenshotShare.visibility = View.GONE
            fabNewsDetailFav.show()
        }

        tvShareScreenShot.setOnClickListener {
            rlScreenshotShare.visibility = View.GONE
            fabNewsDetailFav.show()
            if(checkPermission()){
                val view:View = findViewById(R.id.rlShareCardNews)
                val androidColors = resources.getIntArray(R.array.androidcolors)
                val randomAndroidColor = androidColors[Random().nextInt(androidColors.size)]
                view.setBackgroundColor(randomAndroidColor)

                val bitmap = takeScreenshot(view)
                saveBitmap(bitmap)
                shareIt(imageFile!!)
            }else {
                requestPermission()
            }

        }

    }

    fun takeScreenshot(view : View): Bitmap {
        view.isDrawingCacheEnabled = true
        return Bitmap.createBitmap(view.drawingCache)
    }

    private var imagePath: String? = null
    private var imageFile: File?=null

    fun saveBitmap(bitmap: Bitmap) {

        val now = Date()
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now)
        imagePath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg"

        Log.e("image path ", " :: $imagePath")
        imageFile = File(imagePath)
        val fos: FileOutputStream
        try {
            fos = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            fos.flush()
            fos.close()
        } catch (e: FileNotFoundException) {
            Log.e("GREC", e.message, e)
        } catch (e: IOException) {
            Log.e("GREC", e.message, e)
        }
    }

    private fun shareIt(imageFile: File) {

        Log.e("image File ", " :: $imageFile")
        val uri = Uri.fromFile(imageFile)
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "image/*"
//        val shareBody = "In Tweecher, My highest score with screen shot"
//        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "My Tweecher score")
//        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri)

        try {
            startActivity(Intent.createChooser(intent, "Share Screenshot via "))
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this@FavouriteActivity, "No App Available", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkPermission() : Boolean{
        val result = ContextCompat.checkSelfPermission(this@FavouriteActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() =
        if(ActivityCompat.shouldShowRequestPermissionRationale(this@FavouriteActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            ActivityCompat.requestPermissions(this@FavouriteActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                Constant.PERMISSION_REQUEST_CODE
            )
        }else {
            ActivityCompat.requestPermissions(this@FavouriteActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                Constant.PERMISSION_REQUEST_CODE
            )
        }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode == Constant.PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Bitmap bitmap = takeScreenshot();
//                saveBitmap(bitmap);
//                shareIt();
            } else {
                Log.e("value", "Permission Denied, You cannot use Storage Local Drive. ");
            }
        }
    }

    //Share News Views instance here...
    private lateinit var tvShareScreenCancel:TextView
    private lateinit var tvShareScreenShot:TextView
    private lateinit var rlScreenshotShare:RelativeLayout
    lateinit var title: TextView
    lateinit var desc: TextView
    lateinit var author: TextView
    lateinit var publishedAt: TextView
    lateinit var time: TextView
    lateinit var urlLink: TextView
    lateinit var newsImgView: ImageView
    lateinit var mProgBar: ProgressBar
    lateinit var source : TextView

    private var articleList: List<Article>? = null
    private var dbHelper: NewsDbHelper? = null
    private var favToolbar: Toolbar? = null
    private var sharedPrefs: SharedPrefs? = null
    private lateinit var swipeStackLoader: SwipeStack
    private var swipeStackAdapter: NewsSwipeStackAdapter? = null
    private lateinit var fabNewsDetailFav: FloatingActionButton

    private lateinit var fabSwipeFavLeft: FloatingActionButton
    private lateinit var fabSwipeFavRight: FloatingActionButton

    private lateinit var rlNoFavNews:RelativeLayout
    private lateinit var clFavourite : androidx.coordinatorlayout.widget.CoordinatorLayout

//    private lateinit var adViewBannerFav: AdView
    private var newsDbHelper: NewsDbHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        sharedPrefs = SharedPrefs(this@FavouriteActivity)
        if (sharedPrefs!!.isNightModeEnabled) {
            setTheme(R.style.AppTheme_Base_Night)
        } else {
            setTheme(R.style.AppTheme_Base_Light)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite)
        initViews()

        //Banner ads initialize here....
        /*MobileAds.initialize(this, getString(R.string.banner_search_unit));
        val adRequest = AdRequest.Builder().build()
        adViewBannerFav.loadAd(adRequest)*/
//        onShowLoading()

        newsDbHelper = NewsDbHelper(this@FavouriteActivity)
        setSupportActionBar(favToolbar)
        if (supportActionBar != null) {
            supportActionBar!!.title = resources.getString(R.string.favTitle)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        dbHelper = NewsDbHelper(this)
        articleList = dbHelper!!.allNewsData


        if(dbHelper!!.checkDBTable() > 0){
            swipeStackAdapter = NewsSwipeStackAdapter(this@FavouriteActivity,
                articleList!!, "FavouriteActivity")

            swipeStackLoader.adapter = swipeStackAdapter
            swipeStackAdapter!!.setShareNewsListener(this@FavouriteActivity)
            swipeStackAdapter!!.notifyDataSetChanged()
//            onHideLoading()
            rlNoFavNews.visibility = View.GONE
            fabNewsDetailFav.show()

            Log.e("checkDBTable", "true" + " :: "+dbHelper!!.checkDBTable())
        }else{
            Log.e("checkDBTable", "False")
            rlNoFavNews.visibility = View.VISIBLE
            fabNewsDetailFav.hide()
        }

        swipeStackLoader.setSwipeProgressListener(object  : SwipeStack.SwipeProgressListener{

            override fun onSwipeProgress(position: Int, progress: Float) {
                Log.e("progress", " :: $position")
                if(progress > 0.2 && progress<0.45){
                    showDeleteParticularNewsDialog(position)
                }
            }

            override fun onSwipeEnd(position: Int) {
                Log.e("progress", ""+position)
            }

            override fun onSwipeStart(position: Int) {
                Log.e("progress", ""+position)
                fabNewsDetailFav.show()
            }
        })

        swipeStackLoader.setListener(object : SwipeStack.SwipeStackListener {
            override fun onStackEmpty() {
                rlNoFavNews.visibility = View.VISIBLE
                fabNewsDetailFav.hide()
            }

            override fun onViewSwipedToLeft(position: Int) {
                Log.e("checkDBTable", "False")
            }

            override fun onViewSwipedToRight(position: Int) {
                Log.e("checkDBTable", "False")
            }
        })

        fabNewsDetailFav.setOnClickListener {
            Log.e("topview curntPos ", " :: "+swipeStackLoader.currentPosition)

            val currentPos = swipeStackLoader.currentPosition
            val articleModel = articleList!![currentPos]

            val intent =  Intent(this@FavouriteActivity, NewsDetailActivity::class.java)
            intent.putExtra("url", articleModel.url)
            intent.putExtra("title", articleModel.title)
            intent.putExtra("news_img", articleModel.urlToImage)
            intent.putExtra("date", articleModel.publishedAt)
            intent.putExtra("source_name",articleModel.source!!.name)
            intent.putExtra("author",articleModel.author)
            startActivity(intent)
        }

    }

    /*TapTarget.forBounds(rickTarget, "Down", ":^)")
                    .cancelable(false)

                    TapTarget.forView(findViewById(R.id.clearAllMenu), "Gonna"),
                    .icon(rick)*/

    private fun tapTargetViewImplement(viewMenuItem : View){
        fabSwipeFavLeft.show()
        fabSwipeFavRight.show()

        val tapTargetView = TapTargetSequence(this)
            .targets(
                TapTarget.forView(viewMenuItem, "Delete All Favourite News Article",
                    "After click on delete button. You don't see your favourites News Article Again.")
                    .outerCircleColor(R.color.colorPrimary)
                    .targetCircleColor(R.color.colorDimGrey)
                    .textColor(R.color.colorWhite)
                    .transparentTarget(true)
                    .descriptionTextColor(R.color.colorWhite)
                    .cancelable(false) ,

                TapTarget.forView(findViewById(R.id.fabSwipeFavLeft), "Left Swipe to see Next News Article",
                    "You can see next news article when card swipe on Left.")
                    .outerCircleColor(R.color.colorPrimary)
                    .targetCircleColor(R.color.colorDimGrey)
                    .textColor(R.color.colorWhite)
                    .transparentTarget(true)
                    .descriptionTextColor(R.color.colorWhite)
                    .cancelable(false) ,

                TapTarget.forView(
                    findViewById(R.id.fabSwipeFavRight), "Right Swipe to delete Article",
                    "You can delete current news article when card swipe on Right."
                )
                    .outerCircleColor(R.color.colorPrimary)
                    .targetCircleColor(R.color.colorDimGrey)
                    .textColor(R.color.colorWhite)
                    .transparentTarget(true)
                    .descriptionTextColor(R.color.colorWhite)
                    .cancelable(false),

                TapTarget.forView(findViewById(R.id.fabNewsDetailFav), "News Full Detail",
                    "You can see full detail of current article when you tap on this button.")
                    .outerCircleColor(R.color.colorPrimary)
                    .targetCircleColor(R.color.colorDimGrey)
                    .textColor(R.color.colorWhite)
                    .transparentTarget(true)
                    .descriptionTextColor(R.color.colorWhite)
                    .cancelable(false)
            )
            .listener(object : TapTargetSequence.Listener {

                override fun onSequenceStep(lastTarget: TapTarget?, targetClicked: Boolean) {

                }

                override fun onSequenceFinish() {
                    fabSwipeFavLeft.hide()
                    fabSwipeFavRight.hide()
                }

                override fun onSequenceCanceled(lastTarget: TapTarget) {
                    // Boo
                }
            })
        tapTargetView.start()
    }

    public override fun onPause() {
//        adViewBannerFav.pause()
        super.onPause()
    }

    public override fun onResume() {
        super.onResume()
//        adViewBannerFav.resume()
    }

    public override fun onDestroy() {
//        adViewBannerFav.destroy()
        super.onDestroy()
    }

  /*  private fun onShowLoading() {
        findViewById<View>(R.id.shimmerCardLoading).visibility = View.VISIBLE
    }

    private fun onHideLoading() {
        findViewById<View>(R.id.shimmerCardLoading).visibility = View.GONE

    }*/

    private fun initViews() {
        favToolbar = findViewById(R.id.favToolbar)
        swipeStackLoader = findViewById(R.id.swipeStackLoader)
//        adViewBannerFav = findViewById(R.id.adViewBannerFav)
        rlNoFavNews = findViewById(R.id.rlNoFavNews)
        clFavourite = findViewById(R.id.clFavourite)

        fabNewsDetailFav = findViewById(R.id.fabNewsDetailFav)
        fabSwipeFavLeft = findViewById(R.id.fabSwipeFavLeft)
        fabSwipeFavRight = findViewById(R.id.fabSwipeFavRight)

        //Share View News initialization here.....
        title = findViewById(R.id.titleShare)
        desc = findViewById(R.id.tvDescptShare)
        source = findViewById(R.id.sourceShare)
        author = findViewById(R.id.authorTV)
        publishedAt = findViewById(R.id.publishedAt)
        time = findViewById(R.id.timeShare)
        urlLink = findViewById(R.id.urlLinkShare)

        newsImgView = findViewById(R.id.newsImgViewShare)
        mProgBar = findViewById(R.id.progress_photo_load)
        rlScreenshotShare = findViewById(R.id.rlScreenshotShare)
        tvShareScreenShot = findViewById(R.id.tvShareScreenShot)
        tvShareScreenCancel = findViewById(R.id.tvShareScreenCancel)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.favourites_menu, menu)

       if(sharedPrefs?.favGuiderShow!!){
           Handler().post(Runnable {
               val viewItem:View = findViewById(R.id.clearAllMenu)
               tapTargetViewImplement(viewItem)
               sharedPrefs?.favGuiderShow = false
           })
       }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if(item?.itemId == R.id.clearAllMenu){
            if(dbHelper!!.checkDBTable() > 0){
                showDeleteNewsDialog()
            }else{
                Toast.makeText(this@FavouriteActivity, resources.getString(R.string.favNewsText), Toast.LENGTH_SHORT).show()
            }
        }else {
            rlNoFavNews.visibility = View.GONE
            swipeStackLoader.resetStack()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {

        if(rlScreenshotShare.visibility == View.VISIBLE){
            rlScreenshotShare.visibility = View.GONE
            fabNewsDetailFav.show()
        }else{
            super.onBackPressed()
        }
    }
    /* TapTargetView.showFor(this@BasicInformation, // `this` is an Activity
                TapTarget.forView(view, "You can tap here to get Chat Support")
                    // All options below are optional
                    .outerCircleColor(R.color.colorAccent)      // Specify a color for the outer circle
                    .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                    .targetCircleColor(R.color.white)   // Specify a color for the target circle
                    .titleTextSize(30)                  // Specify the size (in sp) of the title text
                    .titleTextColor(R.color.white)      // Specify the color of the title text
                    .textColor(R.color.white)            // Specify a color for both the title and description text
                    .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                    .dimColor(R.color.black)            // If set, will dim behind the view with 30% opacity of the given color
                    .drawShadow(true)                   // Whether to draw a drop shadow or not
                    .cancelable(true)                  // Whether tapping outside the outer circle dismisses the view
                    .tintTarget(true)                   // Whether to tint the target view's color
                    .transparentTarget(false)           // Specify whether the target is transparent (displays the content underneath)
                    .targetRadius(60), // Specify the target radius (in dp)
                object :
                    TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                    override fun onTargetClick(view: TapTargetView) {
                        super.onTargetClick(view)      // This call is optional
                        //doSomething();
                    }
                })*/
    private fun showDeleteNewsDialog(){
        val alertBuilder : AlertDialog.Builder = AlertDialog.Builder(this@FavouriteActivity)
        alertBuilder.setCancelable(false)

        val inflater = layoutInflater
        val deleteAllNewsView = inflater.inflate(R.layout.delete_all_news_dialog_layout, null)
        val tvDeleteYes:TextView = deleteAllNewsView.findViewById(R.id.tvDeleteYes)
        val tvDeleteNo:TextView = deleteAllNewsView.findViewById(R.id.tvDeleteNo)
        alertBuilder.setView(deleteAllNewsView)

        val alertDialog:AlertDialog  = alertBuilder.create();
        alertDialog.show()

        tvDeleteNo.setOnClickListener { alertDialog.dismiss() }

        tvDeleteYes.setOnClickListener {
            alertDialog.dismiss()

            newsDbHelper!!.deleteAllValues()
            swipeStackAdapter!!.notifyDataSetChanged()
            recreate()
        }
    }

    private fun showDeleteParticularNewsDialog(position : Int){

        Log.e("topview ", " :: "+swipeStackLoader.topView)

        Snackbar.make(clFavourite, resources.getString(R.string.deleteNewsArticle), Snackbar.LENGTH_LONG)
            .setAction(resources.getString(R.string.deleteText)) {
                dbHelper!!.deleteNewsData(articleList!!.get(position).id)
                recreate()
//                swipeStackLoader.resetStack()
            }
            .show()
        /*val alertBuilder : AlertDialog.Builder = AlertDialog.Builder(this@FavouriteActivity)
        alertBuilder.setCancelable(false)

        val inflater = layoutInflater
        val deleteAllNewsView = inflater.inflate(R.layout.delete_all_news_dialog_layout, null)
        val tvDeleteYes:TextView = deleteAllNewsView.findViewById(R.id.tvDeleteYes)
        val tvDeleteNo:TextView = deleteAllNewsView.findViewById(R.id.tvDeleteNo)
        alertBuilder.setView(deleteAllNewsView)

        val alertDialog:AlertDialog  = alertBuilder.create();
        alertDialog.show()

        tvDeleteNo.setOnClickListener { alertDialog.dismiss() }

        tvDeleteYes.setOnClickListener {
            alertDialog.dismiss()

            newsDbHelper!!.deleteAllValues()
            swipeStackAdapter!!.notifyDataSetChanged()
            recreate()
        }*/
    }
}
