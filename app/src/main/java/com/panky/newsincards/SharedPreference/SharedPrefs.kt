package com.panky.newsincards.SharedPreference

import android.content.Context
import android.content.SharedPreferences
import android.util.Log

import com.panky.newsincards.Constants.Constant
import java.text.SimpleDateFormat
import java.util.*

class SharedPrefs(mContext: Context) : Constant {

    private val newsPreference: SharedPreferences
    private val newsEditor: SharedPreferences.Editor

    // Date setting objects here....
    var c = Calendar.getInstance().time
    val calendarInstance = Calendar.getInstance()
    var df = SimpleDateFormat("yyyy-MM-dd", Locale.US)
    var currentFormattedDate = df.format(c)
    var dateValue:String? = null


    companion object {
       private const val prefs_name = "news_info"
        private const val mode = 0 }

    init {
        newsPreference = mContext.getSharedPreferences(prefs_name, mode)
        newsEditor = newsPreference.edit()
        newsEditor.apply()

        dateValue = (calendarInstance.get(Calendar.YEAR)).toString()+"-0"+
                (calendarInstance.get(Calendar.MONTH)+1)+"-"+(calendarInstance.get(Calendar.DAY_OF_MONTH)-1)
        Log.e("date :: ", dateValue+" :: "+(calendarInstance.get(Calendar.MONTH)+1))
    }

    fun Clear() {
        newsEditor.clear()
        newsEditor.commit()
    }

    var category: String?
        get() = newsPreference.getString("category", "business")
        set(catValue) {
            newsEditor.putString("category", catValue)
            newsEditor.commit()
        }

    var countryCode: String?
        get() = newsPreference.getString("countryCode", "in")
        set(countryCode) {
            newsEditor.putString("countryCode", countryCode)
            newsEditor.commit()
        }

    var countryName: String?
        get() = newsPreference.getString("countryName", "INDIA")
        set(countryName) {
            newsEditor.putString("countryName", countryName)
            newsEditor.commit()
        }

    var favouriteNews: Boolean
        get() = newsPreference.getBoolean("isFavNews", true)
        set(isCheckFav) {
            newsEditor.putBoolean("isFavNews", isCheckFav)
            newsEditor.commit()
        }

    var categoryPos: Int
        get() = newsPreference.getInt("categoryPos", 0)
        set(categoryPos) {
            newsEditor.putInt("categoryPos", categoryPos)
            newsEditor.commit()
        }

    var sourcePos: Int
        get() = newsPreference.getInt("sourcePos", 0)
        set(sourcePos) {
            newsEditor.putInt("sourcePos", sourcePos)
            newsEditor.commit()
        }

    var isNightModeEnabled: Boolean
        get() = newsPreference.getBoolean(Constant.NIGHT_MODE, false)
        set(state) {
            newsEditor.putBoolean(Constant.NIGHT_MODE, state)
            newsEditor.commit()
        }

    var selectDateFrom: String?
        get() = newsPreference.getString("dateFrom", currentFormattedDate)
        set(dateFrom) {
            newsEditor.putString("dateFrom", dateFrom)
            newsEditor.commit()
        }

    var selectDateTo: String?
        get() = newsPreference.getString("dateTo", dateValue)
        set(dateTo) {
            newsEditor.putString("dateTo", dateTo)
            newsEditor.commit()
        }

    var newsSortsBy : String?
        get()  = newsPreference.getString("newsSortBy", "publishedAt")
        set(sortBy) {
            newsEditor.putString("newsSortBy", sortBy)
            newsEditor.commit()
        }

    var countryPos : Int
        get()  = newsPreference.getInt("countryPos", -1)
        set(countryPos) {
            newsEditor.putInt("countryPos", countryPos)
            newsEditor.commit()
        }

    var langNameSetup : String?
        get()  = newsPreference.getString("langNameSetup", "")
        set(langNameSetup) {
            newsEditor.putString("langNameSetup", langNameSetup)
            newsEditor.commit()
        }

    var languagePos: Int
        get() = newsPreference.getInt("languagePos", -1)
        set(languagePos) {
            newsEditor.putInt("languagePos", languagePos)
            newsEditor.commit()
        }

    var langCountryCode: String?
        get() = newsPreference.getString("langCountryCode", "")
        set(langCountryCode) {
            newsEditor.putString("langCountryCode", langCountryCode)
            newsEditor.commit()
        }

    var langCode: String?
        get() = newsPreference.getString("langCode", "")
        set(langCode) {
            newsEditor.putString("langCode", langCode)
            newsEditor.commit()
        }

    var classReference: String?
        get() = newsPreference.getString("classRef", "")
        set(classRef) {
            newsEditor.putString("classRef", classRef)
            newsEditor.commit()
        }

    var favGuiderShow: Boolean?
        get() = newsPreference.getBoolean("favGuider", true)
        set(favGuider) {
            newsEditor.putBoolean("favGuider", favGuider?:false)
            newsEditor.commit()
        }

    var MainGuiderShow: Boolean?
        get() = newsPreference.getBoolean("mainGuider", true)
        set(mainGuider) {
            newsEditor.putBoolean("mainGuider", mainGuider?:false)
            newsEditor.commit()
        }
}
