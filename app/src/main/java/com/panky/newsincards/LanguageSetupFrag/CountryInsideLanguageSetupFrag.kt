package com.panky.newsincards.LanguageSetupFrag

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.database.*
import com.panky.newsincards.Adapter.CountryWithLangRVAdapter
import com.panky.newsincards.FinishUpDialogFrag
import com.panky.newsincards.LanguagePage
import com.panky.newsincards.R
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.models.CountryLangModel
import kotlin.collections.ArrayList

class CountryInsideLanguageSetupFrag() : androidx.fragment.app.Fragment(), CountryWithLangRVAdapter.countrySelectedListener,
    View.OnClickListener {

    internal lateinit var view: View
    private var layoutMangager: androidx.recyclerview.widget.RecyclerView.LayoutManager? = null
    private lateinit var rvCountrySetup: androidx.recyclerview.widget.RecyclerView
    private var countryLangModel : ArrayList<CountryLangModel>? = null
    private lateinit var pbLoadingData : ProgressBar

    private lateinit var dbReferenceCountry: DatabaseReference
    private lateinit var countryWithLandRVAdapter: CountryWithLangRVAdapter

    private lateinit var tvLangAction: TextView
    private lateinit var tvlanguageSelect: TextView
    private lateinit var sharedPrefs: SharedPrefs
    private var langValue:String? = null
    private var langName:String? = null

    @SuppressLint("ValidFragment")
    constructor(tvLangAction : TextView, tvlanguageSelect : TextView) : this(){
        this.tvLangAction = tvLangAction
        this.tvlanguageSelect = tvlanguageSelect
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        view = inflater.inflate(R.layout.fragment_country_inside_language_setup, container, false)
        initViews()
        setUpRecyclerView()

        sharedPrefs = SharedPrefs(context!!)
        countryLangModel = ArrayList()
        dbReferenceCountry = FirebaseDatabase.getInstance().reference.child("CountryData")

        tvLangAction.setOnClickListener(this)
        val bundle = this.arguments
        if(bundle != null){
            langValue = bundle.getString("langCode")
            langName = bundle.getString("langName")
            Log.e("langCode ", " :: $langValue")
            getCountryDataWithLangCode(langValue)
        }

        (activity as LanguagePage).changeTitle(CountryInsideLanguageSetupFrag())
        return view
    }

    override fun getPosition(position: Int) {
        val countryLang = countryLangModel!!.get(position)

        Log.e("country data ", ""+countryLang.CountryName+" :: "+countryLang.CountryCode)

        sharedPrefs.langCountryCode = countryLang.CountryCode
        sharedPrefs.langCode = langValue
        sharedPrefs.countryCode = countryLang.CountryCode
        sharedPrefs.countryName = countryLang.CountryName

        tvlanguageSelect.text = ""
        tvlanguageSelect.text = String.format(langName + " ("+ countryLang.CountryName+")")
    }

    override fun onClick(v: View?) {
       if(sharedPrefs.countryPos >= 0){
         val finishUpDialogFrag = FinishUpDialogFrag()
           finishUpDialogFrag.show(childFragmentManager, "Finish Up Dialog")
       }else{
           Toast.makeText(activity, resources.getString(R.string.selectCountry), Toast.LENGTH_SHORT).show()
       }
    }

    private fun getCountryDataWithLangCode(langValue: String?) {
        dbReferenceCountry.child(langValue?:"").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (postSnapshot in dataSnapshot.children) {

                    pbLoadingData.visibility = View.GONE
                    val countryLangModels:CountryLangModel = postSnapshot.getValue<CountryLangModel>(CountryLangModel::class.java)!!
                    countryLangModel?.add(countryLangModels)
                }

//                dataSnapshot.children.mapNotNullTo(countryLangModel) { it.getValue<CountryLangModel>(CountryLangModel::class.java) }
                countryWithLandRVAdapter = CountryWithLangRVAdapter(context!!, countryLangModel)
                rvCountrySetup.adapter = countryWithLandRVAdapter
                countryWithLandRVAdapter.setSelectedViewListener(this@CountryInsideLanguageSetupFrag)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                pbLoadingData.visibility = View.GONE
                Log.e("db error :: ", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        })

    }

    private fun initViews(){
        rvCountrySetup = view.findViewById(R.id.rvCountrySetup)
        pbLoadingData = view.findViewById(R.id.pbLoadingData)
    }

    private fun setUpRecyclerView(){
        layoutMangager =
            androidx.recyclerview.widget.GridLayoutManager(context!!, 2)
        rvCountrySetup.layoutManager = layoutMangager
        rvCountrySetup.setHasFixedSize(true)
        rvCountrySetup.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
    }
}
