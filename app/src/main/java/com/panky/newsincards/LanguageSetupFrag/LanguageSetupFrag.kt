package com.panky.newsincards.LanguageSetupFrag

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.panky.newsincards.Adapter.LangRecyclerViewAdapter
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.models.LangModel
import java.util.*
import kotlin.collections.ArrayList
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import android.widget.TextView
import android.view.animation.AnimationUtils.loadAnimation
import android.view.animation.Animation
import android.widget.Toast
import com.panky.newsincards.LanguagePage
import com.panky.newsincards.R

class LanguageSetupFrag() : androidx.fragment.app.Fragment(), View.OnClickListener, LangRecyclerViewAdapter.langSelectedListener{

    internal lateinit var view:View
    private lateinit var rvLanguageSetup: androidx.recyclerview.widget.RecyclerView
    private var langDataList : ArrayList<LangModel>? = null
    private var langListAdapter: LangRecyclerViewAdapter? = null

    private var layoutMangager: androidx.recyclerview.widget.RecyclerView.LayoutManager? = null
    private var langCode:String? = null
    private lateinit var sharedPrefs: SharedPrefs
    private lateinit var tvLangAction:TextView
    private lateinit var tvlanguageSelect:TextView
    private var slide_up : Animation?= null

    @SuppressLint("ValidFragment")
    constructor(tvLangAction : TextView, tvLangSelect:TextView) : this(){
        this.tvLangAction = tvLangAction
        this.tvlanguageSelect = tvLangSelect
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        view = inflater.inflate(R.layout.fragment_language_setup, container, false)
        initViews()

        //Load animation
        val slide_down = loadAnimation(
            activity,
            R.anim.slide_down)

         slide_up = loadAnimation(
            activity,
             R.anim.slide_up
        )

        tvlanguageSelect.startAnimation(slide_down)
        sharedPrefs = SharedPrefs(context!!)
        langDataList = ArrayList()
        prepareLangList()
        setUpRecyclerView()

        langListAdapter = LangRecyclerViewAdapter(context!!, langDataList)
        rvLanguageSetup.adapter = langListAdapter

        langListAdapter!!.setSelectedViewListener(this)
//        tvLangAction.visibility = View.GONE
        tvLangAction.setOnClickListener(this)

        Log.e("country code ", " :: "+ Locale.getDefault().country)
        Log.e("country lang ", " :: "+ Locale.getDefault().language)
        Log.e("country cd iso3 ", " :: "+ Locale.getDefault().isO3Country)

        (activity as LanguagePage).changeTitle(LanguageSetupFrag())
        return view
    }

    private fun initViews(){
        rvLanguageSetup = view.findViewById(R.id.rvLanguageSetup)
    }

    private fun setUpRecyclerView(){
        layoutMangager = androidx.recyclerview.widget.LinearLayoutManager(
            context!!,
            LinearLayout.VERTICAL,
            false
        )
        rvLanguageSetup.layoutManager = layoutMangager
        rvLanguageSetup.setHasFixedSize(true)
        rvLanguageSetup.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
    }

    override fun getPosition(position: Int) {

        tvLangAction.visibility = View.VISIBLE
        tvlanguageSelect.visibility = View.VISIBLE
        tvlanguageSelect.startAnimation(slide_up)

        val langModelValue:LangModel = langDataList!!.get(position)
        langCode = langModelValue.langCode

        sharedPrefs.langCode = langCode
        sharedPrefs.langNameSetup = langModelValue.langName
//        updateResources(context!!, langCode!!)
        Log.e("listener value ", " :: "+langModelValue.langName)
        tvlanguageSelect.text = langModelValue.langName
    }

    private fun sendMessage() {
        val intent = Intent("custom-event-name")
        intent.putExtra("message", "your message")
        androidx.localbroadcastmanager.content.LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent)
    }

    override fun onClick(v: View?) {

        val textValue = tvLangAction.text.toString()
        if(textValue.equals(resources.getString(R.string.nextLang), true)){

           if(sharedPrefs.languagePos >= 0){
               val bundle = Bundle()
               bundle.putString("langCode", sharedPrefs.langCode)
               bundle.putString("langName", tvlanguageSelect.text.toString())

               val countryFragment =
                   CountryInsideLanguageSetupFrag(tvLangAction, tvlanguageSelect)
               countryFragment.arguments = bundle
               sharedPrefs.countryPos = -1

               (activity as LanguagePage).onFragmentReplace(countryFragment, false)
               Log.e("langCode Lang", " :: "+langCode+" :: "+sharedPrefs.langCountryCode)
           }else {
               Toast.makeText(activity, resources.getString(R.string.toastLang), Toast.LENGTH_SHORT).show()
           }
        }
    }

    fun prepareLangList(){
        langDataList?.add(LangModel("Arabic", "ar", "العربية"))
        langDataList?.add(LangModel("Chinese Traditional", "zh", "中文 (Zhōngwén), 汉语, 漢語"))
        langDataList?.add(LangModel("Dutch", "nl", "Nederlands, Vlaams"))
        langDataList?.add(LangModel("English", "en", "UK, USA, India"))
        langDataList?.add(LangModel("French", "fr", "français, langue française"))
        langDataList?.add(LangModel("German", "de", "Deutsch"))
        langDataList?.add(LangModel("Hebrew", "he", "עברית"))
        langDataList?.add(LangModel("Italian", "it", "Italiano"))
//        langDataList?.add(LangModel("Northern Sami", "se", "Davvisámegiella"))
        langDataList?.add(LangModel("Norwegian Bokmål", "no", "Norsk"))
//        langDataList?.add(LangModel("Pakistan", "ud", "Urdu (اردو)", "pk"))
        langDataList?.add(LangModel("Portuguese", "pt", "Português"))
        langDataList?.add(LangModel("Russian", "ru", "русский язык"))
        langDataList?.add(LangModel("Spanish", "es", "español, castellano"))
    }/*"español, castellano"*/

}
