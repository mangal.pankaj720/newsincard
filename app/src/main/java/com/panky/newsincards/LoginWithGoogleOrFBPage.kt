package com.panky.newsincards

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.cardview.widget.CardView
import com.google.firebase.auth.FirebaseAuth
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import android.content.Intent
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.*
import com.facebook.*
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.GoogleAuthProvider
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.firebase.auth.FirebaseUser
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.github.ybq.android.spinkit.style.Circle
import com.github.ybq.android.spinkit.style.FadingCircle
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.auth.FacebookAuthProvider
import com.panky.newsincards.Constants.Constants
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.models.UserData
import com.panky.newsincards.view.MainPage.view.MainActivity
import java.util.*

class LoginWithGoogleOrFBPage : AppCompatActivity(), View.OnClickListener,
    GoogleApiClient.OnConnectionFailedListener {

    private val TAG:String = "LoginWithGoogleOrFBPage"
    private lateinit var cvLoginGoogle: androidx.cardview.widget.CardView
    private lateinit var cvLoginFacebook: androidx.cardview.widget.CardView
    private lateinit var tvSkipLoginSetup : TextView
    private lateinit var imgBackLogin : ImageView
    private val RC_SIGN_IN = 9001
    private var mAuth: FirebaseAuth? = null
    private var mAuthListener: FirebaseAuth.AuthStateListener? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    var name: String? = null
    var email:String? = null
    var idToken: String? = null

    private var boolean_google: Boolean = false
    private var callbackManager:CallbackManager?=null
    private var sharedPrefs : SharedPrefs?= null
    private lateinit var pbFBLoader:ProgressBar
    private lateinit var pbGoogleLoader:ProgressBar
    private lateinit var rlGoogleLayout:RelativeLayout
    private lateinit var rlGoogleLoader:RelativeLayout
    private lateinit var rlFBLayout:RelativeLayout
    private lateinit var rlFBLoader:RelativeLayout
    private var viewValue:String?=null
    private lateinit var chkPolicy:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_with_google_or_fbpage)
        initViews()

        //call Facebook onclick on your customized button on click by the following
        FacebookSdk.sdkInitialize(this.applicationContext)
        callbackManager = CallbackManager.Factory.create()

        //Shared Preference object here
        sharedPrefs = SharedPrefs(this@LoginWithGoogleOrFBPage)

        mAuth = FirebaseAuth.getInstance()
        mAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user != null) {
                // User is signed in
                Log.e(TAG, "onAuthStateChanged:signed_in:" + user.uid)
            } else {
                // User is signed out
                Log.e(TAG, "onAuthStateChanged:signed_out")
            }
        }

        val txtPrivacy = "I agree to Privacy Policy"
        val spannableString = SpannableString(txtPrivacy)
        val clickableSpan =  object : ClickableSpan() {

             override fun onClick(textView:View) {
                val intent = Intent(this@LoginWithGoogleOrFBPage, PrivacyPolicyPage::class.java)
//                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
//                finish()
            }

             override fun updateDrawState(ds:TextPaint) {
                super.updateDrawState(ds)
                 ds.isUnderlineText = true
            }
        }
        spannableString.setSpan(clickableSpan, 11, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        chkPolicy.text = spannableString
        chkPolicy.movementMethod = LinkMovementMethod.getInstance()
        chkPolicy.highlightColor = Color.BLUE

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.web_client_id))
            .requestEmail()
            .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this@LoginWithGoogleOrFBPage /* FragmentActivity */, this /* OnConnectionFailedListener */)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()

        cvLoginGoogle.setOnClickListener(this)
        cvLoginFacebook.setOnClickListener(this)
        tvSkipLoginSetup.setOnClickListener(this)
        imgBackLogin.setOnClickListener(this)

        val circle = Circle()
        pbGoogleLoader.indeterminateDrawable = circle
        val fadingCircle = FadingCircle()
        pbFBLoader.indeterminateDrawable = fadingCircle

        val intentValue = intent
        viewValue = intentValue.getStringExtra("displayView")
        if(viewValue.equals("MainActivity")){
            imgBackLogin.visibility  =  View.VISIBLE
            tvSkipLoginSetup.visibility = View.GONE
            sharedPrefs!!.classReference = "LoginWithGoogleOrFBMain"
        }else {
            imgBackLogin.visibility  =  View.GONE
            tvSkipLoginSetup.visibility = View.VISIBLE
            sharedPrefs!!.classReference = "LoginWithGoogleOrFBPage"
        }
    }

    private fun initViews(){
        cvLoginGoogle = findViewById(R.id.llLoginGoogle)
        cvLoginFacebook = findViewById(R.id.llLoginFacebook)
        tvSkipLoginSetup = findViewById(R.id.tvSkipLoginSetup)
        imgBackLogin = findViewById(R.id.imgBackLogin)

        pbGoogleLoader = findViewById(R.id.pbGoogleLoader)
        rlGoogleLayout = findViewById(R.id.rlGoogleLayout)
        rlGoogleLoader = findViewById(R.id.rlGoogleLoader)

        pbFBLoader = findViewById(R.id.pbFBLoader)
        rlFBLayout = findViewById(R.id.rlFBLayout)
        rlFBLoader = findViewById(R.id.rlFBLoader)
        chkPolicy = findViewById(R.id.chkPolicy)
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d(TAG, "handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    val user = mAuth!!.currentUser

                    val userData = UserData()
                    userData.name = user!!.displayName
                    userData.email = user.email
                    userData.photoUrl = user.photoUrl.toString()
                    userData.socialType = "facebook"
                    Constants.userDataObject.userData = userData

                    Log.e(TAG, "fb result :: " +user.email + " :: "+user.displayName+ " :: "+user.photoUrl +
                    " :: "+ user.phoneNumber)

                    val image_url:String = "http://graph.facebook.com/" + Profile.getCurrentProfile().getId() + "/picture?type=large"
                    Log.e("url ", " :: "+image_url)

                    rlFBLoader.visibility = View.GONE
                    rlFBLayout.visibility = View.VISIBLE

                    gotoLanguagePage()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    Toast.makeText(baseContext, resources.getString(R.string.errorAuthFailed),
                        Toast.LENGTH_SHORT).show()

                    rlFBLoader.visibility = View.GONE
                    rlFBLayout.visibility = View.VISIBLE
//                    updateUI(null)
                }

                // ...
            }.addOnFailureListener(OnFailureListener {

                rlFBLoader.visibility = View.GONE
                rlFBLayout.visibility = View.VISIBLE
            })
    }

    override fun onClick(v: View?) {
        when(v!!.id){

            R.id.tvSkipLoginSetup -> {
                gotoLanguagePage()
            }

            R.id.imgBackLogin -> {
                val intentMainActivity = Intent(this@LoginWithGoogleOrFBPage, MainActivity::class.java)
                startActivity(intentMainActivity)
            }

            R.id.llLoginGoogle -> {
                signIn()
            }

            R.id.llLoginFacebook -> {
               /* LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_photos", "email", "public_profile", "user_posts" , "AccessToken"))
                LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"))*/

                rlFBLoader.visibility = View.VISIBLE
                rlFBLayout.visibility = View.GONE

                LoginManager.getInstance()
                    .logInWithReadPermissions(this@LoginWithGoogleOrFBPage, Arrays.asList("email", "public_profile"))

                LoginManager.getInstance().registerCallback(callbackManager,
                    object : FacebookCallback<LoginResult> {
                        override fun onSuccess(loginResult: LoginResult) {
                            Log.d("Success", "Login")


                            handleFacebookAccessToken(loginResult.accessToken)
                            rlFBLoader.visibility = View.GONE
                            rlFBLayout.visibility = View.VISIBLE
                        }

                        override fun onCancel() {
                            rlFBLoader.visibility = View.GONE
                            rlFBLayout.visibility = View.VISIBLE
                            Toast.makeText(this@LoginWithGoogleOrFBPage, "Login Cancel", Toast.LENGTH_LONG).show()
                        }

                        override fun onError(exception: FacebookException) {
                            rlFBLoader.visibility = View.GONE
                            rlFBLayout.visibility = View.VISIBLE
                            Toast.makeText(this@LoginWithGoogleOrFBPage, exception.message, Toast.LENGTH_LONG).show()
                        }
                    })
            }
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {

            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            if(task.isSuccessful){
                try {
                    val account = task.getResult(ApiException::class.java)
                    firebaseAuthWithGoogle(account!!)

                } catch (e: ApiException) {
                    Log.e(TAG, "signInResult:failed code=" + e.statusCode)
                }
            }
        }else{
            // Pass the activity result back to the Facebook SDK
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.e("LoginActivity", "firebaseAuthWithGoogle:" + acct.id!!+" :: token id "+acct.idToken)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                Log.e("LoginActivity", "signInWithCredential:onComplete:" + task.isSuccessful)

                if(task.isSuccessful){

                    rlGoogleLoader.visibility = View.GONE
                    rlGoogleLayout.visibility = View.VISIBLE

                    val firebaseUser : FirebaseUser = mAuth!!.currentUser!!
                    Log.e(TAG, "signInResult : " +firebaseUser.email + " :: "+firebaseUser.displayName+ " :: "+firebaseUser.photoUrl)

                    val userData = UserData()
                    userData.name = firebaseUser.displayName
                    userData.email = firebaseUser.email
                    userData.photoUrl = firebaseUser.photoUrl.toString()
                    userData.socialType = "google"
                    Constants.userDataObject.userData = userData

                    gotoLanguagePage()

                }else {
                    Log.e("LoginActivity", "signInWithCredential", task.exception)
                    Toast.makeText(
                        this@LoginWithGoogleOrFBPage, resources.getString(R.string.errorAuthFailed),
                        Toast.LENGTH_SHORT
                    ).show()

                    rlGoogleLoader.visibility = View.GONE
                    rlGoogleLayout.visibility = View.VISIBLE
                }
            }.addOnFailureListener(OnFailureListener {
                rlGoogleLoader.visibility = View.GONE
                rlGoogleLayout.visibility = View.VISIBLE
            })
    }

    private fun signOut() {
        // Firebase sign out
        mAuth!!.signOut()

        // Google sign out
        Auth.GoogleSignInApi.signOut(mGoogleApiClient)
    }
    private fun signIn() {
        rlGoogleLoader.visibility = View.VISIBLE
        rlGoogleLayout.visibility = View.GONE

        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.e("LoginActivity", "onConnectionFailed:$connectionResult")
        Toast.makeText(this, resources.getString(R.string.errorPlayService), Toast.LENGTH_SHORT).show()
    }

    private fun gotoLanguagePage() {

        if(viewValue.equals("MainActivity")){
            val intentMain = Intent(this@LoginWithGoogleOrFBPage, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

           /* if(Constants.userDataObject.userData.socialType.equals("google")){
                signOut()
            }else{

            }*/
            startActivity(intentMain)
            finish()
        }else {
            val intentProceed = Intent(this@LoginWithGoogleOrFBPage, LanguagePage::class.java)
            intentProceed.putExtra("getType","login")
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            /*if(Constants.userDataObject.userData.socialType.equals("google")){
                signOut()
            }else{

            }*/
            startActivity(intentProceed)
            finish()
        }
    }

    public override fun onStart() {
        super.onStart()
        mAuth?.addAuthStateListener(mAuthListener!!)
    }

    public override fun onStop() {
        super.onStop()
        if (mAuthListener != null) {
            mAuth?.removeAuthStateListener(mAuthListener!!)
        }
    }
}
