package com.panky.newsincards

import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import com.panky.newsincards.Adapter.BoardingPagerAdapter
import com.panky.newsincards.models.BoardingScreenData
import java.util.*
import kotlin.collections.ArrayList

class OnBoardingScreenPage : AppCompatActivity(), androidx.viewpager.widget.ViewPager.OnPageChangeListener {

    private var timer : Timer? =null

    private lateinit var viewPagerBoardingScreen : androidx.viewpager.widget.ViewPager
    private lateinit var boardingPagerAdapter: BoardingPagerAdapter
    private lateinit var boardingDataList : ArrayList<BoardingScreenData>
    private lateinit var tlIndicator: TabLayout
    private lateinit var tvNextPager:TextView
    private lateinit var tvSkipBoarding : TextView

    override fun onCreate(savedInstanceState: Bundle?) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = Color.parseColor("#88adadad")
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding_screen_page)
        initViews()

       prepareBoardingScreenData()

        Log.e("list size ", " :: "+boardingDataList.size)
        boardingPagerAdapter = BoardingPagerAdapter(this@OnBoardingScreenPage, boardingDataList)
        viewPagerBoardingScreen.adapter = boardingPagerAdapter
        tlIndicator.setupWithViewPager(viewPagerBoardingScreen)

        viewPagerBoardingScreen.addOnPageChangeListener(this)
        var currentPage = -1
        // start auto scroll of viewpager
        val handler = Handler()
        val Update = Runnable {
            viewPagerBoardingScreen.setCurrentItem(++currentPage, true)
            // go to initial page i.e. position 0
            if (currentPage == boardingPagerAdapter.count - 1) {
                currentPage = -1
                // ++currentPage will make currentPage = 0
            }
        }

        timer = Timer() // This will create a new Thread
        timer!!.schedule(object : TimerTask() { // task to be scheduled

            override fun run() {
                handler.post(Update)
            }
        }, 4000, 3000)

        tvSkipBoarding.setOnClickListener {
            gotoLanguagePage()
        }

        tvNextPager.setOnClickListener {

            val btnText = tvNextPager.text.toString()
            if(btnText.equals(resources.getString(R.string.proceedPage))){
               gotoLanguagePage()
            }else {
                if(viewPagerBoardingScreen.currentItem == 0){
                    viewPagerBoardingScreen.setCurrentItem(1, true)
                }else if(viewPagerBoardingScreen.currentItem == 1){
                    viewPagerBoardingScreen.setCurrentItem(2, true)
                }else if(viewPagerBoardingScreen.currentItem == 2){
                    viewPagerBoardingScreen.setCurrentItem(3, true)
                }else if(viewPagerBoardingScreen.currentItem == 3){
                    tvNextPager.text = resources.getString(R.string.proceedPage)
                    tvSkipBoarding.visibility = View.INVISIBLE
                }
            }

        }
    }

    private fun initViews() {
        viewPagerBoardingScreen = findViewById(R.id.viewPagerBoardingScreen)
        tlIndicator = findViewById(R.id.tlIndicator)
        tvNextPager = findViewById(R.id.tvNextPager)
        tvSkipBoarding = findViewById(R.id.tvSkipBoarding)

    }

    private fun gotoLanguagePage(){
        val intenLang = Intent(this@OnBoardingScreenPage, LoginWithGoogleOrFBPage::class.java)
        intenLang.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra("displayView", "boarding")
        startActivity(intenLang)
        finish()
    }

    private fun prepareBoardingScreenData(){
        boardingDataList = ArrayList()
        boardingDataList.add(BoardingScreenData(resources.getString(R.string.boardPageTitle1), resources.getString(R.string.boardPageDesc1), R.drawable.smartphone))
        boardingDataList.add(BoardingScreenData(resources.getString(R.string.boardPageTitle2), resources.getString(R.string.boardPageDesc2), R.drawable.boarding_discover))
        boardingDataList.add(BoardingScreenData(resources.getString(R.string.boardPageTitle3), resources.getString(R.string.boardPageDesc3), R.drawable.boarding_knowledge_source))
        boardingDataList.add(BoardingScreenData(resources.getString(R.string.boardPageTitle4), resources.getString(R.string.boardPageDesc4), R.drawable.boarding_news_storage))
    }

    override fun onPageScrollStateChanged(p0: Int) {
        Log.e("", "")
    }

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
        if(p0 == 0){
            tvNextPager.text = resources.getString(R.string.nextpage)
            tvSkipBoarding.visibility = View.VISIBLE
        }else if(p0 == 1){
            tvNextPager.text = resources.getString(R.string.nextpage)
            tvSkipBoarding.visibility = View.VISIBLE
        }else if(p0 == 2){
            tvNextPager.text = resources.getString(R.string.nextpage)
            tvSkipBoarding.visibility = View.VISIBLE
        }else if(p0 == 3){
            tvNextPager.text = resources.getString(R.string.proceedPage)
            tvSkipBoarding.visibility = View.INVISIBLE
        }
    }

    override fun onPageSelected(p0: Int) {
        Log.e("", "")
    }
}
