package com.panky.newsincards

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
//import com.google.android.gms.ads.AdRequest
//import com.google.android.gms.ads.AdView
//import com.google.android.gms.ads.MobileAds

import com.panky.newsincards.SharedPreference.SharedPrefs

class NotificationActivity : AppCompatActivity() {

    private val TAG : String = NotificationActivity::class.java.simpleName
    private var mToolbar: Toolbar? = null
    private var sharedPrefs: SharedPrefs? = null
    private var btnRetry: Button? = null

//    private lateinit var adViewBannerNotification: AdView

    override fun onCreate(savedInstanceState: Bundle?) {
        sharedPrefs = SharedPrefs(this@NotificationActivity)

        Log.e("$TAG :: ", "onCreate Called")
        if (sharedPrefs!!.isNightModeEnabled) {
            setTheme(R.style.AppTheme_Base_Night)
        } else {
            setTheme(R.style.AppTheme_Base_Light)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        initViews()

        //Banner ads initialize here....
//        MobileAds.initialize(this, getString(R.string.banner_search_unit));
//        val adRequest = AdRequest.Builder().build()
//        adViewBannerNotification.loadAd(adRequest)

        setSupportActionBar(mToolbar)
        if (supportActionBar != null) {
            supportActionBar!!.title = "Notification"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

    }


    override fun onStart() {
        super.onStart()

        Log.e("$TAG :: ", "onStart Called")
    }


    public override fun onPause() {

        Log.e("$TAG :: ", "onPause Called")
//        adViewBannerNotification.pause()
        super.onPause()
    }

    public override fun onResume() {
        Log.e("$TAG :: ", "onResume Called")
        super.onResume()
//        adViewBannerNotification.resume()
    }

    public override fun onDestroy() {

        Log.e("$TAG :: ", "onDestroy Called")
//        adViewBannerNotification.destroy()
        super.onDestroy()
    }

    override fun onStop() {
        super.onStop()

        Log.e("$TAG :: ", "onStop Called")
    }

    override fun onRestart() {
        super.onRestart()

        Log.e("$TAG :: ", "onRestart Called")
    }

    private fun initViews() {
        mToolbar = findViewById(R.id.custom_toolbar)
        btnRetry = findViewById(R.id.btnRetry)

//        adViewBannerNotification = findViewById(R.id.adViewBannerNotification)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.news_setting_forall_activity, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.settingNews) {

            startActivity(Intent(this@NotificationActivity, SettingActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }
}
