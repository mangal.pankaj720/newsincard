package com.panky.newsincards

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.panky.newsincards.LanguageSetupFrag.CountryInsideLanguageSetupFrag
import com.panky.newsincards.LanguageSetupFrag.LanguageSetupFrag
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.view.MainPage.view.MainActivity

class LanguagePage : AppCompatActivity() {

    private val TAG : String = LanguagePage::class.java.simpleName
    private lateinit var langToolbar:Toolbar
    private var fragmentManager: androidx.fragment.app.FragmentManager? = null
    private lateinit var tvlanguageToolbarTitle:TextView
    private lateinit var tvlanguageSelect:TextView
    private lateinit var tvLangAction:TextView
    private lateinit var imgBackLang:ImageView

    private lateinit var sharedPrefs : SharedPrefs

    override fun onCreate(savedInstanceState: Bundle?) {

        sharedPrefs = SharedPrefs(this@LanguagePage)

        Log.e("$TAG :: ", "onCreate Called")
        if (sharedPrefs.isNightModeEnabled) {
            setTheme(R.style.AppTheme_Base_Night)
        } else {
            setTheme(R.style.AppTheme_Base_Light)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language_page)
        initViews()

        //Shared Preference object here
        sharedPrefs = SharedPrefs(this@LanguagePage)

//        tvLangAction.visibility = View.GONE
        fragmentManager = supportFragmentManager
        setSupportActionBar(langToolbar)

        val intentValue = intent
        val type = intentValue.getStringExtra("getType")
        if(type.equals("login")){
            imgBackLang.visibility = View.GONE
            sharedPrefs.classReference = "LanguagePage"
            onFragmentReplace(LanguageSetupFrag(tvLangAction, tvlanguageSelect), true)
        }else if(type.equals("changeLang")){
            imgBackLang.visibility = View.VISIBLE
            sharedPrefs.classReference = "MainLang"
            onFragmentReplace(LanguageSetupFrag(tvLangAction, tvlanguageSelect), true)
        }else{
            imgBackLang.visibility = View.VISIBLE
            sharedPrefs.classReference = "MainLang"
            onFragmentReplace(LanguageSetupFrag(tvLangAction, tvlanguageSelect), true)
        }

        imgBackLang.setOnClickListener {
            val intentLang = Intent(this@LanguagePage, MainActivity::class.java)
            intentLang.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intentLang)
            finish()
        }
    }

    private fun initViews() {
        langToolbar = findViewById(R.id.langToolbar)
        tvlanguageToolbarTitle = findViewById(R.id.tvlanguageToolbarTitle)
        tvlanguageSelect = findViewById(R.id.tvlanguageSelect)
        tvLangAction = findViewById(R.id.tvLangAction)

        imgBackLang = findViewById(R.id.imgBackLang)
    }

    fun onFragmentReplace(fragment: androidx.fragment.app.Fragment, flag: Boolean) {

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        if (flag) {
            if (!supportFragmentManager.popBackStackImmediate(supportFragmentManager.javaClass.name, 0)) {
                fragmentTransaction.add(R.id.flLanguageContainer, fragment, fragment.javaClass.name)
                fragmentTransaction.addToBackStack(supportFragmentManager.javaClass.name)
                fragmentTransaction.commit()
            }
        } else {
            //            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.flLanguageContainer, fragment, fragment.javaClass.name)
            fragmentTransaction.addToBackStack(supportFragmentManager.javaClass.name)
            fragmentTransaction.commit()
        }
    }

    fun changeTitle(fragment: androidx.fragment.app.Fragment){

        if(fragment is LanguageSetupFrag){
           tvlanguageToolbarTitle.text = resources.getString(R.string.langTitle)
            tvLangAction.text = resources.getString(R.string.nextLang)
            tvlanguageSelect.visibility = View.VISIBLE
            tvlanguageSelect.text = sharedPrefs.langNameSetup
        }else if(fragment is CountryInsideLanguageSetupFrag){
            tvlanguageToolbarTitle.text = resources.getString(R.string.countryTitle)
            tvLangAction.text = resources.getString(R.string.finishLangSetup)
        }
    }

    fun changeLanguage(){

        val intent = Intent(this@LanguagePage, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (fragmentManager!!.backStackEntryCount > 1) {
            fragmentManager!!.popBackStack()
        }else{
//            val intentCountry = Intent(this@LanguagePage, MainActivity::class.java)
//            intentCountry.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK

//            recreate()
//            startActivity(intentCountry)
            finish()
//            super.onBackPressed()
//            onFragmentReplace(LanguageSetupFrag(tvLangAction, tvlanguageSelect), true)

            //moveTaskToBack(true)
        }


        /*else {
            // Otherwise, ask user if he wants to leave :)
            AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes) { arg0, arg1 ->
                    // MainActivity.super.onBackPressed();

                    moveTaskToBack(true)
                    finish()
                }.create().show()
        }*/
    }
}


/*English - en/
Norwegian - no/
Italian - it/
Arabic - ar/
pakistan - ud/
German - de/
Portuguese - pt/
Hebrew (modern) - he/
Russian - ru/
Northern Sami - se/
French - fr/
Spanish - es
Dutch - nl
Chinese - zh*/


/*ar de en es fr he it nl no pt ru se ud zh*/