package com.panky.newsincards.models

class CountryLangModel() {

    var CountryName:String? = null
    var CountryCode:String? = null
    var ImageUrl:String? = null

    constructor(CountryName: String?, CountryCode: String?, ImageUrl: String?) : this() {
        this.CountryName = CountryName
        this.CountryCode = CountryCode
        this.ImageUrl = ImageUrl
    }
}