package com.panky.newsincards.models

class UserData {

    var name : String?=null
    var email : String?=null
    var photoUrl : String?=null
    var socialType: String? = null
}