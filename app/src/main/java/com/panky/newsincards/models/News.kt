package com.panky.newsincards.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class News {

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("totalResults")
    @Expose
    var totalResults: Int = 0

    @SerializedName("articles")
    @Expose
    var articleList: MutableList<Article>? = null
}
