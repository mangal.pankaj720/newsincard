package com.panky.newsincards.models

import android.os.Parcel
import android.os.Parcelable

class languagesModel : Parcelable {

    var lang_name: String? = null
    var langCode: String? = null
    var countryName: String? = null
    var langNativeName: String? = null

    constructor(lang_name: String, langCode: String, countryName: String, langNativeName: String) {
        this.lang_name = lang_name
        this.langCode = langCode
        this.countryName = countryName
        this.langNativeName = langNativeName
    }

    protected constructor(`in`: Parcel) {
        lang_name = `in`.readString()
        langCode = `in`.readString()
        countryName = `in`.readString()
        langNativeName = `in`.readString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(lang_name)
        dest.writeString(langCode)
        dest.writeString(countryName)
        dest.writeString(langNativeName)
    }

    companion object {

        val CREATOR: Parcelable.Creator<languagesModel> = object : Parcelable.Creator<languagesModel> {
            override fun createFromParcel(`in`: Parcel): languagesModel {
                return languagesModel(`in`)
            }

            override fun newArray(size: Int): Array<languagesModel?> {
                return arrayOfNulls(size)
            }
        }
    }

    object CREATOR : Parcelable.Creator<languagesModel> {
        override fun createFromParcel(parcel: Parcel): languagesModel {
            return languagesModel(parcel)
        }

        override fun newArray(size: Int): Array<languagesModel?> {
            return arrayOfNulls(size)
        }
    }
}
