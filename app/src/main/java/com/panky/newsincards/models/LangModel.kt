package com.panky.newsincards.models

class LangModel {

    var langName:String? = null
    var langCode:String? = null
    var nativeName:String? = null
//    var countryCode:String? = null

    constructor(langName:String, langCode:String, nativeName:String){
        this.langName = langName
        this.langCode = langCode
        this.nativeName = nativeName
//        this.countryCode = countryCode
    }
}