package com.panky.newsincards.models

import android.os.Parcel
import android.os.Parcelable

class countryModel : Parcelable {

    var countryName: String? = null
    var countryCode: String? = null

    private constructor(`in`: Parcel) : super() {
        readFromParcel(`in`)
    }

    constructor(countryName: String, countryCode: String) {
        this.countryName = countryName
        this.countryCode = countryCode
    }

    private fun readFromParcel(`in`: Parcel) {
        countryName = `in`.readString()
        countryCode = `in`.readString()

    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(countryName)
        dest.writeString(countryCode)
    }

    companion object {
        val CREATOR: Parcelable.Creator<countryModel> = object : Parcelable.Creator<countryModel> {
            override fun createFromParcel(`in`: Parcel): countryModel {
                return countryModel(`in`)
            }

            override fun newArray(size: Int): Array<countryModel?> {
                return arrayOfNulls(size)
            }
        }
    }

     object CREATOR : Parcelable.Creator<countryModel> {
        override fun createFromParcel(parcel: Parcel): countryModel {
            return countryModel(parcel)
        }

        override fun newArray(size: Int): Array<countryModel?> {
            return arrayOfNulls(size)
        }
    }
}
