package com.panky.newsincards.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Sources {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("sources")
    @Expose
    var sources: MutableList<Source>? = null

    inner class Source(
            @field:SerializedName("id")
                       @field:Expose
                       var id: String?, @field:SerializedName("name")
                       @field:Expose
                       var name: String?, @field:SerializedName("description")
                       @field:Expose
                       var description: String?, @field:SerializedName("url")
                       @field:Expose
                       var url: String?, @field:SerializedName("category")
                       @field:Expose
                       var category: String?, @field:SerializedName("language")
                       @field:Expose
                       var language: String?, @field:SerializedName("country")
                       @field:Expose
                       var country: String?)
}
