package com.panky.newsincards.models

class BoardingScreenData {

    var title : String?=null
    var desc : String?=null
    var imageUrl : Int?=null

    constructor(title: String?, desc: String?, imageUrl: Int?) {
        this.title = title
        this.desc = desc
        this.imageUrl = imageUrl
    }
}