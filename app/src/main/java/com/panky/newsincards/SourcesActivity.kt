package com.panky.newsincards

import androidx.annotation.StyleRes
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
//import com.google.android.gms.ads.AdRequest
//import com.google.android.gms.ads.InterstitialAd

import com.panky.newsincards.Adapter.NewsSourcesRecyclerAdapter
import com.panky.newsincards.BottomSheetFrag.BottomSheetDataFragment
import com.panky.newsincards.BottomSheetFrag.BottomSheetFragment
import com.panky.newsincards.Constants.Constant
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.api.ApiClient
import com.panky.newsincards.api.ApiInterface
import com.panky.newsincards.models.Sources
import com.panky.newsincards.models.countryModel
import com.panky.newsincards.models.languagesModel

import org.json.JSONException
import org.json.JSONObject

import java.io.IOException
import java.util.ArrayList

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SourcesActivity : AppCompatActivity(), Constant {

    private var newsSourcesNameRV: androidx.recyclerview.widget.RecyclerView? = null
    private var layoutManager: androidx.recyclerview.widget.RecyclerView.LayoutManager? = null
    private var sourcesRecyclerAdapter: NewsSourcesRecyclerAdapter? = null

    private var newsSourcesList: MutableList<Sources.Source>? = null
    private var errorLayout: RelativeLayout? = null
    private var errorImageView: ImageView? = null
    private var errorTitle: TextView? = null
    private var errorMessage: TextView? = null
    private var retryButton: Button? = null
    private var sourceNotAvailableRL : RelativeLayout? = null

    private var mToolbar: Toolbar? = null
    private var sharedPrefs: SharedPrefs? = null
    private var countryModelList: ArrayList<countryModel>? = null
    private var languageModelList : ArrayList<languagesModel>? = null
    private var tvsourceNot : TextView? = null
    lateinit var btnRetryLoadCountry : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        sharedPrefs = SharedPrefs(this@SourcesActivity)

        if (sharedPrefs!!.isNightModeEnabled) {
            setAppTheme(R.style.AppTheme_Base_Night)
        } else {
            setAppTheme(R.style.AppTheme_Base_Light)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sources)
        initViews()
        countryModelList = ArrayList()
        languageModelList = ArrayList()

//        Log.e("", " "+loadSourcesFromRetrofit("", "", ""))



        setSupportActionBar(mToolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setTitle(R.string.sourceTab)
        }

        layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(this@SourcesActivity)
        newsSourcesNameRV!!.layoutManager = layoutManager
        newsSourcesNameRV!!.isNestedScrollingEnabled = false
        newsSourcesNameRV!!.setHasFixedSize(true)
        newsSourcesNameRV!!.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        if(newsSourcesList.isNullOrEmpty()){
            loadSourcesFromRetrofit("", sharedPrefs!!.countryCode!!, sharedPrefs!!.countryName!!)
        }

        btnRetryLoadCountry.setOnClickListener {
            sourceNotAvailableRL?.visibility = View.GONE
            errorLayout?.visibility = View.GONE

            showCountryBottomSheetDialog()
        }

        try {
            val obj = JSONObject(readJSONFromAsset())
            val langObj = JSONObject(readJSONFromAssetLang())
            val langArray = langObj.getJSONArray("language")
            val countryArray = obj.getJSONArray("countries")
            Log.e("lang Array ::", ""+langArray.length())
            Log.e("json error msg :: ", "" + countryArray.length())

            for (i in 0 until countryArray.length()) {

                val mainObject = countryArray.getJSONObject(i)
                val country_ID = mainObject.getJSONObject("country").getString("country_id")
                val country_Name = mainObject.getJSONObject("country").getString("country_name")

                countryModelList!!.add(countryModel(country_Name, country_ID))
                Log.e("json error msg :: ", "$country_ID :: $country_Name")
            }

            for (i in 0 until langArray.length()){
                val jsonObjectLang = langArray.getJSONObject(i)

                val lang_name = jsonObjectLang.getString("lang_name")
                val lang_code = jsonObjectLang.getString("lang_code")
                val countName = jsonObjectLang.getString("country")
                val nativeName = jsonObjectLang.getString("nativeName")

                languageModelList!!.add(languagesModel(lang_name, lang_code, countName, nativeName))
                Log.e("lang name ", " :: "+jsonObjectLang.getString("lang_name")+" :: "+jsonObjectLang.getString("country"))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    private fun readJSONFromAsset(): String? {
        val json: String
        try {
            val `is` = assets.open("country_code.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer, charset("UTF_8"))
        } catch (ex: IOException) {
            ex.printStackTrace()
            Log.e("json error msg :: ", "" + ex.message)
            return null
        }

        return json
    }

    private fun readJSONFromAssetLang(): String? {
        val json: String
        try {
            val `is` = assets.open("language.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer, charset("UTF_8"))
        } catch (ex: IOException) {
            ex.printStackTrace()
            Log.e("json error msg :: ", "" + ex.message)
            return null
        }

        return json
    }

    private fun setAppTheme(@StyleRes style: Int) {
        setTheme(style)
    }

    private fun initViews() {
        newsSourcesNameRV = findViewById(R.id.newsSourcesNameRV)
        errorLayout = findViewById(R.id.errorLayout)
        errorImageView = findViewById(R.id.errorImage)
        errorTitle = findViewById(R.id.errorTitle)
        errorMessage = findViewById(R.id.errorMessage)
        retryButton = findViewById(R.id.btnRetry)

        mToolbar = findViewById(R.id.custom_toolbar)
        sourceNotAvailableRL = findViewById(R.id.sourceNotAvailableRL)
        tvsourceNot = findViewById(R.id.sourceNotPlease)
        btnRetryLoadCountry = findViewById(R.id.btnRetryLoadCountry)
        //        bottom_sheet_nestedSV = findViewById(R.id.bottom_sheet_nestedSV);
    }

    private fun onShowShimmerLoading(){
        findViewById<View>(R.id.sourceShimmerLoadingLayout).visibility = View.VISIBLE
    }

    private fun onHideShimmerLoading(){
        findViewById<View>(R.id.sourceShimmerLoadingLayout).visibility = View.GONE
    }

    //    When Internet is not Available then this layout is appear ....
    private fun showErrorMessage(ImgId: Int, title: String, msg: String) {

        if (errorLayout!!.visibility == View.GONE) {
            errorLayout!!.visibility = View.VISIBLE
        }

        errorImageView!!.setImageResource(ImgId)
        errorTitle!!.text = title
        errorMessage!!.text = msg

        retryButton!!.setOnClickListener { loadSourcesFromRetrofit("", sharedPrefs!!.countryCode!!, "") }
    }

    fun loadSourcesFromRetrofit(lang: String, code_name: String, countryName : String) {

        sourceNotAvailableRL?.visibility = View.GONE
        errorLayout?.visibility = View.GONE

        onShowShimmerLoading()
        val apiInterface = ApiClient.apiClient.create<ApiInterface>(ApiInterface::class.java)
        /*val sourcesCall = apiInterface.getNewsSourcesName(lang, code_name, Constant.API_KEY)

        sourcesCall.enqueue(object : Callback<Sources> {
            override fun onResponse(call: Call<Sources>, response: Response<Sources>) {

                if (response.isSuccessful && response.body()!!.sources != null) {

                    onHideShimmerLoading()
                    Log.e("source totl size ", ""+response.body()!!.sources?.size)
                    Log.e("respose :: ", "" + response)
                    if (!newsSourcesList.isNullOrEmpty()) {
                        newsSourcesList!!.clear()
                    }

                    if(response.body()?.sources?.size == 0){

                        val sb = StringBuilder()

                        sourceNotAvailableRL!!.visibility = View.VISIBLE
                        tvsourceNot?.text = sb.append(resources.getString(R.string.sourceNot)).append(" ").append(countryName).toString()
                    }else{
                        newsSourcesList = response.body()?.sources
                        val values = ArrayList<String>()
                        val hashSet = HashSet<String>()
                        val values2 = ArrayList<String>()
                        val hashSet2 = HashSet<String>()

                        for (i in 0 until newsSourcesList!!.size){


                            values2.add(newsSourcesList!![i].language.toString())
                            hashSet2.addAll(values2)
                            values2.clear()
                            values2.addAll(hashSet2)


                            values.add(newsSourcesList!![i].country.toString())
                            hashSet.addAll(values)
                            values.clear()
                            values.addAll(hashSet)
//                            Log.e("language :: ", ""+newsSourcesList!![i].language+":: country :: "+newsSourcesList!![i].country)
                        }

                        Log.e("country size :: ", ""+values.size)
                        Log.e("country size Lang:: ", ""+values2.size)
                        for (j in 0 until values.size){
                            Log.e("country :: ", ""+values[j])

                        }
                        for (j in 0 until values2.size){

                            Log.e("language :: ", ""+values2[j])
                        }
                        sourcesRecyclerAdapter = NewsSourcesRecyclerAdapter(this@SourcesActivity, newsSourcesList!!)
                        newsSourcesNameRV?.adapter = sourcesRecyclerAdapter
                        sourcesRecyclerAdapter?.notifyDataSetChanged()
                    }
                } else {
                    val errorCode: String
                    Log.e("Error Code :: ", "" + response.code())
                    when (response.code()) {
                        404 -> {
                            errorCode = "404 not found"
                        }
                        500 -> {
                            errorCode = "500 server broken"
                        }
                        else -> errorCode = "Unknown error"
                    }
                    errorLayout?.visibility = View.VISIBLE
                    sourceNotAvailableRL!!.visibility = View.GONE
                    onHideShimmerLoading()
                    showErrorMessage(R.drawable.no_result, "No Result", "Please Try Again \n$errorCode")
                }
            }

            override fun onFailure(call: Call<Sources>, t: Throwable) {

                Log.e("Error :: ", "" + t.toString())
                errorLayout?.visibility = View.VISIBLE
                sourceNotAvailableRL!!.visibility = View.GONE
                onHideShimmerLoading()
                showErrorMessage(R.drawable.oops, "Oops...", "Network Failure, \nPlease Try Again ")
            }
        })*/
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.news_forall_activity, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun showCountryBottomSheetDialog(){
        //Show the Bottom Sheet Fragment
        val bundle = Bundle()
        Log.e("countModel size :: ", "" + countryModelList!!.size)
        bundle.putParcelableArrayList("countryList", countryModelList)
        val bottomSheetDialogFragment = BottomSheetDataFragment()

        bottomSheetDialogFragment.arguments = bundle
        bottomSheetDialogFragment.show(this@SourcesActivity.supportFragmentManager,
                "Bottom Sheet Data Dialog Fragment !!")
    }

    private fun showLanguageBottomSheetDialog(){
        //Show the Bottom Sheet Fragment
        val bundle = Bundle()
        Log.e("countModel size :: ", "" + languageModelList!!.size)
        bundle.putParcelableArrayList("languageList", languageModelList)
        val bottomSheetDialogFragment = BottomSheetFragment()

        bottomSheetDialogFragment.arguments = bundle
        bottomSheetDialogFragment.show(this@SourcesActivity.supportFragmentManager,
                "Bottom Sheet Dialog Fragment !!")
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {

            R.id.searchFilterByCountry -> {
                showCountryBottomSheetDialog()
            }

            R.id.searchFilterByLanguage -> {
                showLanguageBottomSheetDialog()
            }
        }
        /* if(item.getItemId() == R.id.searchFilter){


//            bottomSheetDialogFragment.show(getSupportFragmentManager(), "Bottom Sheet Dialog Fragment");



           *//* if(mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                mBottomSheetBehavior.setPeekHeight(150);
            }else {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                mBottomSheetBehavior.setPeekHeight(0);
            }*//*
        }*/
        return super.onOptionsItemSelected(item)
    }
}
