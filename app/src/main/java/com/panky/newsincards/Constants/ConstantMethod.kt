package com.panky.newsincards.Constants

import android.content.Context
import com.panky.newsincards.R
import com.panky.newsincards.SharedPreference.SharedPrefs

object ConstantMethod {

    private var sharedPrefs: SharedPrefs? = null

    fun initSharedPrefs(context: Context) {
        sharedPrefs = SharedPrefs(context)
        if (sharedPrefs!!.isNightModeEnabled) {
            context.setTheme(R.style.AppTheme_Base_Night)
        } else {
            context.setTheme(R.style.AppTheme_Base_Light)
        }
    }

    /*private fun setAppTheme(@StyleRes style: Int) {
        setTheme(style)
    }*/

    /*SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView)menu.findItem(R.id.searchView).getActionView();
        MenuItem menuItem = menu.findItem(R.id.searchView);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Search Latest News....");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query.length()>2){
                    onLoadingSwipeRefresh(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                loadJsonData(newText, sharedPrefs.getCategory());
                return false;
            }
        });

        menuItem.getIcon().setVisible(false, false);*/
}