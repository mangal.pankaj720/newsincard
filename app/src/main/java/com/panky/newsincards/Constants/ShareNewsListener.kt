package com.panky.newsincards.Constants

interface ShareNewsListener {
    fun getShareNewsPosition(position: Int)
}