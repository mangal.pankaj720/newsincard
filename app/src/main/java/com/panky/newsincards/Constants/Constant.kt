package com.panky.newsincards.Constants

interface Constant {
    companion object {

//        const val API_KEY = "7e7f37c73c9f496aad1531d152399e0a"

        const val SOURCE_ID = "source_id"
        const val SOURCE_NAME = "source_name"
        const val NIGHT_MODE = "night_mode"

        const val NEWS_ID = "newsID"
        const val TABLE_NAME = "news_data_table"
        const val TABLE_NAME_CHANGE = "news_articles_table"
        const val NEWS_TITLE = "title"
        const val NEWS_DESCRIPTION = "description"
        const val NEWS_IMAGE_URL = "image_url"
        const val NEWS_WEB_URL = "web_url"
        const val NEWS_SOURCE_NAME = "source_name"
        const val NEWS_SOURCE_ID = "source_id"
        const val NEWS_AUTHOR = "author"
        const val NEWS_PUBLISHED_AT = "publishedAt"
        const val PERMISSION_REQUEST_CODE = 9001
    }
}
