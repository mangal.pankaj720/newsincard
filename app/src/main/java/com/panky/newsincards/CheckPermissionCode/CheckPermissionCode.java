package com.panky.newsincards.CheckPermissionCode;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.core.content.ContextCompat;

import java.io.File;

public class CheckPermissionCode {

    private Context context;
    private int PERMISSION_REQUEST_CODE = 3;
    File imagePath;
    String path;

    public CheckPermissionCode(Context context) {
        this.context = context;
    }

    private void shareIt() {

        if(checkPermission()) {

//            Uri uri = Uri.fromFile(path);
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("image/*");
            /*String shareBody = "Screen shared";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "My Catch score");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);*/
//            sharingIntent.putExtra(Intent.EXTRA_STREAM, path);

//            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }else {
            requestPermission();
        }
    }


    private boolean checkPermission(){

        int result = ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE);

        if(result == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        else {
            return false;
        }
    }

    private void requestPermission() {
//        if(ActivityCompat.shouldShowRequestPermissionRationale(context, android.Manifest.permission.READ_EXTERNAL_STORAGE)){
//            ActivityCompat.requestPermissions(context, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
//        /}else {
//            ActivityCompat.requestPermissions(context, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
//        }
    }


//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//
//        if(requestCode == PERMISSION_REQUEST_CODE) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//               /* Bitmap bitmap = takeScreenshot();
//                saveBitmap(bitmap);*/
//                shareIt();
//            } else {
//                Log.e("value", "Permission Denied, You cannot use Storage Local Drive. ");
//            }
//        }
//    }
}
