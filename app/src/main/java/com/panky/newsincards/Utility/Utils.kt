package com.panky.newsincards.Utility

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import com.panky.newsincards.SharedPreference.SharedPrefs

import org.ocpsoft.prettytime.PrettyTime

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.Random

class Utils(mContext:Context) {

    private val pattern1 = "yyyy-MM-dd'T'HH:mm:ss"
    private val pattern3 = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    private val pattern2 = "yyyy-MM-dd'T'HH:mm:ss.sss'Z'"
    private val sharedPrefs = SharedPrefs(mContext)

    private val vibrantLightColorList = arrayOf(ColorDrawable(Color.parseColor("#ffeead")),
        ColorDrawable(Color.parseColor("#93cfb3")), ColorDrawable(Color.parseColor("#fd7a7a")),
        ColorDrawable(Color.parseColor("#faca5f")), ColorDrawable(Color.parseColor("#1ba798")),
        ColorDrawable(Color.parseColor("#6aa9ae")), ColorDrawable(Color.parseColor("#ffbf27")),
        ColorDrawable(Color.parseColor("#d93947")))

    val randomDrawbleColor: ColorDrawable
        get() {
            val idx = Random().nextInt(vibrantLightColorList.size)
            return vibrantLightColorList[idx]
        }

    val country: String
        get() {
//            val locale = sharedPrefs.langCode
            var country=""
            if(sharedPrefs.langCode.equals("en")){
                country = "us"
            }else if(sharedPrefs.langCode.equals("ar")){
                country = "ar"
            }else if(sharedPrefs.langCode.equals("fr")){
                country = "fr"
            }else if(sharedPrefs.langCode.equals("zh")){
                country = "cn"
            } else{
                country = sharedPrefs.countryCode.toString()
            }

            Log.e("util lang ", " :: ${sharedPrefs.langCode}")
            Log.e("util country ", " :: $country")
            return country.toLowerCase()
        }

    //        String locale = context.getResources().getConfiguration().locale.getDisplayName();
    //        String locale = java.util.Locale.getDefault().getDisplayName();
    val language: String
        get() = sharedPrefs.langCode!!

    private fun isValid(value: String): Boolean {
        try {
            SimpleDateFormat(pattern1).parse(value)
            return true
        } catch (e: ParseException) {
            return false
        }
    }

    fun DateToTimeFormat(oldstringDate: String): String? {
        val p = PrettyTime(Locale(country))
        var isTime: String? = null
        try {
            val sdf: SimpleDateFormat
            if (isValid(oldstringDate)) {
                Log.e("old string 1:: ", "" + oldstringDate)
                sdf = SimpleDateFormat(pattern1, Locale("in"))
            } else {
                Log.e("old string 1 else:: ", "" + oldstringDate)
                sdf = SimpleDateFormat(pattern2, Locale("in"))
            }
            //            2019-03-04T07:27:20Z"
            val date = sdf.parse(oldstringDate)
            isTime = p.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return isTime
    }

    fun DateFormat(oldstringDate: String): String {
        var newDate: String
        val dateFormat = SimpleDateFormat("E, d MMM yyyy", Locale(country))
        try {
            if (isValid(oldstringDate)) {
                Log.e("old string 2:: ", "" + oldstringDate)
                val date = SimpleDateFormat(pattern1).parse(oldstringDate)
                newDate = dateFormat.format(date)
            } else {
                Log.e("old string 2 else:: ", "" + oldstringDate)
                val date = SimpleDateFormat(pattern2).parse(oldstringDate)
                newDate = dateFormat.format(date)
            }
        } catch (e: ParseException) {
            e.printStackTrace()
            newDate = oldstringDate
        }
        return newDate
    }
}
