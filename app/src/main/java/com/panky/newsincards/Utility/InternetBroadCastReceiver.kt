package com.panky.newsincards.Utility

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.panky.newsincards.Utils.ConnectionStatus

class InternetBroadCastReceiver() : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val connectivity = ConnectionStatus.CheckInternetConnection(context)
        if (!connectivity) {
            Log.e("connection :: "+connectivity, ""+connectivity)
        } else {
            Log.e("connection :: ", ""+connectivity)
        }
    }
}