package com.panky.newsincards

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.ProgressBar
import com.github.ybq.android.spinkit.style.CubeGrid
import android.view.WindowManager
import android.os.Build
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.panky.newsincards.Constants.Constants
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.models.UserData
import com.panky.newsincards.view.MainPage.view.MainActivity
import java.util.*

class NewsSplashPage : AppCompatActivity() {

    private lateinit var pbSplashLoader:ProgressBar
    private lateinit var sharedPrefs: SharedPrefs
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//            window.statusBarColor = resources.getColor(R.color.colorPrimary)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_splash_page)
        initViews()

        mAuth = FirebaseAuth.getInstance()
        //Shared Preference object here
        sharedPrefs = SharedPrefs(this@NewsSplashPage)

        val cubeGrid = CubeGrid()
        pbSplashLoader.indeterminateDrawable = cubeGrid

       getClassReference()
    }

    private fun initViews(){
        pbSplashLoader = findViewById(R.id.pbSplashLoader)
    }

    private fun getClassReference(){

        if(sharedPrefs.classReference.equals("LoginWithGoogleOrFBPage", true)){

            val mainIntent = Intent(this, LoginWithGoogleOrFBPage::class.java)
            mainIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(mainIntent)
            finish()
        }else if(sharedPrefs.classReference.equals("LanguagePage")){
            val mainIntent = Intent(this, LanguagePage::class.java)
            mainIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(mainIntent)
            finish()

        }else if(sharedPrefs.classReference.equals("MainActivity")){
            val mainIntent = Intent(this, MainActivity::class.java)

            val user = mAuth!!.currentUser
            Log.e("userData ", " :: "+user)

            if(user != null){
                val userData = UserData()
                userData.name = user.displayName
                userData.email = user.email
                userData.photoUrl = user.photoUrl.toString()
                userData.socialType = "google"

                updateResources(this@NewsSplashPage, sharedPrefs.langCode!!)
                Constants.userDataObject.userData = userData
                mainIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(mainIntent)
                finish()
            }else{
                updateResources(this@NewsSplashPage, sharedPrefs.langCode!!)
                mainIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(mainIntent)
                finish()
            }


        }else if((sharedPrefs.classReference.equals("MainLang")) or sharedPrefs.classReference.equals("LoginWithGoogleOrFBMain")){

            val mainIntent = Intent(this, MainActivity::class.java)
            val user = mAuth!!.currentUser
            Log.e("userData ", " :: "+user)

            if(user != null){
                val userData = UserData()
                userData.name = user.displayName
                userData.email = user.email
                userData.photoUrl = user.photoUrl.toString()
                userData.socialType = "google"

                updateResources(this@NewsSplashPage, sharedPrefs.langCode!!)
                Constants.userDataObject.userData = userData
                mainIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(mainIntent)
                finish()
            }else{
                updateResources(this@NewsSplashPage, sharedPrefs.langCode!!)
                mainIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(mainIntent)
                finish()
            }
        } else {
            Handler().postDelayed({
                /* Create an Intent that will start the Menu-Activity. */
                val mainIntent = Intent(this, OnBoardingScreenPage::class.java)
                mainIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(mainIntent)
                finish()

//            pbSplashLoader.visibility = View.GONE
            }, 4000)
        }
    }

    private fun updateResources(context: Context, language: String) {
        val locale = Locale(language)
        Locale.setDefault(locale)

        val resources = context.resources
        val configuration = resources.configuration

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale)
        } else {
            configuration.locale = locale
        }

        resources.updateConfiguration(configuration, resources.displayMetrics)
    }
}
