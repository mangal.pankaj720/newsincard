package com.panky.newsincards.BottomSheetFrag

import android.annotation.SuppressLint
import android.app.Dialog
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.coordinatorlayout.widget.CoordinatorLayout
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import com.panky.newsincards.Adapter.languageDataAdapter
import com.panky.newsincards.R
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.SourcesActivity

import com.panky.newsincards.models.languagesModel

import java.util.ArrayList

class BottomSheetFragment : BottomSheetDialogFragment() {

    private var contentView: View? = null
    private var languageList: ArrayList<languagesModel>? = null
    private var sharedPrefs: SharedPrefs? = null
    lateinit var etSearchLang : EditText
    lateinit var tvNoSearchFound : TextView
    private var lvLanguageData: ListView? = null
    private var languageDataAdapter : languageDataAdapter? = null

    //Bottom Sheet Callback
    private val mBottomSheetBehaviorCallback = object : BottomSheetBehavior.BottomSheetCallback() {

        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss()
            }
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {}
    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        //Get the content View
        sharedPrefs = SharedPrefs(context!!)
        contentView = View.inflate(context, R.layout.filter_source_by_language, null)
        dialog.setContentView(contentView!!)
        initViews()

        val extras = arguments
        if(extras != null){
            languageList = extras.getParcelableArrayList("languageList")
            Log.e("Bottom size :: ", "" + languageList!!.size)

            languageDataAdapter = languageDataAdapter(context!!, languageList!!, tvNoSearchFound)
            lvLanguageData?.adapter = languageDataAdapter
        }

        //enables filtering for the contents of the given ListView
        lvLanguageData?.setTextFilterEnabled(true)

        lvLanguageData!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->

            val model = parent.getItemAtPosition(position) as languagesModel
            Log.e("country :: ", ""+view.id+model.countryName+"" + languageList!![position].lang_name + " :: " + languageList!![position].countryName)
            (activity as SourcesActivity).loadSourcesFromRetrofit( model.langCode.toString(),"", model.countryName.toString())
//            sharedPrefs!!.countryCode =model.countryCode
//            sharedPrefs!!.countryName =model.countryName
            dismiss()
        }

        //Set the coordinator layout behavior
        val params = (contentView!!.parent as View).layoutParams as androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams
        val behavior = params.behavior
        (contentView!!.parent as View).setBackgroundColor(resources.getColor(android.R.color.transparent))

        //Set callback
        if (behavior != null) {
            if (behavior is BottomSheetBehavior<*>) {
                behavior.setBottomSheetCallback(mBottomSheetBehaviorCallback)

                etSearchLang.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                    }

                    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                        behavior.peekHeight = Int.MAX_VALUE
                        languageDataAdapter!!.filter.filter(s.toString())
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })
            }
        }
    }

    private fun initViews() {
        lvLanguageData = contentView!!.findViewById(R.id.lvLanguageData)
        etSearchLang = contentView!!.findViewById(R.id.etSearchLanguage)
        tvNoSearchFound = contentView!!.findViewById(R.id.tvNoSearchFoundLang)
    }
}
