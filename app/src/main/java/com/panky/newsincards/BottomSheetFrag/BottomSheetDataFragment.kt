package com.panky.newsincards.BottomSheetFrag

import android.annotation.SuppressLint
import android.app.Dialog
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.coordinatorlayout.widget.CoordinatorLayout
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.ListView
import android.widget.TextView

import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.models.countryModel
import com.panky.newsincards.Adapter.countryDataAdapter
import com.panky.newsincards.R
import com.panky.newsincards.SourcesActivity
import java.util.ArrayList

class BottomSheetDataFragment : BottomSheetDialogFragment() {

    private var contentView: View? = null
    private var lvCountryData: ListView? = null
    private var countryList: ArrayList<countryModel>? = null
    private var countryDataAdapter: countryDataAdapter? = null
    private var sharedPrefs: SharedPrefs? = null
    lateinit var etSearchCountry : EditText
    lateinit var tvNoSearchFound : TextView

    //Bottom Sheet Callback
    private val mBottomSheetBehaviorCallback = object : BottomSheetBehavior.BottomSheetCallback() {

        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss()
            }
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {

            // React to dragging events
            Log.e("onSlide", "onSlide " + slideOffset);
        }
    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        //Get the content View
        sharedPrefs = SharedPrefs(context!!)
        contentView = View.inflate(context, R.layout.filter_source_by_country, null)
        dialog.setContentView(contentView!!)
        initViews()

        val extras = arguments
        if (extras != null) {
            countryList = extras.getParcelableArrayList("countryList")
            Log.e("Bottom size 2:: ", "" + countryList!!.size)
            countryDataAdapter = countryDataAdapter(context!!, countryList!!, tvNoSearchFound)
            lvCountryData!!.adapter = countryDataAdapter
        }

        //enables filtering for the contents of the given ListView
        lvCountryData?.setTextFilterEnabled(true)


        lvCountryData!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->

            val model = parent.getItemAtPosition(position) as countryModel
            Log.e("country :: ", ""+view.id+model.countryName+"" + countryList!![position].countryCode + " :: " + countryList!![position].countryName)
            (activity as SourcesActivity).loadSourcesFromRetrofit("", model.countryCode.toString(), model.countryName.toString())
            sharedPrefs!!.countryCode =model.countryCode
            sharedPrefs!!.countryName =model.countryName
            dismiss()
        }
        //Set the coordinator layout behavior
        val params = (contentView!!.parent as View).layoutParams as androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams
        val behavior = params.behavior
        (contentView!!.parent as View).setBackgroundColor(resources.getColor(android.R.color.transparent))

        //Set callback
        if (behavior != null) {
            if (behavior is BottomSheetBehavior<*>) {
                behavior.setBottomSheetCallback(mBottomSheetBehaviorCallback)

                etSearchCountry.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                    }

                    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                        behavior.peekHeight = Int.MAX_VALUE
                        countryDataAdapter!!.filter.filter(s.toString())
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })
            }
        }

    }

    private fun initViews() {
        lvCountryData = contentView!!.findViewById(R.id.lvCountryData)
        etSearchCountry = contentView!!.findViewById(R.id.etSearchCountry)
        tvNoSearchFound = contentView!!.findViewById(R.id.tvNoSearchFound)
    }
}
