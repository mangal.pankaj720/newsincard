package com.panky.newsincards.BottomSheetFrag

import android.annotation.SuppressLint
import android.app.Dialog
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.coordinatorlayout.widget.CoordinatorLayout
import android.util.Log
import android.view.View
import android.widget.*

import com.panky.newsincards.SharedPreference.SharedPrefs
import android.app.DatePickerDialog
import androidx.cardview.widget.CardView
import java.util.*
import java.text.SimpleDateFormat
import android.widget.RadioButton
import android.widget.RadioGroup
import com.panky.newsincards.R
import com.panky.newsincards.SearchActivity

@SuppressLint("ValidFragment")
class SettingBottomSheetFragment : BottomSheetDialogFragment(), View.OnClickListener {

    private var contentView: View? = null
    private var sharedPrefs: SharedPrefs? = null
    private lateinit var imgSelectDateFrom:ImageView
    private lateinit var imgSelectDateTo:ImageView
    private lateinit var tvDateFrom:TextView
    private lateinit var tvDateTo:TextView
    private lateinit var rgSortBy:RadioGroup
    private lateinit var myCalendarFrom: Calendar
    private lateinit var myCalendarTo: Calendar
    private lateinit var dateFrom:DatePickerDialog.OnDateSetListener
    private lateinit var dateTo:DatePickerDialog.OnDateSetListener
    private lateinit var radioButton:RadioButton
    private lateinit var rdPopularity:RadioButton
    private lateinit var rdPublishedAt:RadioButton
    private lateinit var rdRelevancy:RadioButton
    private lateinit var btnOkCardView: androidx.cardview.widget.CardView
    private lateinit var imgClose:ImageView

    //Bottom Sheet Callback
    private val mBottomSheetBehaviorCallback = object : BottomSheetBehavior.BottomSheetCallback() {

        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss()
            }
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {

            // React to dragging events
            Log.e("onSlide", "onSlide $slideOffset");
        }
    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        //Get the content View
        sharedPrefs = SharedPrefs(context!!)
        contentView = View.inflate(context, R.layout.setting_bottom_sheet_layout, null)
        dialog.setContentView(contentView!!)
        initViews()

        imgSelectDateFrom.setOnClickListener(this)
        imgSelectDateTo.setOnClickListener(this)
        btnOkCardView.setOnClickListener(this)
        imgClose.setOnClickListener(this)

        myCalendarFrom = Calendar.getInstance();
        dateFrom = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            myCalendarFrom.set(Calendar.YEAR, year)
            myCalendarFrom.set(Calendar.MONTH, monthOfYear)
            myCalendarFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabelFrom()
        }

        myCalendarTo = Calendar.getInstance();
        dateTo = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            myCalendarTo.set(Calendar.YEAR, year)
            myCalendarTo.set(Calendar.MONTH, monthOfYear)
            myCalendarTo.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabelTo()
        }

        rgSortBy.setOnCheckedChangeListener { group, checkedId ->
            radioButton = contentView!!.findViewById(checkedId) as RadioButton
            var radioValue = radioButton.text.toString()

            if(radioValue.equals(resources.getString(R.string.stPopularity), true)){
                radioValue = "Popularity"
            }else if(radioValue.equals(resources.getString(R.string.stpublishedAt), true)){
                radioValue = "PublishedAt"
            }else if(radioValue.equals(resources.getString(R.string.stRelevancy), true)){
                radioValue = "Relevancy"
            }
//            val radioIndex = rgSortBy.checkedRadioButtonId
//            sharedPrefs!!.newsSortByID = radioIndex
            val mainRadioValue = radioValue.substring(0, 1).toLowerCase() + radioValue.substring(1)
            sharedPrefs!!.newsSortsBy = mainRadioValue
            Log.e("radioBtn value ::", mainRadioValue+" :: " + sharedPrefs!!.newsSortsBy)
        }

        tvDateFrom.text = sharedPrefs!!.selectDateFrom
        tvDateTo.text = sharedPrefs!!.selectDateTo
        //Set the coordinator layout behavior

        //        Log.e("value :: ", ""+sharedPrefs!!.newsSortsBy)
//        Log.e("value :: ", ""+resources.getString(R.string.stPopularity)+" :: ")
        if(sharedPrefs!!.newsSortsBy.equals(resources.getString(R.string.stPopularity), true)){
            rdPopularity.isChecked = true
        }else if(sharedPrefs!!.newsSortsBy.equals(resources.getString(R.string.stpublishedAt), true)){
            rdPublishedAt.isChecked = true
        }else{
            rdRelevancy.isChecked = true
        }

        val params = (contentView!!.parent as View).layoutParams as androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams
        val behavior = params.behavior
        (contentView!!.parent as View).setBackgroundColor(resources.getColor(android.R.color.transparent))

        //Set callback
        if (behavior != null) {
            if (behavior is BottomSheetBehavior<*>) {
                behavior.setBottomSheetCallback(mBottomSheetBehaviorCallback)
            }
        }
    }

    private fun initViews() {
        imgSelectDateFrom = contentView!!.findViewById(R.id.imgSelectDateFrom)
        imgSelectDateTo = contentView!!.findViewById(R.id.imgSelectDateTo)
        tvDateFrom = contentView!!.findViewById(R.id.tvDateFrom)
        tvDateTo = contentView!!.findViewById(R.id.tvDateTo)
        rgSortBy = contentView!!.findViewById(R.id.rgSortBy)

        rdPopularity = contentView!!.findViewById(R.id.rdPopularity)
        rdPublishedAt = contentView!!.findViewById(R.id.rdPublishedAt)
        rdRelevancy = contentView!!.findViewById(R.id.rdRelevancy)
        btnOkCardView = contentView!!.findViewById(R.id.btnOkCardView)
        imgClose = contentView!!.findViewById(R.id.imgClose)
    }

    override fun onClick(v: View?) {
        when(v?.id){

            R.id.imgSelectDateFrom -> {

                DatePickerDialog(context!!, dateFrom, myCalendarFrom.get(Calendar.YEAR), myCalendarFrom.get(Calendar.MONTH),
                        myCalendarFrom.get(Calendar.DAY_OF_MONTH)).show()
            }

            R.id.imgSelectDateTo -> {
                DatePickerDialog(context!!, dateTo, myCalendarTo.get(Calendar.YEAR), myCalendarTo.get(Calendar.MONTH),
                        myCalendarTo.get(Calendar.DAY_OF_MONTH)).show()
            }

            R.id.btnOkCardView ->{

                val extras = arguments
                val queryValue = extras?.getString("queryValue").toString()
                if(!queryValue.isEmpty()){
                    Log.e("allValues :: ", "From :: "+sharedPrefs!!.selectDateFrom+ " To :: "+sharedPrefs!!.selectDateTo
                            +" sortBy :: "+sharedPrefs!!.newsSortsBy)
                    (activity as SearchActivity).loadJsonData(queryValue, sharedPrefs!!.selectDateFrom!!,
                            sharedPrefs!!.selectDateTo!!,1, sharedPrefs!!.newsSortsBy!!)
                }else{
                    Toast.makeText(context,"Please enter search query...", Toast.LENGTH_SHORT).show()
                }

                dismiss()
            }

            R.id.imgClose -> {
                dismiss()
            }
        }
    }

    private fun updateLabelFrom() {
        val myFormat = "yyyy-MM-dd" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)

        sharedPrefs!!.selectDateFrom = sdf.format(myCalendarFrom.getTime())
        tvDateFrom.text = sharedPrefs!!.selectDateFrom
    }

    private fun updateLabelTo() {
        val myFormat = "yyyy-MM-dd" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)

        sharedPrefs!!.selectDateTo = sdf.format(myCalendarTo.getTime())
        tvDateTo.text = sharedPrefs!!.selectDateTo
    }
}
