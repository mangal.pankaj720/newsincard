package com.panky.newsincards.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.panky.newsincards.R

class SearchBaseAdapter(private val mContext: Context, private var searchList: MutableList<String>?) : BaseAdapter() {

    override fun getCount(): Int {
        return searchList!!.size
    }

    override fun getItem(position: Int): Any {
        return searchList!![position]
    }

    override fun getItemId(position: Int): Long {
       return position.toLong()
    }

    // class for holding the cached view
    private inner class ViewHolder {
        var tvSearchValue: TextView? = null
//        private val newsImgView: ImageView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        var searchView = convertView
        var searchViewHolder: ViewHolder

        if(searchView == null){

            searchViewHolder = ViewHolder()
            val inflater = LayoutInflater.from(mContext)
            searchView = inflater.inflate(R.layout.search_constant_value_display, parent, false)

            searchViewHolder.tvSearchValue = searchView.findViewById(R.id.tvSearchValue);

            // save the viewHolder to be reused later.
            searchView?.setTag(searchViewHolder)
        } else run { searchViewHolder = searchView.tag as ViewHolder }


        searchViewHolder.tvSearchValue?.text = searchList!!.get(position)

        return searchView
    }
}