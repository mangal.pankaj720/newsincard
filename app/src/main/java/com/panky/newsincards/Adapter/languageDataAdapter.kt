package com.panky.newsincards.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.TextView
import com.panky.newsincards.R

import kotlin.collections.ArrayList
import android.widget.Filterable
import com.panky.newsincards.models.languagesModel

class languageDataAdapter(private val mContext: Context, private var langModelArrayList: ArrayList<languagesModel>) : BaseAdapter(), Filterable{

    private var langNameSearchList : ArrayList<languagesModel>? = null
    private var valueFilter: ValueFilter? = null
    private var tvNoSearchFound : TextView? = null

    init {

        langNameSearchList = ArrayList()
        Log.e("country1 size :: ", ""+langNameSearchList?.size)
        langNameSearchList = langModelArrayList
        Log.e("country2 size :: ", ""+langNameSearchList?.size)

        filter
    }

    constructor(mContext: Context, langModelArrayList: ArrayList<languagesModel>, tvNoSearchFound :TextView) : this(mContext, langModelArrayList){
        this.tvNoSearchFound = tvNoSearchFound
    }

    override fun getCount(): Int {
        return langModelArrayList.size
    }

    override fun getItem(position: Int): Any {
        return langModelArrayList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var baseView = convertView

        val model = langModelArrayList.get(position)
        if (baseView == null) {
            // inflate the views from layout for the new row
            val inflater = LayoutInflater.from(mContext)
            baseView = inflater.inflate(R.layout.country_data_inflate_layout, parent, false)

        }

        val tvCountryName = baseView!!.findViewById<TextView>(R.id.tvCountryName)
        val tvCountryCode = baseView.findViewById<TextView>(R.id.tvCountryCode)
        tvCountryName.text = model.lang_name
        tvCountryCode.text = model.langNativeName

        return baseView
    }

    //Returns a filter that can be used to constrain data with a filtering pattern.
    override fun getFilter(): Filter {
        if (valueFilter == null) {
            valueFilter = ValueFilter()
        }
        return valueFilter as ValueFilter
    }

    inner class ValueFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val results = FilterResults()        // Holds the results of a filtering operation in values
            val FilteredArrList = ArrayList<languagesModel>()

            var constraints = constraint

            if (langNameSearchList == null) {
                langNameSearchList = ArrayList(langModelArrayList); // saves the original data in mOriginalValues
            }

            /********
             *
             *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
             *  else does the Filtering and returns FilteredArrList(Filtered)
             *
             ********/
            if (constraints == null || constraints.length == 0) {

                // set the Original result to return
                results.count = langNameSearchList!!.size
                results.values = langNameSearchList
            } else {
                constraints = constraint.toString().toLowerCase()
                for (i in 0 until langNameSearchList!!.size) {
                    val data = langNameSearchList!!.get(i).lang_name
                    if (data?.toLowerCase()!!.startsWith(constraints.toString())) {
                        FilteredArrList.add(languagesModel(langNameSearchList!!.get(i).lang_name!!,langNameSearchList!!.get(i).langCode!!,
                                langNameSearchList!!.get(i).countryName!!, langNameSearchList!!.get(i).langNativeName!!))
                    }else{

//                        tvNoSearchFound!!.visibility = View.VISIBLE
                    }
                }
                // set the Filtered result to return
                results.count = FilteredArrList.size
                results.values = FilteredArrList;
            }
            return results;
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {

            langModelArrayList = results.values as ArrayList<languagesModel>

            notifyDataSetChanged()
        }
    }
}
