package com.panky.newsincards.Adapter

import android.content.Context
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.panky.newsincards.R
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.models.LangModel
import android.graphics.drawable.Animatable

class LangRecyclerViewAdapter(private val mContext: Context, private var langList: ArrayList<LangModel>?) :
    androidx.recyclerview.widget.RecyclerView.Adapter<LangRecyclerViewAdapter.LangViewHolder>() {

    private var selectedPos:Int = 0
    private var sharedPrefs: SharedPrefs ?= null
    private var listener:langSelectedListener?= null

    init {
        sharedPrefs = SharedPrefs(mContext)
        selectedPos = sharedPrefs!!.languagePos

        Log.e("selectPos ", " :: $selectedPos")
//        listener = langSelectedListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): LangViewHolder {
       val mainView:View = LayoutInflater.from(mContext).inflate(R.layout.language_list_layout, parent, false)
       return LangViewHolder(mainView)
    }

    fun setSelectedViewListener(listener: langSelectedListener){
        this.listener = listener
    }

    override fun onBindViewHolder(langViewHolder: LangViewHolder, position: Int) {

        val langModel:LangModel = langList!!.get(position)

        if(selectedPos == position){
            langViewHolder.rlLayoutSelector.visibility = View.VISIBLE
            langViewHolder.imgSelectedLang.visibility = View.VISIBLE
            langViewHolder.itemView.isSelected = false
//            (langViewHolder.imgSelectedLang.getDrawable() as Animatable).start()
            Log.e("selectPosView ", " :: $position")
        }else{
            langViewHolder.rlLayoutSelector.visibility = View.GONE
            langViewHolder.itemView.isSelected = false
            langViewHolder.imgSelectedLang.visibility = View.GONE
        }

        langViewHolder.tvNativeName.text = langModel.nativeName
        langViewHolder.tvLangName.text = langModel.langName
        langViewHolder.itemView.setOnClickListener{

            listener?.getPosition(position)
            notifyItemChanged(selectedPos)
            selectedPos = position
            notifyItemChanged(selectedPos)

            sharedPrefs!!.languagePos = selectedPos
        }
    }

    override fun getItemCount(): Int {
        return langList!!.size
    }

    inner class LangViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

          var tvLangName:TextView
          var tvNativeName:TextView
          var imgSelectedLang:ImageView
          var clLanguageSelect: androidx.cardview.widget.CardView
        var rlLayoutSelector : RelativeLayout

        init {

            rlLayoutSelector = itemView.findViewById(R.id.rlLayoutSelector)
            tvLangName = itemView.findViewById(R.id.tvLangName)
            tvNativeName = itemView.findViewById(R.id.tvNativeName)
            imgSelectedLang = itemView.findViewById(R.id.imgSelectedLang)
            clLanguageSelect = itemView.findViewById(R.id.clLanguageSelect)
        }
    }

    interface langSelectedListener{
        fun getPosition(position:Int)
    }
}