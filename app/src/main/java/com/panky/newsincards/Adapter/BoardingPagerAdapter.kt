package com.panky.newsincards.Adapter

import android.content.Context
import androidx.viewpager.widget.PagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.panky.newsincards.R
import com.panky.newsincards.models.BoardingScreenData

class BoardingPagerAdapter() : androidx.viewpager.widget.PagerAdapter() {

    private var mContext:Context?=null
    private var boardingScreenDataList:ArrayList<BoardingScreenData>?=null
    var layoutInflater: LayoutInflater? = null

    constructor(mContext: Context, boardingScreenDataList: ArrayList<BoardingScreenData>?) : this() {
        this.boardingScreenDataList = boardingScreenDataList
        this.mContext = mContext
        layoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
    }


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
       return (view === `object` as LinearLayout)
    }

    override fun getCount(): Int {
        Log.e("list sizeAdp ", " :: "+boardingScreenDataList!!.size)
       return boardingScreenDataList?.size!!
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as LinearLayout)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val boardingScreenData:BoardingScreenData = boardingScreenDataList!!.get(position)
//        val layoutInflater = (mContext as LayoutInflater).
        val viewBoarding = layoutInflater!!.inflate(R.layout.boarding_screen_layout, container, false)
        val imgBoardingScreen:ImageView = viewBoarding.findViewById(R.id.imgBoardingScreen)
        val tvTitleBoarding:TextView = viewBoarding.findViewById(R.id.tvTitleBoarding)
        val tvDescBoarding:TextView = viewBoarding.findViewById(R.id.tvDescBoarding)

        imgBoardingScreen.setImageResource(boardingScreenData.imageUrl!!)
        tvTitleBoarding.text = boardingScreenData.title
        tvDescBoarding.text = boardingScreenData.desc

        container.addView(viewBoarding)
        return viewBoarding
    }
}