package com.panky.newsincards.Adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.panky.newsincards.R
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.models.CountryLangModel

class CountryWithLangRVAdapter(private val mContext: Context, private var countryLangList: ArrayList<CountryLangModel>?) :
    androidx.recyclerview.widget.RecyclerView.Adapter<CountryWithLangRVAdapter.CountryLangViewHolder>() {

    private var selectedPos:Int = 0
    private var sharedPrefs: SharedPrefs?= null
    private var listener:countrySelectedListener?= null

    init {
        sharedPrefs = SharedPrefs(mContext)
        selectedPos = sharedPrefs!!.countryPos

        Log.e("selectPos ", " :: $selectedPos")
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CountryLangViewHolder {
        val mainView: View = LayoutInflater.from(mContext).inflate(R.layout.country_with_lang_setup_layout,
            parent, false)
        return CountryLangViewHolder(mainView)
    }

    fun setSelectedViewListener(listener: countrySelectedListener){
        this.listener = listener
    }

    override fun onBindViewHolder(langViewHolder: CountryLangViewHolder, position: Int) {

        val countryLangModel: CountryLangModel = countryLangList!!.get(position)

        if(selectedPos == position){
            langViewHolder.llCountrySelector.visibility = View.VISIBLE
            langViewHolder.itemView.isSelected = false
            Log.e("selectPosView ", " :: $position")
        }else{
            langViewHolder.itemView.isSelected = false
            langViewHolder.llCountrySelector.visibility = View.GONE
        }

        langViewHolder.tvCountryNameFlag.text = countryLangModel.CountryName

        Glide.with(mContext).load(countryLangModel.ImageUrl)
            .placeholder(R.drawable.flag_placeholder)
            .into(langViewHolder.imgCountryFlag)

        langViewHolder.itemView.setOnClickListener{

            listener?.getPosition(position)
            notifyItemChanged(selectedPos)
            selectedPos = position
            notifyItemChanged(selectedPos)

            sharedPrefs!!.countryPos = selectedPos
        }

    }

    override fun getItemCount(): Int {
        return countryLangList!!.size
    }

    inner class CountryLangViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        var tvCountryNameFlag: TextView
        var imgCountryFlag: ImageView
        var llCountrySelector : LinearLayout

        init {
            llCountrySelector = itemView.findViewById(R.id.llCountrySelector)
            tvCountryNameFlag = itemView.findViewById(R.id.tvCountryNameFlag)
            imgCountryFlag = itemView.findViewById(R.id.imgCountryFlag)
        }
    }

    interface countrySelectedListener{
        fun getPosition(position:Int)
    }
}