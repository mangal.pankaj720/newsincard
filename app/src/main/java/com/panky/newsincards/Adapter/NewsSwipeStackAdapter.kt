package com.panky.newsincards.Adapter

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView

import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.panky.newsincards.Constants.ShareNewsListener
import com.panky.newsincards.DBHelper.NewsDbHelper
import com.panky.newsincards.R
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.Utility.Utils
import com.panky.newsincards.models.Article

class NewsSwipeStackAdapter(private val mContext: Context, private val articleList: List<Article>,
                            private val classReference : String) : BaseAdapter() {

    private val newsPrefers: SharedPrefs = SharedPrefs(mContext)
    private val dbHelper: NewsDbHelper = NewsDbHelper(mContext)

    private val TAG = "NewsSwipeStackAdapter"
    private lateinit var listenerShareNewsObject : ShareNewsListener

    override fun getCount(): Int {
        Log.e("Stackadapter size :: ", "" + articleList.size)
        return articleList.size
    }

    override fun getItem(position: Int): Any {
        return articleList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun setShareNewsListener(listenerShareNewsObject : ShareNewsListener){
        this.listenerShareNewsObject = listenerShareNewsObject
    }
    // class for holding the cached view
    private class ViewHolder {
        lateinit var selectableRelativeLayout: RelativeLayout
        lateinit var title: TextView
        lateinit var desc: TextView
        lateinit var author: TextView
        lateinit var publishedAt: TextView
        lateinit var time: TextView
        lateinit var urlLink: TextView
        lateinit var newsImgView: ImageView
        lateinit var mProgBar: ProgressBar
        lateinit var source : TextView
        lateinit var ivShareNews: ImageView
    }

    @SuppressLint("CheckResult")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var baseView = convertView

        val utils = Utils(mContext)
        // holder of the views to be reused.
        val newsViewHolder: ViewHolder
        val articleModel = articleList[position]

        Log.e("Stackadapter pos :: ", "" + articleModel.title)
        // if no previous views found
        if (baseView == null) {
            // create the container ViewHolder
            newsViewHolder = ViewHolder()

            // inflate the views from layout for the new row
            val inflater = LayoutInflater.from(mContext)

            if(classReference.equals("favouriteActivity", true)){
                baseView = inflater.inflate(R.layout.favourite_news_item_layout, parent, false)
            }else{
                baseView = inflater.inflate(R.layout.news_items_layout, parent, false)
            }

            // set the view to the ViewHolder.
            newsViewHolder.selectableRelativeLayout = baseView!!.findViewById(R.id.selectableRL)
            newsViewHolder.title = baseView.findViewById(R.id.title)
            newsViewHolder.desc = baseView.findViewById(R.id.tvDescpt)
            newsViewHolder.source = baseView.findViewById(R.id.source)
            newsViewHolder.author = baseView.findViewById(R.id.authorTV)
            newsViewHolder.publishedAt = baseView.findViewById(R.id.publishedAt)
            newsViewHolder.time = baseView.findViewById(R.id.time)
            newsViewHolder.urlLink = baseView.findViewById(R.id.urlLink)
            newsViewHolder.ivShareNews = baseView.findViewById(R.id.ivShareNews)

            newsViewHolder.newsImgView = baseView.findViewById(R.id.newsImgView)
            newsViewHolder.mProgBar = baseView.findViewById(R.id.progress_photo_load)

            // save the viewHolder to be reused later.
            baseView.tag = newsViewHolder
        } else {
            // there is already ViewHolder, reuse it.
            newsViewHolder = baseView.tag as ViewHolder
        }

        val requestOptions = RequestOptions()
        requestOptions.placeholder(utils.randomDrawbleColor)
        requestOptions.error(utils.randomDrawbleColor)
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)
        requestOptions.skipMemoryCache(true);
        requestOptions.centerCrop()

        /*
                .thumbnail(0.1f)
                .dontTransform()*/
        Glide.with(mContext)
                .load(articleModel.urlToImage)
                .apply(requestOptions)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                        newsViewHolder.mProgBar.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(resource: Drawable, model: Any?, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                        newsViewHolder.mProgBar.visibility = View.GONE
                        return false
                    }
                })
                .error(R.drawable.new_pic)
                .thumbnail(0.6f)
                .dontAnimate()
                .dontTransform()
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(newsViewHolder.newsImgView)

        newsViewHolder.ivShareNews.setOnClickListener {
            listenerShareNewsObject.getShareNewsPosition(position)
        }


        Log.e("DB Data :: ","")
        newsViewHolder.title.text = articleModel.title

        if(!articleModel.description.isNullOrEmpty()){

            val result = Html.fromHtml(articleModel.description!!).toString().replace("\n", "").trim()
            newsViewHolder.desc.text = result
            newsViewHolder.desc.setOnClickListener { showDescAlertDialog(result) }
        }else{
            newsViewHolder.desc.visibility = View.GONE
        }

        newsViewHolder.source.text = articleModel.source?.name
        val outputDateFormat = articleModel.publishedAt?.subSequence(0, 19)
        Log.e("date value :: ", outputDateFormat as String?)
        newsViewHolder.time.text = String.format(" \u2022 "+utils.DateToTimeFormat(outputDateFormat.toString()))
        newsViewHolder.publishedAt.text = utils.DateFormat(outputDateFormat.toString())
        newsViewHolder.author.text = articleModel.author
        newsViewHolder.urlLink.movementMethod = LinkMovementMethod.getInstance()
        newsViewHolder.urlLink.text = articleModel.url

        return baseView
    }

    private fun showDescAlertDialog(msg: String) {
        val descAlert = AlertDialog.Builder(mContext)
        descAlert.setTitle("News Description")
                .setMessage(msg)
                .setCancelable(true)
                .setPositiveButton("Cancel") { dialog, _ -> dialog.dismiss() }

        descAlert.show()
    }
}
