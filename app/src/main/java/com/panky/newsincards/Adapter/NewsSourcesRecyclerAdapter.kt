package com.panky.newsincards.Adapter

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
//import com.google.android.gms.ads.AdListener
//import com.google.android.gms.ads.AdRequest
//import com.google.android.gms.ads.InterstitialAd

import com.panky.newsincards.Constants.Constant
import com.panky.newsincards.R
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.SourceDetailActivity
import com.panky.newsincards.models.Sources

class NewsSourcesRecyclerAdapter(private val mContext: Context, private val newsSourcesList: List<Sources.Source>) :
    androidx.recyclerview.widget.RecyclerView.Adapter<NewsSourcesRecyclerAdapter.NewsSourceViewHolder>(), Constant {

//    private val sharedPrefs: SharedPrefs
    private val selectedPos: Int
//    private var mInterstitialAd : InterstitialAd? =null

    init {
        val sharedPrefs = SharedPrefs(mContext)
        selectedPos = sharedPrefs.sourcePos

        /*mInterstitialAd = InterstitialAd(mContext)
        mInterstitialAd!!.adUnitId = mContext.resources.getString(R.string.interstitial_sources_ad_unit_id)
        mInterstitialAd!!.loadAd(AdRequest.Builder().build())*/
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): NewsSourceViewHolder {

        val view = LayoutInflater.from(mContext).inflate(R.layout.source_item_layout, viewGroup, false)

        return NewsSourceViewHolder(view)
    }

    override fun onBindViewHolder(newsSourceViewHolder: NewsSourceViewHolder, position: Int) {

        val sources = newsSourcesList[position]
        Log.e("source name :: ", "" + sources.name!!)
        newsSourceViewHolder.sourcesTV.text = sources.name

        /*if (selectedPos == position) {
            newsSourceViewHolder.itemView.setSelected(true);
            newsSourceViewHolder.sourcesTV.setTextColor(Color.parseColor("#ffffff"));
        } else {
            newsSourceViewHolder.itemView.setSelected(false);
            newsSourceViewHolder.sourcesTV.setTextColor(Color.parseColor("#696969"));
        }*/

        newsSourceViewHolder.itemView.setOnClickListener {
            //                notifyItemChanged(selectedPos);
            //                selectedPos = position;
            //                notifyItemChanged(selectedPos);

            //                sharedPrefs.setCategory(catValue);
            //                sharedPrefs.setSourcePos(selectedPos);

           /* mInterstitialAd!!.loadAd(AdRequest.Builder().build())

            mInterstitialAd!!.adListener = object : AdListener(){
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    showInterstitialAd()
                }
            }*/

            val intentSourceDetails = Intent(mContext, SourceDetailActivity::class.java)
            intentSourceDetails.putExtra(Constant.SOURCE_ID, sources.id)
            intentSourceDetails.putExtra(Constant.SOURCE_NAME, sources.name)
            mContext.startActivity(intentSourceDetails)

            //                loadMoreButton.setVisibility(View.GONE);
            //                tvdataNotFound.setVisibility(View.GONE);
            //                Log.e("catValue ", " :: "+catValue);
            //                loadJsonData("", catValue);
        }
    }

   /* private fun showInterstitialAd() {
        if (mInterstitialAd!!.isLoaded) {
            mInterstitialAd!!.show()
        }
    }*/

    override fun getItemCount(): Int {
        Log.e("source size :: ", "" + newsSourcesList.size)
        return newsSourcesList.size
    }

    inner class NewsSourceViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        internal val sourcesTV: TextView

        init {

            sourcesTV = itemView.findViewById(R.id.tvSourceName)
        }
    }
}
