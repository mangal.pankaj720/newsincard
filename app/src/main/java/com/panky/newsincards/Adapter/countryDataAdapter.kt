package com.panky.newsincards.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.TextView
import com.panky.newsincards.R

import com.panky.newsincards.models.countryModel
import kotlin.collections.ArrayList
import android.widget.Filterable

class countryDataAdapter(private val mContext: Context,  private var countryModelArrayList: ArrayList<countryModel>) : BaseAdapter(), Filterable{

    private var countryNameSearchList : ArrayList<countryModel>? = null
    private var valueFilter: ValueFilter? = null
    private var tvNoSearchFound : TextView? = null

    init {

        countryNameSearchList = ArrayList()
        Log.e("country1 size :: ", ""+countryNameSearchList?.size)
        countryNameSearchList = countryModelArrayList
        Log.e("country2 size :: ", ""+countryNameSearchList?.size)

        filter
    }

    constructor(mContext: Context, countryModelArrayList: ArrayList<countryModel>, tvNoSearchFound :TextView) : this(mContext, countryModelArrayList){
        this.tvNoSearchFound = tvNoSearchFound
    }

    override fun getCount(): Int {
        return countryModelArrayList.size
    }

    override fun getItem(position: Int): Any {
        return countryModelArrayList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var baseView = convertView

        val model = countryModelArrayList.get(position)
        if (baseView == null) {
            // inflate the views from layout for the new row
            val inflater = LayoutInflater.from(mContext)
            baseView = inflater.inflate(R.layout.country_data_inflate_layout, parent, false)

        }

        val tvCountryName = baseView!!.findViewById<TextView>(R.id.tvCountryName)
        val tvCountryCode = baseView.findViewById<TextView>(R.id.tvCountryCode)
        tvCountryName.text = model.countryName
        tvCountryCode.text = model.countryCode

        return baseView
    }

    //Returns a filter that can be used to constrain data with a filtering pattern.
    override fun getFilter(): Filter {
        if (valueFilter == null) {
            valueFilter = ValueFilter()
        }
        return valueFilter as ValueFilter
    }

    inner class ValueFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val results = FilterResults()        // Holds the results of a filtering operation in values
            val FilteredArrList = ArrayList<countryModel>()

            var constraints = constraint

            if (countryNameSearchList == null) {
                countryNameSearchList = ArrayList(countryModelArrayList); // saves the original data in mOriginalValues
            }

            /********
             *
             *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
             *  else does the Filtering and returns FilteredArrList(Filtered)
             *
             ********/
            if (constraints == null || constraints.length == 0) {

                // set the Original result to return
                results.count = countryNameSearchList!!.size
                results.values = countryNameSearchList
            } else {
                constraints = constraint.toString().toLowerCase();
                for (i in 0 until countryNameSearchList!!.size) {
                    val data = countryNameSearchList!!.get(i).countryName
                    if (data?.toLowerCase()!!.startsWith(constraints.toString())) {
                        FilteredArrList.add(countryModel(countryNameSearchList!!.get(i).countryName!!,countryNameSearchList!!.get(i).countryCode!!));
                    }else{

//                        tvNoSearchFound!!.visibility = View.VISIBLE
                    }
                }
                // set the Filtered result to return
                results.count = FilteredArrList.size
                results.values = FilteredArrList;
            }
            return results;
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {

            countryModelArrayList = results.values as ArrayList<countryModel>

            notifyDataSetChanged()
        }
    }
}
