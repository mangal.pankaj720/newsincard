package com.panky.newsincards

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar

import com.panky.newsincards.SharedPreference.SharedPrefs

class HelpActivity : AppCompatActivity() {

    private var customToolbar: Toolbar? = null
    private var sharedPrefs: SharedPrefs? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        sharedPrefs = SharedPrefs(this@HelpActivity)
        if (sharedPrefs!!.isNightModeEnabled) {
            setTheme(R.style.AppTheme_Base_Night)
        } else {
            setTheme(R.style.AppTheme_Base_Light)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)
        initViews()


        setSupportActionBar(customToolbar)
        if (supportActionBar != null) {
            supportActionBar!!.title = resources.getString(R.string.helpTitle)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }


    }

    private fun initViews() {
        customToolbar = findViewById(R.id.custom_toolbar)
    }
}