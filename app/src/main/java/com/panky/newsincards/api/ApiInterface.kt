package com.panky.newsincards.api

import com.panky.newsincards.models.News
import com.panky.newsincards.models.Sources

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("top-headlines")
    fun getNews(

        /* @Query("country") country: String,*/

            @Query("country") country: String,
            @Query("category") category: String,
            @Query("page") page:Int,
            @Query("pagesize")pageSize:Int,
            @Query("apiKey") apiKey: String
    ): Call<News>

    @GET("top-headlines")
    fun getNewsBySources(

            @Query("sources") source: String,
            @Query("page") page:Int,
            @Query("pagesize")pageSize:Int,
            @Query("apiKey") apiKey: String
    ): Call<News>

    @GET("everything")
    fun getNewsSearch(

            @Query("q") keyword: String,
            @Query("language") language: String,
            @Query("sortBy") sortBy: String,
            @Query("apiKey") apiKey: String
    ): Call<News>

    @GET("everything")
    fun getNewsSearchByQuery(

            @Query("q") keyword: String,
            @Query("from") fromDate: String,
            @Query("to") toDate: String,
            @Query("language") language: String,
            @Query("sortBy") sortBy: String,
            @Query("page") page:Int,
            @Query("pagesize")pageSize:Int,
            @Query("apiKey") apiKey: String
    ): Call<News>

    @GET("sources")
    fun getNewsSourcesName(
            @Query("language") lang: String,
            @Query("country") country: String,
            @Query("apiKey") apiKey: String
    ): Call<Sources>

}
