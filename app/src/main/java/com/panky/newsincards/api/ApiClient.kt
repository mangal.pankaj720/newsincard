package com.panky.newsincards.api

import com.panky.newsincards.BuildConfig
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    private var retrofit: Retrofit? = null

    val apiClient: Retrofit
        get() {

            val logging= HttpLoggingInterceptor()
            logging.level= HttpLoggingInterceptor.Level.BODY
            val  httpclient= OkHttpClient.Builder()
            httpclient.addInterceptor{chain->
                val original=chain.request()
                val  request=original.newBuilder()
//                request.addHeader("accept","application/json")
                val request1=request.build()
                chain.proceed(request1)
            }
            httpclient.connectTimeout(60,java.util.concurrent.TimeUnit.SECONDS)
            httpclient.readTimeout(60,java.util.concurrent.TimeUnit.SECONDS)
            httpclient.addInterceptor(logging)

            if (retrofit == null) {
                retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                        .client(httpclient.build())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
            }
            return retrofit!!
        }

   /* private// Create a trust manager that does not validate certificate chains
    // Install the all-trusting trust manager
    // Create an ssl socket factory with our all-trusting manager
    val unsafeOkHttpClient: OkHttpClient.Builder
        get() {
            try {
                val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
//                    @Throw(CertificateException::class)
                    override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                    }

//                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                    }

                    override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                        return arrayOf()
                    }
                })
                val sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, java.security.SecureRandom())
                val sslSocketFactory = sslContext.socketFactory

                val builder = OkHttpClient.Builder()
                builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                builder.hostnameVerifier { _, session -> true }
                return builder
            } catch (e: Exception) {
                throw RuntimeException(e)
            }

        }*/
}
