package com.panky.newsincards.DBHelper

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

import com.panky.newsincards.Constants.Constant
import com.panky.newsincards.models.Article
import com.panky.newsincards.models.Source

import java.util.ArrayList

class NewsDbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION), Constant {

    companion object {
        private val DATABASE_NAME = "news_db"
        private val DATABASE_VERSION = 4

        private val CREATE_NEWS_TABLE = ("CREATE TABLE " + Constant.TABLE_NAME
                + "( " + Constant.NEWS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Constant.NEWS_TITLE + " TEXT UNIQUE, " + Constant.NEWS_DESCRIPTION + " TEXT, "
                + Constant.NEWS_WEB_URL + " TEXT, " + Constant.NEWS_IMAGE_URL + " TEXT, "
                + Constant.NEWS_SOURCE_NAME + " TEXT, " + Constant.NEWS_AUTHOR + " TEXT, "
                + Constant.NEWS_PUBLISHED_AT + " TEXT, "+ Constant.NEWS_SOURCE_ID + " TEXT ) ")

        private val CREATE_NEWS_ARTICLE_TABLE = ("CREATE TABLE " + Constant.TABLE_NAME
                + "( " + Constant.NEWS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Constant.NEWS_SOURCE_ID + " TEXT, " + Constant.NEWS_SOURCE_NAME + " TEXT, "
                + Constant.NEWS_TITLE + " TEXT UNIQUE, " + Constant.NEWS_DESCRIPTION + " TEXT, "
                + Constant.NEWS_WEB_URL + " TEXT, " + Constant.NEWS_IMAGE_URL + " TEXT, "
                 + Constant.NEWS_AUTHOR + " TEXT, " + Constant.NEWS_PUBLISHED_AT + " TEXT ) ")
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_NEWS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (newVersion > oldVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + Constant.TABLE_NAME)
//            db.execSQL(Constant.TABLE_NAME_CHANGE)
        }
        onCreate(db)
    }

    val allNewsData: List<Article>
        get() {
            val newsDataList = ArrayList<Article>()
            val selectQuery = "SELECT  * FROM " + Constant.TABLE_NAME

            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery, null)
            if (cursor.moveToFirst()) {
                do {
                    val newsData = Article()
                    val source = Source()
                    source.name = cursor.getString(cursor.getColumnIndex(Constant.NEWS_SOURCE_NAME))
                    source.id = cursor.getString(cursor.getColumnIndex(Constant.NEWS_SOURCE_ID))

                    newsData.id = cursor.getInt(cursor.getColumnIndex(Constant.NEWS_ID))
                    newsData.title = cursor.getString(cursor.getColumnIndex(Constant.NEWS_TITLE))
                    newsData.description = cursor.getString(cursor.getColumnIndex(Constant.NEWS_DESCRIPTION))
                    newsData.url = cursor.getString(cursor.getColumnIndex(Constant.NEWS_WEB_URL))
                    newsData.urlToImage = cursor.getString(cursor.getColumnIndex(Constant.NEWS_IMAGE_URL))
                    newsData.source = source
                    newsData.author = cursor.getString(cursor.getColumnIndex(Constant.NEWS_AUTHOR))
                    newsData.publishedAt = cursor.getString(cursor.getColumnIndex(Constant.NEWS_PUBLISHED_AT))


                    Log.e("source Name ", " :: "+cursor.getString(cursor.getColumnIndex(Constant.NEWS_SOURCE_NAME)))
                    newsDataList.add(newsData)
                } while (cursor.moveToNext())
            }
            cursor.close()
            db.close()
            return newsDataList
        }

    val countDBTable: Int
        get() {
            val db = this.readableDatabase
            val mCursor = db.rawQuery("SELECT * FROM " + Constant.TABLE_NAME, null)
            return mCursor.count
        }

    fun insertFavouriteNewsData(title: String, desc: String, url: String, imgUrl: String,
                                 sourceName: String, author: String, pubAt: String, source_Id: String): Boolean {
        val db = this.writableDatabase
        val values = ContentValues()

        values.put(Constant.NEWS_SOURCE_NAME, sourceName)
        values.put(Constant.NEWS_SOURCE_ID, source_Id)
        values.put(Constant.NEWS_TITLE, title)
        values.put(Constant.NEWS_DESCRIPTION, desc)
        values.put(Constant.NEWS_WEB_URL, url)
        values.put(Constant.NEWS_IMAGE_URL, imgUrl)
        values.put(Constant.NEWS_AUTHOR, author)
        values.put(Constant.NEWS_PUBLISHED_AT, pubAt)

        val result = db.insertWithOnConflict(Constant.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE)
        db.close()
        return result != -1L
    }


    fun checkDBTable(): Int {

        val db = this.readableDatabase
        val mCursor = db.rawQuery("SELECT * FROM " + Constant.TABLE_NAME, null)
//        mCursor.close()
        return mCursor.count
    }

    fun updateNewsData(id: Int,sourceName: String, source_Id: String, title: String, desc: String, url: String, imgUrl: String,
                       author: String, pubAt: String): Boolean {
        val db = this.writableDatabase
        val values = ContentValues()

        values.put(Constant.NEWS_SOURCE_NAME, sourceName)
        values.put(Constant.NEWS_SOURCE_ID, source_Id)
        values.put(Constant.NEWS_TITLE, title)
        values.put(Constant.NEWS_DESCRIPTION, desc)
        values.put(Constant.NEWS_WEB_URL, url)
        values.put(Constant.NEWS_IMAGE_URL, imgUrl)
        values.put(Constant.NEWS_AUTHOR, author)
        values.put(Constant.NEWS_PUBLISHED_AT, pubAt)

        db.update(Constant.TABLE_NAME, values, Constant.NEWS_ID + " = ?", arrayOf(id.toString()))

        //        db is close here...
        db.close()
        return true
    }

    fun deleteNewsData(id: Int): Int? {
        val db = this.writableDatabase
        return db.delete(Constant.TABLE_NAME, Constant.NEWS_ID + " = ?", arrayOf(id.toString()))
    }


   fun deleteAllValues(){
       val db = this.writableDatabase
       db.delete(Constant.TABLE_NAME,null,null)
    }
}
