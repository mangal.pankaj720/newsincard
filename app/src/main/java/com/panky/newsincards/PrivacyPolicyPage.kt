package com.panky.newsincards

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import com.panky.newsincards.SharedPreference.SharedPrefs

class PrivacyPolicyPage : AppCompatActivity() {

    private val TAG : String = PrivacyPolicyPage::class.java.simpleName
    private lateinit var customToolbar: Toolbar
    private lateinit var wvPrivacyPolicy:WebView
    private var sharedPrefs: SharedPrefs? = null
    private lateinit var pbPolicyLoader:ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {

        sharedPrefs = SharedPrefs(this@PrivacyPolicyPage)

        Log.e("$TAG :: ", "onCreate Called")
        if (sharedPrefs!!.isNightModeEnabled) {
            setTheme(R.style.AppTheme_Base_Night)
        } else {
            setTheme(R.style.AppTheme_Base_Light)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy_page)
        initViews()

        setSupportActionBar(customToolbar)
            supportActionBar!!.title = resources.getString(R.string.privacy_policy)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        initWebView("https://sites.google.com/view/newsincardprivacypolicy/home")
    }

    private fun initViews(){
        customToolbar = findViewById(R.id.custom_toolbar)
        wvPrivacyPolicy = findViewById(R.id.wvPrivacyPolicy)
        pbPolicyLoader = findViewById(R.id.pbPolicyLoader)
    }

    private fun initWebView(url: String?) {
        wvPrivacyPolicy.settings.loadsImagesAutomatically = true
        wvPrivacyPolicy.settings.javaScriptEnabled = true
        wvPrivacyPolicy.settings.domStorageEnabled = true
        wvPrivacyPolicy.settings.loadsImagesAutomatically = true
        wvPrivacyPolicy.settings.setSupportZoom(true)
        wvPrivacyPolicy.settings.builtInZoomControls = true
        wvPrivacyPolicy.settings.displayZoomControls = false
        wvPrivacyPolicy.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        wvPrivacyPolicy.webViewClient = WebViewClient()
        wvPrivacyPolicy.invokeZoomPicker()
        wvPrivacyPolicy.loadUrl(url)
        pbPolicyLoader.visibility = View.GONE
    }
}
