package com.panky.newsincards.Services

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.panky.newsincards.view.MainPage.view.MainActivity
import com.panky.newsincards.R

class NotificationMessagingServices : FirebaseMessagingService() {

   /* override fun onMessageReceived(msgFromFirebase: RemoteMessage?) {
        msgFromFirebase?.notification?.body?.let { showNotification(it) }
    }*/


    private fun showNotification(msg : String){

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent : PendingIntent = PendingIntent.getActivity(this, 0,
            intent, PendingIntent.FLAG_ONE_SHOT)

        val notificationSoundUri:Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notification : Notification = NotificationCompat.Builder(this)
            .setSmallIcon(R.drawable.ic_notification_name)
            .setContentTitle("News Pro Notificaiton !!")
            .setContentText(msg)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setSound(notificationSoundUri)
            .build()

        val notificationManager : NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notification)
    }
}