package com.panky.newsincards.view.MainPage.repo

import androidx.lifecycle.MutableLiveData
import com.panky.newsincards.api.ApiClient
import com.panky.newsincards.api.ApiInterface
import com.panky.newsincards.models.News
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsRespository {

    private var api: ApiInterface? = null

    init {
        val networkModule = ApiClient.apiClient
        api = networkModule.create(ApiInterface::class.java)
    }

    fun getNewsData(country:String, category:String, page:Int, pageSize:Int, apiKey:String):MutableLiveData<News>{
        val newsData = MutableLiveData<News>()
        api?.getNews(country, category, page, pageSize, apiKey)?.enqueue(object : Callback<News>{
            override fun onFailure(call: Call<News>, t: Throwable) {
                newsData.value = null
            }

            override fun onResponse(call: Call<News>, response: Response<News>) {
                if(response.isSuccessful){
                    newsData.value = response.body()!!
                }
            }
        })
        return newsData
    }
}