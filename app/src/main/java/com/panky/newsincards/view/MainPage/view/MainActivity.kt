package com.panky.newsincards.view.MainPage.view

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.facebook.login.LoginManager
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
//import com.google.android.gms.ads.AdListener
//import com.google.android.gms.ads.AdRequest
//import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.iid.FirebaseInstanceId
import com.panky.newsincards.*
import com.panky.newsincards.Adapter.NewsSwipeStackAdapter
import com.panky.newsincards.Constants.Constant
import com.panky.newsincards.Constants.Constant.Companion.PERMISSION_REQUEST_CODE
import com.panky.newsincards.Constants.ConstantMethod
import com.panky.newsincards.Constants.Constants
import com.panky.newsincards.Constants.ShareNewsListener
import com.panky.newsincards.DBHelper.NewsDbHelper
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.Utility.InternetBroadCastReceiver
import com.panky.newsincards.Utility.Utils
import com.panky.newsincards.Utils.ConnectionStatus
import com.panky.newsincards.api.ApiClient
import com.panky.newsincards.api.ApiInterface
import com.panky.newsincards.models.Article
import com.panky.newsincards.models.News
import com.panky.newsincards.models.UserData
import com.panky.newsincards.view.MainPage.ViewModel.NewsViewModel
import com.rupins.drawercardbehaviour.CardDrawerLayout
import com.varunest.sparkbutton.SparkButton
import link.fls.swipestack.SwipeStack
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/*SwipeRefreshLayout.OnRefreshListener,*/
class MainActivity : AppCompatActivity(), Constant, View.OnClickListener,
    NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener, ShareNewsListener {

    override fun getShareNewsPosition(position: Int) {
        Log.e("Share Pos ", " :: $position")

        fabNewsDetail.hide()
        rlScreenshotShare.visibility = View.VISIBLE
        val utils = Utils(this@MainActivity)
        val articleModel = articleList[position]

        val requestOptions = RequestOptions()
        requestOptions.placeholder(utils.randomDrawbleColor)
        requestOptions.error(utils.randomDrawbleColor)
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)
        requestOptions.skipMemoryCache(true)
        requestOptions.centerCrop()

        Log.e("urlToImage ", " :: "+articleModel.urlToImage)

        Glide.with(this@MainActivity)
            .load(articleModel.urlToImage)
            .apply(requestOptions)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    mProgBar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any?, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    mProgBar.visibility = View.GONE
                    return false
                }
            })
            .error(R.drawable.new_pic)
            .thumbnail(0.6f)
            .dontAnimate()
            .dontTransform()
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(newsImgView)


        Log.e("DB Data :: ","")
        title.text = articleModel.title

        if(!articleModel.description.isNullOrEmpty()){

            val result = Html.fromHtml(articleModel.description!!).toString().replace("\n", "").trim()
            desc.text = result
        }else{
            desc.visibility = View.GONE
        }

        source.text = articleModel.source?.name
        val outputDateFormat = articleModel.publishedAt?.subSequence(0, 19)
        Log.e("date value :: ", outputDateFormat as String?)
        time.text = String.format(" \u2022 "+utils.DateToTimeFormat(outputDateFormat.toString()))
        publishedAt.text = utils.DateFormat(outputDateFormat.toString())
        author.text = articleModel.author
        urlLink.movementMethod = LinkMovementMethod.getInstance()
        urlLink.text = articleModel.url

        tvShareScreenCancel.setOnClickListener {
            rlScreenshotShare.visibility = View.GONE
            fabNewsDetail.show()
        }

        tvShareScreenShot.setOnClickListener {
            rlScreenshotShare.visibility = View.GONE
            fabNewsDetail.show()
            if(checkPermission()){
                val view:View = findViewById(R.id.rlShareCardNews)
                val androidColors = resources.getIntArray(R.array.androidcolors)
                val randomAndroidColor = androidColors[Random().nextInt(androidColors.size)]
                view.setBackgroundColor(randomAndroidColor)

                val bitmap = takeScreenshot(view)
                saveBitmap(bitmap)
                shareIt(imageFile!!)
            }else {
                requestPermission()
            }

        }

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.e("LoginActivity", "onConnectionFailed:$connectionResult")
        Toast.makeText(this, resources.getString(R.string.errorPlayService), Toast.LENGTH_SHORT).show()
    }


    private val PERMISSIONS_STORAGE =
        arrayOf<String>(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)

    companion object {
        private const val TAG = "MainActivity"
        private var pageNo : Int = 1
        private var quotientResult:Int= 0
    }

    //Share News Views instance here...
    private lateinit var tvShareScreenCancel:TextView
    private lateinit var tvShareScreenShot:TextView
    private lateinit var rlScreenshotShare:RelativeLayout
    lateinit var title: TextView
    lateinit var desc: TextView
    lateinit var author: TextView
    lateinit var publishedAt: TextView
    lateinit var time: TextView
    lateinit var urlLink: TextView
    lateinit var newsImgView: ImageView
    lateinit var mProgBar: ProgressBar
    lateinit var source : TextView

    private var doubleBackToExitPressedOnce = false
    private var newsCategoryRV: androidx.recyclerview.widget.RecyclerView? = null
    private var layoutManagerCategory: androidx.recyclerview.widget.RecyclerView.LayoutManager? = null

    //    private NewsAdapter newsAdapter;
    private var categoryAdapter: NewsCategoryAdapter? = null
    private var errorLayout: RelativeLayout? = null
    private var errorImageView: ImageView? = null
    private var errorTitle: TextView? = null
    private var errorMessage: TextView? = null
    private var imgArticle: ImageView? = null
    private var tvdataNotFound: TextView? = null
    private var tvCountFavNews: TextView? = null
    private var retryButton: Button? = null
    private var loadMoreButton: Button? = null
    private var categoryList: ArrayList<String>? = null
    private var categoryListDuplicate: ArrayList<String>? = null
    private lateinit var articleList: MutableList<Article>
    private lateinit var rlLoadMoreContent : RelativeLayout
    private lateinit var rlFavouriteFab:RelativeLayout

    //    Shared preference object created here....
    private var sharedPrefs: SharedPrefs? = null
    private lateinit var dbHelper: NewsDbHelper
    private var fabSwipeLeft: FloatingActionButton? = null
    private var fabSwipeRight: FloatingActionButton? = null
    private lateinit var fabNewsDetail: FloatingActionButton
    private lateinit var fabSwipeFavLeft: FloatingActionButton
    private lateinit var fabSwipeFavRight: FloatingActionButton

    private var swipeStackLoader: SwipeStack? = null
    private var swipeStackAdapter: NewsSwipeStackAdapter? = null
    private var mainNewsCoLayt: androidx.coordinatorlayout.widget.CoordinatorLayout? = null

    private var newsToolbar: Toolbar? = null
    private var newsDrawerLayout: CardDrawerLayout? = null
    private var newsNavViews: NavigationView? = null
    private var drawerToggle: ActionBarDrawerToggle? = null
    private var btnSparkAnimate: SparkButton? = null

    private var countryCode : String? = null
    private var langCode : String? = null

    private val internetBroadCastReceiver = InternetBroadCastReceiver()
    private var intentFilter: IntentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)

//   private var mInterstitialAd : InterstitialAd? =null
    private var mGoogleApiClient: GoogleApiClient? = null

    //Firebase instance here....
    private var dbReference :DatabaseReference?= null
    private var mAuth: FirebaseAuth? = null
    private var user:FirebaseUser?=null

    private lateinit var newsViewModel:NewsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        sharedPrefs = SharedPrefs(this)


        newsViewModel = ViewModelProviders.of(this@MainActivity).get(NewsViewModel::class.java)

//        mInterstitialAd = InterstitialAd(this)
//        mInterstitialAd!!.adUnitId = resources.getString(R.string.interstitial_ad_unit_id)
//        mInterstitialAd!!.loadAd(AdRequest.Builder().build())

        //Firebase instance initialize here....
        dbReference = FirebaseDatabase.getInstance().reference.child("UserFavouriteNews")
        mAuth = FirebaseAuth.getInstance()
        user = mAuth!!.currentUser

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.web_client_id))
            .requestEmail()
            .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this@MainActivity /* FragmentActivity */, this /* OnConnectionFailedListener */)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
        mGoogleApiClient!!.connect()

//        This is Object of Kotlin using for single parameter and it is best
        ConstantMethod.initSharedPrefs(this@MainActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()  //This method for initialize the View with Id of Views....
        initMembers() //This method for initialize the members value of list of dbHelpter etc.
        setHeaderView()

//        btnSparkAnimate!!.pressOnTouch(false)

        //Shared Preference object here
        sharedPrefs = SharedPrefs(this@MainActivity)
        sharedPrefs!!.classReference = "MainActivity"

        val intentLang = intent
         countryCode = intentLang.getStringExtra("countryCode")
        langCode = intentLang.getStringExtra("langCode")
        Log.e("countryCode ", " :: "+sharedPrefs!!.countryCode!!)

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                // Log and toast
//                val msg = getString(R.string.msg_token_fmt, token)
//                Log.d(TAG, msg)
//                Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
            })

        Log.e("$TAG :: ", "Main onCreate called !!")
        initToolBarWithDrawerNav()  //This method for initialize the Animated Drawer with Toolbar....
        initCategoryList() //This method for initialize the constant value of category....
        setupCategoryRecyclerView() // This method for setting the Category RecyclerView with adapter..

        if(ConnectionStatus.CheckInternetConnection(this@MainActivity)){
            if(articleList.size > 0){
                swipeStackAdapter = NewsSwipeStackAdapter(this@MainActivity, articleList, "MainActivity")
                swipeStackLoader!!.adapter = swipeStackAdapter
            swipeStackAdapter!!.notifyDataSetChanged()
                Log.e("atcle size onStopif:: ", ""+articleList.size)
            }else{
                Log.e("article size onStop:: ", ""+articleList.size)
                loadJsonData(pageNo,sharedPrefs!!.category!!, sharedPrefs!!.countryCode!!)
            }
        }else{
            onHideLoading()
            errorLayout?.visibility = View.VISIBLE
            showErrorMessage(
                R.drawable.oops, resources.getString(
                    R.string.oopString
                ), resources.getString(R.string.noInternetString))
        }

        fabNewsDetail.setOnClickListener(this)
        fabSwipeRight!!.setOnClickListener(this)
        loadMoreButton!!.setOnClickListener(this)
        newsNavViews!!.setNavigationItemSelectedListener(this)
        retryButton!!.setOnClickListener { loadJsonData(pageNo,sharedPrefs!!.category!!, sharedPrefs!!.countryCode!!) }

        //        swipeStackLoader.setAllowedSwipeDirections(SwipeStack.SWIPE_DIRECTION_ONLY_LEFT);
        //        btnSparkAnimate.pressOnTouch(false);

        /*fabSwipeLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipeStackLoader.onViewSwipedToLeft();
            }
        });*/

        swipeStackLoader!!.setSwipeProgressListener(object : SwipeStack.SwipeProgressListener {
            override fun onSwipeStart(position: Int) {
                fabNewsDetail.show()
            }

            override fun onSwipeProgress(position: Int, progress: Float) {
                Log.e("swipe prog. ", "$position :: $progress")

//                fabNewsDetail.show()
                if(progress > 0.2){
                    if(user != null){
                        swipeStackLoader!!.allowedSwipeDirections = SwipeStack.SWIPE_DIRECTION_BOTH

                    }else{
                        swipeStackLoader!!.allowedSwipeDirections = SwipeStack.SWIPE_DIRECTION_ONLY_LEFT
                        val snackbar = Snackbar.make(mainNewsCoLayt!!, "If you add News Article in Favourites. " +
                                "So, please login to access this feature.", Snackbar.LENGTH_LONG)
                        snackbar.setAction("OK") {
                            snackbar.dismiss()
                        }
                        snackbar.show()
                    }
                }

                //                btnSparkAnimate.setInactiveImage(R.drawable.ic_favorite_border);
            }

            override fun onSwipeEnd(position: Int) {
//                fabSwipeRight!!.show()
//                btnSparkAnimate!!.visibility = View.VISIBLE


                /* if(dbHelper.getCountDBTable()>0){
                    Log.e("cursor count :: ", ""+dbHelper.getCountDBTable()+1);
                    tvCountFavNews.setVisibility(View.VISIBLE);
                    tvCountFavNews.setText(String.valueOf(dbHelper.getCountDBTable()+1));
                }else {
                    tvCountFavNews.setVisibility(View.GONE);
                }*/
            }
        })

        swipeStackLoader!!.setListener(object : SwipeStack.SwipeStackListener {
            override fun onViewSwipedToLeft(position: Int) {

                Log.e("swipe dirn. ", " :: " + swipeStackLoader!!.currentPosition)
                //                fabSwipeLeft.hide();
//                fabSwipeRight!!.hide()
//                tvCountFavNews!!.visibility = View.GONE
//                btnSparkAnimate!!.visibility = View.GONE
            }

            override fun onViewSwipedToRight(position: Int) {
                Log.e("swipe dirn. ", " :: " + swipeStackLoader!!.currentPosition)
                addFavouritesData(position)
            }

            override fun onStackEmpty() {
                fabNewsDetail.hide()
                rlFavouriteFab.visibility = View.GONE
                Log.e("swipe dirn. ", " :: " + swipeStackLoader!!.currentPosition)
                rlLoadMoreContent!!.visibility = View.VISIBLE
//                mInterstitialAd!!.loadAd(AdRequest.Builder().build())

                /*mInterstitialAd!!.adListener = object : AdListener(){
                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        showInterstitialAd()
                    }
                }*/
            }
        })
    }

    private fun tapTargetViewImplement(viewMenuItem : View){
        fabSwipeFavLeft.show()
        fabSwipeFavRight.show()

        val tapTargetView = TapTargetSequence(this)
            .targets(
                TapTarget.forView(viewMenuItem, "Search everything from a perfect keyword",
                    "When tap on a Search Icon. It opens a Search Page that's why typing keword get latest News Article.")
                    .outerCircleColor(R.color.colorPrimary)
                    .targetCircleColor(R.color.colorDimGrey)
                    .textColor(R.color.colorWhite)
                    .transparentTarget(true)
                    .descriptionTextColor(R.color.colorWhite)
                    .cancelable(false) ,

                TapTarget.forView(findViewById(R.id.fabSwipeFavLeftMain), "Left Swipe to see Next News Article",
                    "You can see next news article when card swipe on Left.")
                    .outerCircleColor(R.color.colorPrimary)
                    .targetCircleColor(R.color.colorDimGrey)
                    .textColor(R.color.colorWhite)
                    .transparentTarget(true)
                    .descriptionTextColor(R.color.colorWhite)
                    .cancelable(false) ,

                TapTarget.forView(
                    findViewById(R.id.fabSwipeFavRightMain), "Right Swipe to Add Article in Favourites",
                    "You can store your news article in favourites when you login first. If your don't login then not add News in Favourites."
                )
                    .outerCircleColor(R.color.colorPrimary)
                    .targetCircleColor(R.color.colorDimGrey)
                    .textColor(R.color.colorWhite)
                    .transparentTarget(true)
                    .descriptionTextColor(R.color.colorWhite)
                    .cancelable(false),

                TapTarget.forView(findViewById(R.id.fabNewsDetail), "News Full Detail",
                    "You can see full detail of current article when you tap on this button.")
                    .outerCircleColor(R.color.colorPrimary)
                    .targetCircleColor(R.color.colorDimGrey)
                    .textColor(R.color.colorWhite)
                    .transparentTarget(true)
                    .descriptionTextColor(R.color.colorWhite)
                    .cancelable(false)
            )
            .listener(object : TapTargetSequence.Listener {

                override fun onSequenceStep(lastTarget: TapTarget?, targetClicked: Boolean) {

                }

                override fun onSequenceFinish() {
                    fabSwipeFavLeft.hide()
                    fabSwipeFavRight.hide()
                }

                override fun onSequenceCanceled(lastTarget: TapTarget) {
                    // Boo
                }
            })
        tapTargetView.start()
    }

    fun takeScreenshot(view : View): Bitmap {
        view.isDrawingCacheEnabled = true
        return Bitmap.createBitmap(view.drawingCache)
    }

    private var imagePath: String? = null
    private var imageFile:File?=null

    fun saveBitmap(bitmap: Bitmap) {

        val now = Date()
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now)
        imagePath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg"

        Log.e("image path ", " :: $imagePath")
        imageFile = File(imagePath)
        val fos: FileOutputStream
        try {
            fos = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            fos.flush()
            fos.close()
        } catch (e: FileNotFoundException) {
            Log.e("GREC", e.message, e)
        } catch (e: IOException) {
            Log.e("GREC", e.message, e)
        }
    }

    private fun shareIt(imageFile:File) {

        Log.e("image File ", " :: $imageFile")
        val uri = Uri.fromFile(imageFile)
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "image/*"
//        val shareBody = "In Tweecher, My highest score with screen shot"
//        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "My Tweecher score")
//        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri)

        try {
        startActivity(Intent.createChooser(intent, "Share Screenshot via "))
    } catch (e:ActivityNotFoundException) {
        Toast.makeText(this@MainActivity, "No App Available", Toast.LENGTH_SHORT).show()
    }
    }

    /*private fun showInterstitialAd() {
        if (mInterstitialAd!!.isLoaded) {
            mInterstitialAd!!.show()
        }
    }*/

    private fun addFavouritesData(position: Int){

        val articleModel = articleList[position]
        var result = false
        var isDuplicate = false

//        btnSparkAnimate!!.visibility = View.GONE
//        fabSwipeRight!!.setImageResource(R.drawable.ic_favorite_border)

        val selectQuery = "SELECT  * FROM " + Constant.TABLE_NAME
        val db = dbHelper.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        if (cursor.count > 0) {
            if (cursor.moveToFirst()) {
                outer@do {
                    if ((cursor.getString(cursor.getColumnIndex(Constant.NEWS_TITLE))).equals(articleModel.title, true)) {
                        isDuplicate = true
                        break@outer
                    } else {
                        Log.e("cursor comp. :: ", "data is not inserted Successfully !!")
                    }
                } while (cursor.moveToNext())

                if (!isDuplicate) {

                    result = dbHelper.insertFavouriteNewsData(articleModel.title?:"", articleModel.description?:"",
                        articleModel.url?:"", articleModel.urlToImage?:"",
                        articleModel.source!!.name?:"", articleModel.author?:"",
                        articleModel.publishedAt?:"", articleModel.source!!.id?:"")

                    val userFavData : HashMap<String, String> = HashMap()
                    userFavData.set("Title", articleModel.title?:"")
                    userFavData.set("Desc", articleModel.description?:"")
                    userFavData.set("Url", articleModel.url?:"")
                    userFavData.set("UrlToImage", articleModel.urlToImage?:"")
                    userFavData.set("Author", articleModel.author?:"")
                    userFavData.set("PublishedAt", articleModel.publishedAt?:"")
                    userFavData.set("SourceName", articleModel.source!!.name?:"")
                    userFavData.set("SourceId", articleModel.source!!.id?:"")

                    rlFavouriteFab.visibility = View.VISIBLE
                    tvCountFavNews!!.visibility = View.VISIBLE
                    btnSparkAnimate!!.visibility = View.VISIBLE

                    btnSparkAnimate!!.playAnimation()
                    btnSparkAnimate!!.setAnimationSpeed(1.5f)
//        btnSparkAnimate!!.pressOnTouch(false)

                    if (btnSparkAnimate!!.isChecked) {
                        Log.e("btnSpark :: ", "" + btnSparkAnimate!!.isChecked)
                        btnSparkAnimate!!.setActiveImage(R.drawable.ic_favorite_border)
                        btnSparkAnimate!!.setInactiveImage(R.drawable.ic_favorite)
                        btnSparkAnimate!!.isChecked = false
                    } else {
                        Log.e("btnSpark :: ", "" + btnSparkAnimate!!.isChecked)
                        btnSparkAnimate!!.isChecked = true
                        btnSparkAnimate!!.setActiveImage(R.drawable.ic_favorite)
                        btnSparkAnimate!!.setInactiveImage(R.drawable.ic_favorite_border)
                    }

                    tvCountFavNews!!.visibility = View.GONE
                    btnSparkAnimate!!.visibility = View.GONE


                    Handler().postDelayed({
                        rlFavouriteFab.visibility = View.GONE

                    }, 2000)

                    dbReference!!.child(user!!.uid).push().setValue(userFavData).addOnCompleteListener { task ->
                        if(task.isSuccessful){
                            Log.e("addValueDbError ", " :: task is complete")
                        }
                    }
                    Log.e("cursor count :: ", "" + (cursor.count + 1))
                    tvCountFavNews!!.visibility = View.VISIBLE
                    tvCountFavNews!!.text = (cursor.count + 1).toString()

                    Log.e("cursor count :: ", "" + (cursor.count + 1))
                    Snackbar.make(mainNewsCoLayt!!, "Add News in Favourites :: " + (cursor.count + 1), Snackbar.LENGTH_SHORT).show()
                } else {
                    Snackbar.make(mainNewsCoLayt!!, "News is already available in Favourites !!", Snackbar.LENGTH_SHORT).show()
                }
            }
        } else {
            tvCountFavNews!!.visibility = View.VISIBLE
            tvCountFavNews!!.text = (cursor.count + 1).toString()
            result = dbHelper.insertFavouriteNewsData(articleModel.title?:"", articleModel.description?:"",
                articleModel.url?:"", articleModel.urlToImage?:"",
                articleModel.source!!.name?:"", articleModel.author?:"",
                articleModel.publishedAt?:"", articleModel.source!!.id?:"")

            Log.e("cursor count :: ", "" + (cursor.count + 1))
            Snackbar.make(mainNewsCoLayt!!, resources.getString(R.string.addNewsInFav)+" :: " + (cursor.count + 1), Snackbar.LENGTH_SHORT).show()
        }

        if (result) {
            Log.e("result :: ", "data is inserted Successfully !!")
        } else {
            Log.e("result :: ", "data is not inserted !!")
        }

        cursor.close()
    }

    private fun setHeaderView() {

        val header:View = newsNavViews!!.getHeaderView (0)
       val tvUserNameHV:TextView =  header.findViewById (R.id.tvUserNameHV)
        val tvUserEmailHV:TextView =  header.findViewById (R.id.tvUserEmailHV)
        val imgUserHV:ImageView =  header.findViewById (R.id.imgUserHV)
        val rlUserDetailLayout:RelativeLayout = header.findViewById(R.id.rlUserDetailLayout)
        val rlLoginLayout:RelativeLayout = header.findViewById(R.id.rlLoginLayout)
        val btnLogin:Button = header.findViewById(R.id.btnLogin)

        val userDataValue : UserData = Constants.userDataObject.userData

        tvUserNameHV.text = userDataValue.name
        tvUserEmailHV.text = userDataValue.email
        Glide.with(this).load(userDataValue.photoUrl)
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .into(imgUserHV)


        if(user != null){
            rlUserDetailLayout.visibility = View.VISIBLE
            rlLoginLayout.visibility = View.GONE
            newsNavViews!!.menu.findItem(R.id.logoutMenu).isVisible = true
        }else{
            rlUserDetailLayout.visibility = View.GONE
            rlLoginLayout.visibility = View.VISIBLE
            newsNavViews!!.menu.findItem(R.id.logoutMenu).isVisible = false
        }

        btnLogin.setOnClickListener {
            val intentLogin = Intent(this@MainActivity, LoginWithGoogleOrFBPage::class.java)
            intentLogin.putExtra("displayView", "MainActivity")
            startActivity(intentLogin)
        }


    }

    private fun setupCategoryRecyclerView() {
        layoutManagerCategory = androidx.recyclerview.widget.LinearLayoutManager(
            applicationContext,
            androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
            false
        )
        newsCategoryRV!!.layoutManager = layoutManagerCategory
        newsCategoryRV!!.setHasFixedSize(true)
        newsCategoryRV!!.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        categoryAdapter = NewsCategoryAdapter(this, categoryList!!)
        newsCategoryRV!!.adapter = categoryAdapter
    }

    private fun initCategoryList(){
        Log.e("getCategory val+Pos :: ", " :: " + sharedPrefs!!.category + " :: " + sharedPrefs!!.categoryPos)
        val resource : Resources = resources
        val array:Array<String> = resource.getStringArray(R.array.category)
        Log.e("categ array lentg ", " :: "+array.size)

        categoryList = ArrayList()
        categoryListDuplicate = ArrayList()
        categoryListDuplicate!!.add("general")
        categoryListDuplicate!!.add("business")
        categoryListDuplicate!!.add("science")
        categoryListDuplicate!!.add("technology")
        categoryListDuplicate!!.add("entertainment")
        categoryListDuplicate!!.add("sports")
        categoryListDuplicate!!.add("health")

        for(s in array){
            categoryList!!.add(s)
        }
//        Collections.addAll(categoryList, array);
 /*       categoryList = ArrayList()

        categoryList!!.add("")*/
        /*categoryList!!.add("politics")
        categoryList.add("gaming");*/
        /*categoryList.add("music");*/
       /* categoryList!!.add("")
        categoryList!!.add("")
        categoryList!!.add("")
        categoryList!!.add("")
        categoryList!!.add("")*/
        // Sorting
        /*Collections.sort(categoryList,  String.CASE_INSENSITIVE_ORDER);
        for (String list:categoryList ) {
            Log.e("Categ Value :: ", ""+list);
        }*/
    }

    private fun initMembers() {
        articleList = ArrayList()

        //        Here 'this' is a MainActivity Context....
        dbHelper = NewsDbHelper(this@MainActivity)
    }

    private fun initToolBarWithDrawerNav() {
        setSupportActionBar(newsToolbar)
        if (supportActionBar != null) {
            supportActionBar!!.title = resources.getString(R.string.top_headline)
        }

        drawerToggle = ActionBarDrawerToggle(this@MainActivity, newsDrawerLayout, newsToolbar,
            R.string.open_nav,
            R.string.close_nav
        )
        newsDrawerLayout!!.addDrawerListener(drawerToggle!!)
        drawerToggle!!.syncState()

        newsDrawerLayout!!.useCustomBehavior(Gravity.START)
        newsDrawerLayout!!.setViewScale(Gravity.START, 0.9f) //set height scale for main view (0f to 1f)
        newsDrawerLayout!!.setViewElevation(Gravity.START, 20f)//set main view elevation when drawer open (dimension)
        newsDrawerLayout!!.setViewScrimColor(Gravity.START, Color.TRANSPARENT)//set drawer overlay coloe (color)
        newsDrawerLayout!!.setRadius(Gravity.START, 25f)//set end container's corner radius (dimension)
    }

    private fun onShowLoading() {
        findViewById<View>(R.id.shimmerCardLoading).visibility = View.VISIBLE
    }

    private fun onHideLoading() {
        findViewById<View>(R.id.shimmerCardLoading).visibility = View.GONE
    }

    override fun onStart() {
        super.onStart()
        Log.e("$TAG :: ", "Main onStart called !!")
    }

    override fun onResume() {
        registerReceiver(internetBroadCastReceiver, intentFilter)
        super.onResume()
        Log.e("$TAG :: ", "Main onResume called !!")
    }

    override fun onPause() {
        unregisterReceiver(internetBroadCastReceiver);
        super.onPause()
        Log.e("$TAG :: ", "Main onPause called !!")
    }

    override fun onStop() {
        super.onStop()
        Log.e("$TAG :: ", "Main onStop called !!")
    }

    override fun onDestroy() {

//        unregisterReceiver(internetBroadCastReceiver);
        super.onDestroy()
        Log.e("$TAG :: ", "Main onDestroy called !!")
    }

    override fun onRestart() {
        super.onRestart()
        if(articleList.size > 0){
            swipeStackAdapter = NewsSwipeStackAdapter(this@MainActivity, articleList, "MainActivity")
            swipeStackLoader!!.adapter = swipeStackAdapter
            swipeStackAdapter!!.notifyDataSetChanged()
            Log.e("atcle size onStopif:: ", ""+articleList.size)
        }else{
            Log.e("article size onStop:: ", ""+articleList.size)
            loadJsonData(pageNo,sharedPrefs!!.category!!, sharedPrefs!!.countryCode!!)
        }
        Log.e("$TAG :: ", "Main onRestart called !!")

//        this is used for setting the theme of app as day/night depends on...
        ConstantMethod.initSharedPrefs(this@MainActivity)
    }

    private fun initViews() {
        errorLayout = findViewById(R.id.errorLayout)
        errorImageView = findViewById(R.id.errorImage)
        errorTitle = findViewById(R.id.errorTitle)
        errorMessage = findViewById(R.id.errorMessage)
        retryButton = findViewById(R.id.btnRetry)
        newsCategoryRV = findViewById(R.id.newsCategoryRV)

        swipeStackLoader = findViewById(R.id.swipeStack)
        tvdataNotFound = findViewById(R.id.TextNoFound)
        imgArticle = findViewById(R.id.imgArticle)
        rlLoadMoreContent = findViewById(R.id.rlLoadMoreContent)
        loadMoreButton = findViewById(R.id.btnLoadMore)
        fabSwipeLeft = findViewById(R.id.fabSwipeLeft)
        fabSwipeRight = findViewById(R.id.fabSwipeRight)
        mainNewsCoLayt = findViewById(R.id.mainNewsCoLayt)
        tvCountFavNews = findViewById(R.id.tvCountFavNews)

        newsToolbar = findViewById(R.id.mainNews_toolbar)
        newsDrawerLayout = findViewById(R.id.mainNewsDrawerLayout)
        newsNavViews = findViewById(R.id.newsNavigationView)
        btnSparkAnimate = findViewById(R.id.btnSparkAnimate)

        fabNewsDetail = findViewById(R.id.fabNewsDetail)
        rlFavouriteFab = findViewById(R.id.rlFavouriteFab)
        fabSwipeFavLeft = findViewById(R.id.fabSwipeFavLeftMain)
        fabSwipeFavRight = findViewById(R.id.fabSwipeFavRightMain)

        //Share View News initialization here.....
        title = findViewById(R.id.titleShare)
        desc = findViewById(R.id.tvDescptShare)
        source = findViewById(R.id.sourceShare)
        author = findViewById(R.id.authorTV)
        publishedAt = findViewById(R.id.publishedAt)
        time = findViewById(R.id.timeShare)
        urlLink = findViewById(R.id.urlLinkShare)

        newsImgView = findViewById(R.id.newsImgViewShare)
        mProgBar = findViewById(R.id.progress_photo_load)
        rlScreenshotShare = findViewById(R.id.rlScreenshotShare)
        tvShareScreenShot = findViewById(R.id.tvShareScreenShot)
        tvShareScreenCancel = findViewById(R.id.tvShareScreenCancel)
    }

    override fun onClick(v: View) {

        when (v.id) {

            R.id.btnLoadMore -> {

                pageNo++
                Log.e("page No. :: ", " :: $pageNo :: $quotientResult")
                if(pageNo <= quotientResult){
                    loadJsonData(pageNo,sharedPrefs!!.category!!, sharedPrefs!!.countryCode!!)
                }else{
                    showErrorMessage(
                        R.drawable.no_result, resources.getString(
                            R.string.noArticals
                        ),
                        resources.getString(R.string.sorryString)+
                            " ${sharedPrefs!!.category} "+resources.getString(R.string.catgString)
                                + resources.getString(R.string.tryAnotherString))

                    retryButton!!.visibility = View.GONE
                }
                rlLoadMoreContent!!.visibility = View.GONE
            }

            R.id.fabSwipeLeft -> {
            }

            R.id.fabSwipeRight -> {
                tvCountFavNews!!.visibility = View.GONE
                val intentFavourite = Intent(this@MainActivity, FavouriteActivity::class.java)
                startActivity(intentFavourite)
            }

            R.id.fabNewsDetail -> {
                Log.e("topview curntPos ", " :: "+swipeStackLoader!!.currentPosition)

                val currentPos = swipeStackLoader!!.currentPosition
                val articleModel = articleList[currentPos]

                val intent =  Intent(this@MainActivity, NewsDetailActivity::class.java)
                intent.putExtra("url", articleModel.url)
                intent.putExtra("title", articleModel.title)
                intent.putExtra("news_img", articleModel.urlToImage)
                intent.putExtra("date", articleModel.publishedAt)
                intent.putExtra("source_name",articleModel.source!!.name)
                intent.putExtra("author",articleModel.author)
                startActivity(intent)

            }
        }
    }


     private fun checkPermission() : Boolean{
        val result = ContextCompat.checkSelfPermission(this@MainActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
         return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() =
        if(ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE)
        }else {
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE)
        }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Bitmap bitmap = takeScreenshot();
//                saveBitmap(bitmap);
//                shareIt();
            } else {
                Log.e("value", "Permission Denied, You cannot use Storage Local Drive. ");
            }
        }
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {

        when(p0.itemId){

            R.id.homeNews ->{
                swipeStackLoader!!.topView
                Log.e("topView ", " :: "+swipeStackLoader!!.topView.id)
            }
            R.id.newsSource -> startActivity(Intent(this@MainActivity, SourcesActivity::class.java))

            R.id.searchNews -> startActivity(Intent(this@MainActivity, SearchActivity::class.java))

            R.id.notifications -> startActivity(Intent(this@MainActivity, NotificationActivity::class.java))

            R.id.settings -> startActivity(Intent(this@MainActivity, SettingActivity::class.java))

            R.id.favouriteArticle -> {
                startActivity(Intent(this@MainActivity, FavouriteActivity::class.java))
            }

            R.id.shareApp ->{
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.putExtra(
                    Intent.EXTRA_TEXT,
                    "I suggest this app for you : https://play.google.com/store/apps/details?id=com.panky.newsincards"
                )
                intent.setType("text/plain")
                startActivity(intent)
            }

            R.id.rateApp -> {
                val uri: Uri = Uri.parse("market://details?id=" + packageName);
                val goToMarket: Intent = Intent(Intent.ACTION_VIEW, uri)
                goToMarket.addFlags(
                    Intent.FLAG_ACTIVITY_NO_HISTORY or
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK
                )
                try {
                    startActivity(goToMarket)
                } catch (e: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())
                        )
                    )
                }
            }

            R.id.helpFeed -> startActivity(Intent(this@MainActivity, HelpActivity::class.java))

            R.id.changeLang ->{
                val intentLang = Intent(this@MainActivity, LanguagePage::class.java)
                intentLang.putExtra("getType","changeLang")
                startActivity(intentLang)
            }

            R.id.changeCountry ->{
                val intentCountry = Intent(this@MainActivity, ChangeCountryPage::class.java)
                intentCountry.putExtra("fromClass", "MainPage")
                startActivity(intentCountry)
            }

            R.id.logoutMenu ->{
                mainLogoutDialog()
            }
        }

        if (newsDrawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            newsDrawerLayout!!.closeDrawer(GravityCompat.START)
        }
       return true
    }

    private fun mainLogoutDialog(){
        val alertBuilder : AlertDialog.Builder = AlertDialog.Builder(this@MainActivity)
        alertBuilder.setCancelable(false)

        val inflater = layoutInflater
        val deleteAllNewsView = inflater.inflate(R.layout.logout_layout, null)
        val tvlogoutConfirm:TextView = deleteAllNewsView.findViewById(R.id.tvlogoutConfirm)
        val tvlogoutCancel:TextView = deleteAllNewsView.findViewById(R.id.tvlogoutCancel)
        alertBuilder.setView(deleteAllNewsView)

        val alertDialog: AlertDialog = alertBuilder.create();
        alertDialog.show()

        tvlogoutCancel.setOnClickListener { alertDialog.dismiss() }

        tvlogoutConfirm.setOnClickListener {
            alertDialog.dismiss()

            val userDataValue : UserData = Constants.userDataObject.userData
            if(userDataValue.socialType.equals("google")){
                signOut()
                user!!.delete().addOnCompleteListener(OnCompleteListener {
                    recreate()
                })

            }else{
                Logout()
                user!!.delete().addOnCompleteListener(OnCompleteListener {
                    recreate()
                })
            }
        }
    }

    private fun signOut() {
        // Firebase sign out
        mAuth!!.signOut()

        // Google sign out
        Auth.GoogleSignInApi.signOut(mGoogleApiClient)
    }

    private fun Logout(){
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
    }

    //    When Internet is not Available then this layout is appear ....
    private fun showErrorMessage(ImgId: Int, title: String, msg: String) {

        if (errorLayout!!.visibility == View.GONE) {
            errorLayout!!.visibility = View.VISIBLE
        }

        errorImageView!!.setImageResource(ImgId)
        errorTitle!!.text = title
        errorMessage!!.text = msg
    }


    private fun loadJsonData(page:Int, catValue: String, countryCode:String) {

        onShowLoading()
        if (!articleList.isEmpty()) {
            articleList.clear()
            swipeStackLoader!!.resetStack()
        }

        errorLayout!!.visibility = View.GONE
        Log.e("adapter size :: ", "" + articleList.size)
        val apiInterface = ApiClient.apiClient.create(ApiInterface::class.java)

        newsViewModel.getNewsRepository(countryCode, catValue, pageNo, 10, BuildConfig.API_KEY)
            ?.observe(this, Observer<News> {
                Log.e("newsListSize", " :: ${it.articleList?.size}")
            })

        val utils = Utils(this@MainActivity)
        val country = utils.country
        val language = utils.language
        Log.e("lang :: ", language)

        //        This call is used for calling RESTFUL API HERE.....
        val call: Call<News>
        call = apiInterface.getNews(countryCode, catValue,
            pageNo,10, BuildConfig.API_KEY)

        Log.e("call :: ", "" + call)
        Log.e("country :: ", "" + country)
        Log.e("category :: ", "" + catValue)
        Log.e("api_key :: ", "" + BuildConfig.API_KEY)
        call.enqueue(object : Callback<News> {
            override fun onResponse(call: Call<News>, response: Response<News>) {

                swipeStackLoader!!.resetStack()
                Log.e("response :: ", ""+response)

                if (response.isSuccessful && response.body()!!.articleList != null) {
                    val totalResult = response.body()?.totalResults
                    if(totalResult!!.rem(10) == 0){
                        quotientResult = totalResult/10
                    }else{
                        quotientResult = totalResult/10 + 1
                    }
                    Log.e("resp totalResult :: ", "" + response.body()?.totalResults)
                    onHideLoading()
                    if (!articleList.isEmpty()) {
                        articleList.clear()
                    }

                    articleList = response.body()!!.articleList!!
                    swipeStackAdapter = NewsSwipeStackAdapter(this@MainActivity, articleList, "MainActivity")
                    swipeStackLoader!!.adapter = swipeStackAdapter

                    swipeStackAdapter!!.setShareNewsListener(this@MainActivity)
                    swipeStackAdapter!!.notifyDataSetChanged()
                    fabNewsDetail.show()

                } else {
                    val errorCode: String
                    Log.e("Error Code :: ", "" + response.code())
                    when (response.code()) {
                        404 -> {
                            errorCode = "404 not found"
                        }
                        500 -> {
                            errorCode = "500 server broken"
                        }
                        else -> errorCode = "Unknown error"
                    }
                    showErrorMessage(R.drawable.no_result, "No Result", "Please Try Again \n$errorCode")
                    onHideLoading()
                    fabNewsDetail.hide()
                }
            }

            override fun onFailure(call: Call<News>, t: Throwable) {
                Log.e("Error :: ", "" + t.toString())
                showErrorMessage(R.drawable.oops, "Oops...", "Network Failure, \nPlease Try Again ")
                onHideLoading()
                fabNewsDetail.hide()
            }
        })
    }

     fun trimCache(context:Context) {
      try {
         val dir:File = context.cacheDir
         if (dir != null && dir.isDirectory()) {
            deleteDir(dir)
         }
      } catch (e:Exception) {
         Log.e("", ""+e.message.toString())
      }
   }

   fun deleteDir(dir: File) : Boolean{
      if (dir != null && dir.isDirectory()) {
         val children = dir.list()
         for (i in 0 until children.size) {
            val success:Boolean = deleteDir(File(dir, children[i]));
            if (!success) {
               return false;
            }
         }
      }
      // The directory is now empty so delete it
      return dir.delete();
   }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)

        if(sharedPrefs?.MainGuiderShow!!){
            Handler().post(Runnable {
                val viewItem:View = findViewById(R.id.searchView)
                tapTargetViewImplement(viewItem)
                sharedPrefs?.MainGuiderShow = false
            })
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.setting -> {
                startActivity(Intent(this@MainActivity, SettingActivity::class.java))
            }

            R.id.news_notification -> {
                startActivity(Intent(this@MainActivity, NotificationActivity::class.java))
            }

            R.id.searchView -> startActivity(Intent(this@MainActivity, SearchActivity::class.java))

            R.id.news_reset -> {
                swipeStackLoader!!.resetStack()
                rlLoadMoreContent.visibility = View.GONE
                errorLayout!!.visibility = View.GONE
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (newsDrawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            newsDrawerLayout!!.closeDrawer(GravityCompat.START)
        } else if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }else if(rlScreenshotShare.visibility == View.VISIBLE){
            rlScreenshotShare.visibility = View.GONE
            fabNewsDetail.show()
        }else{
            this.doubleBackToExitPressedOnce = true
            Toast.makeText(this, resources.getString(R.string.backAgainString), Toast.LENGTH_SHORT).show()
            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
    }

    //    This adapter is here is inner to MainClass for updating news category according to category value...
    internal inner class NewsCategoryAdapter(private val mContext: Context, private val categoryList: MutableList<String>)
        : androidx.recyclerview.widget.RecyclerView.Adapter<NewsCategoryAdapter.CategoryViewHolder>() {

        private var selectedPos = sharedPrefs!!.categoryPos
        private var selectedCatValue : String? = null
//        private var categoryValue:String?=null

        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CategoryViewHolder {
            val view = LayoutInflater.from(mContext).inflate(R.layout.news_category_tab, viewGroup, false)
            return CategoryViewHolder(view)
        }

        override fun onBindViewHolder(categoryViewHolder: CategoryViewHolder, position: Int) {

            val catValue = categoryListDuplicate!![position]

            if (selectedPos == position) {
                selectedCatValue = categoryListDuplicate!!.get(selectedPos)
                Log.e("selectedCatValue :: ", selectedCatValue)
                if (sharedPrefs!!.isNightModeEnabled) {
                    categoryViewHolder.categoryTV.setTextColor(Color.parseColor("#000000"))
                } else {
                    categoryViewHolder.categoryTV.setTextColor(Color.parseColor("#ffffff"))
                }
                categoryViewHolder.itemView.isSelected = true

            } else {
                categoryViewHolder.itemView.isSelected = false/*#D0ACACAC*/
                if (sharedPrefs!!.isNightModeEnabled) {
                    categoryViewHolder.categoryTV.setTextColor(Color.parseColor("#D0818080"))
                } else {
                    categoryViewHolder.categoryTV.setTextColor(Color.parseColor("#D3D3D3"))
                }
            }
            val str = categoryList[position]
            val cap = str.substring(0, 1).toUpperCase() + str.substring(1)
            categoryViewHolder.categoryTV.text = cap

            categoryViewHolder.itemView.setOnClickListener {

                notifyItemChanged(selectedPos)
                selectedPos = position
                notifyItemChanged(selectedPos)

                pageNo = 1
                /*store the value of category and its position as well...*/
                sharedPrefs!!.category = catValue
                sharedPrefs!!.categoryPos = selectedPos
                Log.e("catValue ", " :: $catValue")

//                This code for not selected category multiple times as well...
                if(selectedCatValue.equals(categoryListDuplicate!!.get(selectedPos), true)){
                    if(articleList.size == 0) {
                        if(ConnectionStatus.CheckInternetConnection(this@MainActivity)){
                            rlLoadMoreContent!!.visibility = View.GONE
                            errorLayout!!.visibility = View.GONE
                            loadJsonData(pageNo,catValue, sharedPrefs!!.countryCode!!)

                        }else{
                            errorLayout?.visibility = View.VISIBLE
                            onHideLoading()
                        }
                    }else{
                        swipeStackAdapter = NewsSwipeStackAdapter(this@MainActivity, articleList, "MainActivity")
                        swipeStackLoader!!.adapter = swipeStackAdapter
                        swipeStackAdapter!!.notifyDataSetChanged()
//                        rlLoadMoreContent!!.visibility = View.GONE

                        categoryViewHolder.itemView.isClickable = false
                        errorLayout!!.visibility = View.GONE
                        Log.e("atcle size onStopif:: ", ""+articleList.size)
                        Log.e("size arrlist inner :: ", ""+articleList.size)
                    }
                }else{
                    /*mInterstitialAd!!.loadAd(AdRequest.Builder().build())

//                    showInterstitialAd()
                   mInterstitialAd!!.adListener = object : AdListener(){
                       override fun onAdLoaded() {
                           super.onAdLoaded()
//                           showInterstitialAd()
                       }
                   }*/
                   /* if (mInterstitialAd!!.isLoaded()) {
                        mInterstitialAd!!.show();
                    } else {
                        Log.e("Interstl ads ", " :: ads not show")
                    }*/

                    try {
                        trimCache(this@MainActivity)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if(ConnectionStatus.CheckInternetConnection(this@MainActivity)){
                        rlLoadMoreContent.visibility = View.GONE
                        errorLayout!!.visibility = View.GONE
                        loadJsonData(pageNo,catValue, sharedPrefs!!.countryCode!!)

                    }else{
                        errorLayout?.visibility = View.VISIBLE
                        onHideLoading()
                    }
                }
            }
        }

        override fun getItemCount(): Int {
            return categoryList.size
        }

        inner class CategoryViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

            val categoryTV: TextView = itemView.findViewById(R.id.categoryTV)
        }
    }

}
