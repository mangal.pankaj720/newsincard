package com.panky.newsincards.view.MainPage.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.panky.newsincards.models.News
import com.panky.newsincards.view.MainPage.repo.NewsRespository


class NewsViewModel : ViewModel() {

    private val newsRepo by lazy {
        NewsRespository()
    }

    fun getNewsRepository(country:String, category:String, page:Int, pageSize:Int, apiKey:String): LiveData<News>? {
        return newsRepo.getNewsData(country, category, page, pageSize, apiKey)
    }
}