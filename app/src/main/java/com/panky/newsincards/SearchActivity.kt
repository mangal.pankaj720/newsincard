package com.panky.newsincards

import android.Manifest
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View

import com.panky.newsincards.Adapter.NewsSwipeStackAdapter
import com.panky.newsincards.Constants.Constant
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.Utility.Utils
import com.panky.newsincards.api.ApiClient
import com.panky.newsincards.api.ApiInterface
import com.panky.newsincards.models.Article
import com.panky.newsincards.models.News

import link.fls.swipestack.SwipeStack
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager.HIDE_IMPLICIT_ONLY
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import android.os.Handler
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.core.app.ActivityCompat
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.panky.newsincards.Adapter.SearchBaseAdapter
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.gms.ads.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.panky.newsincards.BottomSheetFrag.SettingBottomSheetFragment
import com.panky.newsincards.Constants.ConstantMethod
import com.panky.newsincards.Constants.ShareNewsListener
import com.panky.newsincards.DBHelper.NewsDbHelper
import com.panky.newsincards.view.MainPage.view.MainActivity
import com.varunest.sparkbutton.SparkButton
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class SearchActivity : AppCompatActivity(), View.OnClickListener, Constant, ShareNewsListener {

    override fun getShareNewsPosition(position: Int) {
        Log.e("Share Pos ", " :: $position")

        fabNewsDetailSearch.hide()
        rlScreenshotShare.visibility = View.VISIBLE
        val utils = Utils(this@SearchActivity)
        val articleModel = articleList!![position]

        val requestOptions = RequestOptions()
        requestOptions.placeholder(utils.randomDrawbleColor)
        requestOptions.error(utils.randomDrawbleColor)
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)
        requestOptions.skipMemoryCache(true)
        requestOptions.centerCrop()

        Log.e("urlToImage ", " :: "+articleModel.urlToImage)

        Glide.with(this@SearchActivity)
            .load(articleModel.urlToImage)
            .apply(requestOptions)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    mProgBar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any?, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    mProgBar.visibility = View.GONE
                    return false
                }
            })
            .error(R.drawable.new_pic)
            .thumbnail(0.6f)
            .dontAnimate()
            .dontTransform()
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(newsImgView)


        Log.e("DB Data :: ","")
        title.text = articleModel.title

        if(!articleModel.description.isNullOrEmpty()){

            val result = Html.fromHtml(articleModel.description!!).toString().replace("\n", "").trim()
            desc.text = result
        }else{
            desc.visibility = View.GONE
        }

        source.text = articleModel.source?.name
        val outputDateFormat = articleModel.publishedAt?.subSequence(0, 19)
        Log.e("date value :: ", outputDateFormat as String?)
        time.text = String.format(" \u2022 "+utils.DateToTimeFormat(outputDateFormat.toString()))
        publishedAt.text = utils.DateFormat(outputDateFormat.toString())
        author.text = articleModel.author
        urlLink.movementMethod = LinkMovementMethod.getInstance()
        urlLink.text = articleModel.url

        tvShareScreenCancel.setOnClickListener {
            rlScreenshotShare.visibility = View.GONE
            fabNewsDetailSearch.show()
        }

        tvShareScreenShot.setOnClickListener {
            rlScreenshotShare.visibility = View.GONE
            fabNewsDetailSearch.show()
            if(checkPermission()){
                val view:View = findViewById(R.id.rlShareCardNews)
                val androidColors = resources.getIntArray(R.array.androidcolors)
                val randomAndroidColor = androidColors[Random().nextInt(androidColors.size)]
                view.setBackgroundColor(randomAndroidColor)

                val bitmap = takeScreenshot(view)
                saveBitmap(bitmap)
                shareIt(imageFile!!)
            }else {
                requestPermission()
            }

        }

    }

    companion object{
       private var pageNo : Int = 1
    }

    //Share News Views instance here...
    private lateinit var tvShareScreenCancel:TextView
    private lateinit var tvShareScreenShot:TextView
    private lateinit var rlScreenshotShare:RelativeLayout
    lateinit var title: TextView
    lateinit var desc: TextView
    lateinit var author: TextView
    lateinit var publishedAt: TextView
    lateinit var time: TextView
    lateinit var urlLink: TextView
    lateinit var newsImgView: ImageView
    lateinit var mProgBar: ProgressBar
    lateinit var source : TextView

    //Add item Views here...
    private lateinit var btnSparkAnimate: SparkButton
    private var tvCountFavNews: TextView? = null
    private lateinit var rlFavouriteFab:RelativeLayout
    private lateinit var fabAddNewsItem: FloatingActionButton

    private var etSearchQuery: EditText? = null
    private var swipeStackLoader: SwipeStack? = null
    private var articleList: List<Article>? = null
    private var swipeStackAdapter: NewsSwipeStackAdapter? = null
    private var sharedPrefs: SharedPrefs? = null
    private lateinit var imgBackArrow:ImageView
    private lateinit var imgOverflowDot:ImageView
    private lateinit var imgClose: ImageView
    private lateinit var rlSearchLoadMoreContent: RelativeLayout
    private lateinit var btnSearchLoadMore: Button

    private var errorLayout: RelativeLayout? = null
    private var errorImageView: ImageView? = null
    private var errorTitle: TextView? = null
    private var errorMessage: TextView? = null
    private lateinit var retryButton:Button
//    private lateinit var adViewBanner:AdView
    private lateinit var fabNewsDetailSearch: FloatingActionButton

    private var searchBaseAdapter: SearchBaseAdapter?=null
//    private var mInterstitialAd : InterstitialAd? =null

    /*Search Default String Values here ...*/
    private lateinit var relLayoutSearchConstant:RelativeLayout
    private lateinit var tvSearchTextValue: TextView
    private lateinit var lvSearchQuery:ListView
    private lateinit var dbHelper: NewsDbHelper

    //Firebase instance here....
    private var dbReference : DatabaseReference?= null
    private var mAuth: FirebaseAuth? = null
    private var user: FirebaseUser?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        ConstantMethod.initSharedPrefs(this@SearchActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        initViews()

      /*  mInterstitialAd = InterstitialAd(this)
        mInterstitialAd!!.adUnitId = resources.getString(R.string.interstitial_ad_unit_id)
        mInterstitialAd!!.loadAd(AdRequest.Builder().build())
        dbHelper = NewsDbHelper(this@SearchActivity)*/

        //Firebase instance initialize here....
        dbReference = FirebaseDatabase.getInstance().reference.child("UserFavouriteNews")
        mAuth = FirebaseAuth.getInstance()
        user = mAuth!!.currentUser

        articleList = ArrayList()
        sharedPrefs = SharedPrefs(this@SearchActivity)

        if(sharedPrefs!!.isNightModeEnabled){
            imgBackArrow.setColorFilter(ContextCompat.getColor(this@SearchActivity, R.color.primaryDateColorNight),
                    android.graphics.PorterDuff.Mode.SRC_IN)
            imgOverflowDot.setColorFilter(ContextCompat.getColor(this@SearchActivity, R.color.primaryDateColorNight),
                    android.graphics.PorterDuff.Mode.SRC_IN)
        }else{
            imgBackArrow.setColorFilter(ContextCompat.getColor(this@SearchActivity, R.color.primaryDateColor),
                    android.graphics.PorterDuff.Mode.SRC_IN)
            imgOverflowDot.setColorFilter(ContextCompat.getColor(this@SearchActivity, R.color.primaryDateColor),
                    android.graphics.PorterDuff.Mode.SRC_IN)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            etSearchQuery?.setOnEditorActionListener(object : TextView.OnEditorActionListener {
                override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                        val searchQuery = etSearchQuery!!.text.toString()

                        if(!searchQuery.isEmpty()){
                            rlSearchLoadMoreContent.visibility = View.GONE
                            loadJsonData(searchQuery, sharedPrefs!!.selectDateFrom!!,
                                    sharedPrefs!!.selectDateTo!!, pageNo, sharedPrefs!!.newsSortsBy!!)
                        }else{
                            Toast.makeText(this@SearchActivity,"Please enter search query...", Toast.LENGTH_SHORT).show()
                        }


                        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.toggleSoftInput(HIDE_IMPLICIT_ONLY, 0)
                        return true
                    }
                    return false
                }
            })
        }

        etSearchQuery!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {


                if(s.isEmpty()){
                    etSearchQuery!!.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0)
                    imgClose.visibility = View.GONE
                    relLayoutSearchConstant.visibility = View.VISIBLE
                }else{
                    imgClose.visibility = View.VISIBLE
                    relLayoutSearchConstant.visibility = View.GONE
                    etSearchQuery!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                }
            }
            override fun afterTextChanged(s: Editable) {}
        })

        imgOverflowDot.setOnClickListener(this)
        imgBackArrow.setOnClickListener(this)
        imgClose.setOnClickListener(this)
        btnSearchLoadMore.setOnClickListener(this)

        swipeStackLoader!!.setSwipeProgressListener(object : SwipeStack.SwipeProgressListener {
            override fun onSwipeStart(position: Int) {
                fabNewsDetailSearch.show()
            }

            override fun onSwipeProgress(position: Int, progress: Float) {
                Log.e("swipe prog. ", "$position :: $progress")

//                fabNewsDetail.show()
                if(progress > 0.2){
                    if(user != null){
                        swipeStackLoader!!.allowedSwipeDirections = SwipeStack.SWIPE_DIRECTION_BOTH

                    }else{
                        swipeStackLoader!!.allowedSwipeDirections = SwipeStack.SWIPE_DIRECTION_ONLY_LEFT
                        val snackbar = Snackbar.make(findViewById(R.id.rlAddNewsSearch), "If you add News Article in Favourites. " +
                                "So, please login to access this feature.", Snackbar.LENGTH_LONG)
                        snackbar.setAction("OK") {
                            snackbar.dismiss()
                        }
                        snackbar.show()
                    }
                }

                //                btnSparkAnimate.setInactiveImage(R.drawable.ic_favorite_border);
            }

            override fun onSwipeEnd(position: Int) {
//                fabSwipeRight!!.show()
//                btnSparkAnimate!!.visibility = View.VISIBLE


                /* if(dbHelper.getCountDBTable()>0){
                    Log.e("cursor count :: ", ""+dbHelper.getCountDBTable()+1);
                    tvCountFavNews.setVisibility(View.VISIBLE);
                    tvCountFavNews.setText(String.valueOf(dbHelper.getCountDBTable()+1));
                }else {
                    tvCountFavNews.setVisibility(View.GONE);
                }*/
            }
        })

        swipeStackLoader!!.setListener(object : SwipeStack.SwipeStackListener {

            override fun onViewSwipedToLeft(position: Int) {
               Log.e("onViewSwipedToLeft", " :: $position")
            }

            override fun onViewSwipedToRight(position: Int) {
                Log.e("onViewSwipedToRight", " :: $position")
                addFavouritesDataSource(position)
            }

            override fun onStackEmpty() {
                rlSearchLoadMoreContent.visibility = View.VISIBLE
                fabNewsDetailSearch.hide()

                rlFavouriteFab.visibility = View.GONE
                Log.e("swipe dirn. ", " :: " + swipeStackLoader!!.currentPosition)
               /* mInterstitialAd!!.loadAd(AdRequest.Builder().build())

                mInterstitialAd!!.adListener = object : AdListener(){
                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        showInterstitialAd()
                    }
                }*/
            }
        })

        searchConstantValueList()

        lvSearchQuery.onItemClickListener =  AdapterView.OnItemClickListener { parent, view, position, _ ->

            val searchValue:String = parent.getItemAtPosition(position) as String

            relLayoutSearchConstant.visibility = View.GONE
            loadJsonData(searchValue, sharedPrefs!!.selectDateFrom!!,
                sharedPrefs!!.selectDateTo!!, pageNo, sharedPrefs!!.newsSortsBy!!)

        }

        //AdView Code here...
//        adViewBanner.adSize = AdSize.BANNER
//        adViewBanner.adUnitId = getString(R.string.banner_ad_unit_id)

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
//        MobileAds.initialize(this, getString(R.string.banner_ad_unit_id));
//        val adRequest = AdRequest.Builder().build()
//        adViewBanner.loadAd(adRequest)
        /* .addTestDevice("0FB9D45B47B4FF37385B5DB7310D5B43")
        *
        * .addTestDevice(
            AdRequest.DEVICE_ID_EMULATOR)*/

        /*adViewBanner.adListener = object : AdListener() {
            override fun onAdLoaded() {}

            override fun onAdClosed() {
//                Toast.makeText(applicationContext, "Ad is closed!", Toast.LENGTH_SHORT).show()
            }

            override fun onAdFailedToLoad(errorCode: Int) {
//                Toast.makeText(applicationContext, "Ad failed to load! error code: $errorCode", Toast.LENGTH_SHORT)
//                    .show()
            }

            override fun onAdLeftApplication() {
//                Toast.makeText(applicationContext, "Ad left application!", Toast.LENGTH_SHORT).show()
            }
        }*/


        fabNewsDetailSearch.setOnClickListener {
            Log.e("topview curntPos ", " :: "+swipeStackLoader!!.currentPosition)

            val currentPos = swipeStackLoader!!.currentPosition
            val articleModel = articleList!![currentPos]

            val intent =  Intent(this@SearchActivity, NewsDetailActivity::class.java)
            intent.putExtra("url", articleModel.url)
            intent.putExtra("title", articleModel.title)
            intent.putExtra("news_img", articleModel.urlToImage)
            intent.putExtra("date", articleModel.publishedAt)
            intent.putExtra("source_name",articleModel.source!!.name)
            intent.putExtra("author",articleModel.author)
            startActivity(intent)
        }
    }



    private fun addFavouritesDataSource(position: Int){

        val articleModel = articleList!![position]
        var result = false
        var isDuplicate = false

//        btnSparkAnimate!!.visibility = View.GONE
//        fabSwipeRight!!.setImageResource(R.drawable.ic_favorite_border)

        val selectQuery = "SELECT  * FROM " + Constant.TABLE_NAME
        val db = dbHelper.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        Log.e("total count :: ", "${cursor.count}")

        if (cursor.count > 0) {
            if (cursor.moveToFirst()) {
                outer@do {
                    if ((cursor.getString(cursor.getColumnIndex(Constant.NEWS_TITLE))).equals(articleModel.title, true)) {
                        isDuplicate = true
                        break@outer
                    } else {
                        Log.e("cursor comp. :: ", "data is not inserted Successfully !!")
                    }
                } while (cursor.moveToNext())

                if (!isDuplicate) {

                    result = dbHelper.insertFavouriteNewsData(articleModel.title?:"", articleModel.description?:"",
                        articleModel.url?:"", articleModel.urlToImage?:"",
                        articleModel.source!!.name?:"", articleModel.author?:"",
                        articleModel.publishedAt?:"", articleModel.source!!.id?:"")

                    val userFavData : HashMap<String, String> = HashMap()
                    userFavData.set("Title", articleModel.title?:"")
                    userFavData.set("Desc", articleModel.description?:"")
                    userFavData.set("Url", articleModel.url?:"")
                    userFavData.set("UrlToImage", articleModel.urlToImage?:"")
                    userFavData.set("Author", articleModel.author?:"")
                    userFavData.set("PublishedAt", articleModel.publishedAt?:"")
                    userFavData.set("SourceName", articleModel.source!!.name?:"")
                    userFavData.set("SourceId", articleModel.source!!.id?:"")

                    rlFavouriteFab.visibility = View.VISIBLE
                    tvCountFavNews!!.visibility = View.VISIBLE
                    btnSparkAnimate.visibility = View.VISIBLE

                    btnSparkAnimate.playAnimation()
                    btnSparkAnimate.setAnimationSpeed(1.5f)
//        btnSparkAnimate!!.pressOnTouch(false)

                    if (btnSparkAnimate.isChecked) {
                        Log.e("btnSpark :: ", "" + btnSparkAnimate.isChecked)
                        btnSparkAnimate.setActiveImage(R.drawable.ic_favorite_border)
                        btnSparkAnimate.setInactiveImage(R.drawable.ic_favorite)
                        btnSparkAnimate.isChecked = false
                    } else {
                        Log.e("btnSpark :: ", "" + btnSparkAnimate.isChecked)
                        btnSparkAnimate.isChecked = true
                        btnSparkAnimate.setActiveImage(R.drawable.ic_favorite)
                        btnSparkAnimate.setInactiveImage(R.drawable.ic_favorite_border)
                    }

                    tvCountFavNews!!.visibility = View.GONE
                    btnSparkAnimate.visibility = View.GONE


                    Handler().postDelayed({
                        rlFavouriteFab.visibility = View.GONE

                    }, 2000)

                    dbReference!!.child(user!!.uid).push().setValue(userFavData).addOnCompleteListener { task ->
                        if(task.isSuccessful){
                            Log.e("addValueDbError ", " :: task is complete")
                        }
                    }
                    Log.e("cursor count :: ", "" + (cursor.count + 1))
                    tvCountFavNews!!.visibility = View.VISIBLE
                    tvCountFavNews!!.text = (cursor.count + 1).toString()

                    Log.e("cursor count :: ", "" + (cursor.count + 1))
                    Snackbar.make(findViewById(R.id.rlAddNewsSearch), "Add News in Favourites :: " + (cursor.count + 1), Snackbar.LENGTH_SHORT).show()
                } else {
                    Snackbar.make(findViewById(R.id.rlAddNewsSearch), "News is already available in Favourites !!", Snackbar.LENGTH_SHORT).show()
                }
            }
        } else {
            tvCountFavNews!!.visibility = View.VISIBLE
            tvCountFavNews!!.text = (cursor.count + 1).toString()
            result = dbHelper.insertFavouriteNewsData(articleModel.title?:"", articleModel.description?:"",
                articleModel.url?:"", articleModel.urlToImage?:"",
                articleModel.source!!.name?:"", articleModel.author?:"",
                articleModel.publishedAt?:"", articleModel.source!!.id?:"")

            Log.e("cursor count :: ", "" + (cursor.count + 1))
            Snackbar.make(findViewById(R.id.rlAddNewsSearch), resources.getString(R.string.addNewsInFav)+" :: " + (cursor.count + 1), Snackbar.LENGTH_SHORT).show()
        }

        if (result) {
            Log.e("result :: ", "data is inserted Successfully !!")
        } else {
            Log.e("result :: ", "data is not inserted !!")
        }

        cursor.close()
    }

    public override fun onPause() {
//        adViewBanner.pause()
        super.onPause()
    }

    private fun showInterstitialAd() {
       /* if (mInterstitialAd!!.isLoaded) {
            mInterstitialAd!!.show()
        }*/
    }

    public override fun onResume() {
        super.onResume()
//        adViewBanner.resume()
    }

    public override fun onDestroy() {
//        adViewBanner.destroy()
        super.onDestroy()
    }

    private fun searchConstantValueList(){
        val itemNames = resources.getStringArray(R.array.trending_search_value)
        val searchList: MutableList<String>?= ArrayList()

        for (i in 0 until itemNames.size) {
            searchList?.add(itemNames[i])
        }
        searchBaseAdapter = SearchBaseAdapter(this@SearchActivity, searchList)
        lvSearchQuery.adapter = searchBaseAdapter
    }

    private fun checkPermission() : Boolean{
        val result = ContextCompat.checkSelfPermission(this@SearchActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() =
        if(ActivityCompat.shouldShowRequestPermissionRationale(this@SearchActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            ActivityCompat.requestPermissions(this@SearchActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                Constant.PERMISSION_REQUEST_CODE
            )
        }else {
            ActivityCompat.requestPermissions(this@SearchActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                Constant.PERMISSION_REQUEST_CODE
            )
        }

    fun takeScreenshot(view : View): Bitmap {
        view.isDrawingCacheEnabled = true
        return Bitmap.createBitmap(view.drawingCache)
    }

    private var imagePath: String? = null
    private var imageFile: File?=null

    fun saveBitmap(bitmap: Bitmap) {

        val now = Date()
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now)
        imagePath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg"

        Log.e("image path ", " :: $imagePath")
        imageFile = File(imagePath)
        val fos: FileOutputStream
        try {
            fos = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            fos.flush()
            fos.close()
        } catch (e: FileNotFoundException) {
            Log.e("GREC", e.message, e)
        } catch (e: IOException) {
            Log.e("GREC", e.message, e)
        }
    }

    private fun shareIt(imageFile: File) {

        Log.e("image File ", " :: $imageFile")
        val uri = Uri.fromFile(imageFile)
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "image/*"
//        val shareBody = "In Tweecher, My highest score with screen shot"
//        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "My Tweecher score")
//        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri)

        try {
            startActivity(Intent.createChooser(intent, "Share Screenshot via "))
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this@SearchActivity, "No App Available", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode == Constant.PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Bitmap bitmap = takeScreenshot();
//                saveBitmap(bitmap);
//                shareIt();
            } else {
                Log.e("value", "Permission Denied, You cannot use Storage Local Drive. ");
            }
        }
    }

    private fun initViews() {

        errorLayout = findViewById(R.id.errorLayout)
        errorImageView = findViewById(R.id.errorImage)
        errorTitle = findViewById(R.id.errorTitle)
        errorMessage = findViewById(R.id.errorMessage)
        retryButton = findViewById(R.id.btnRetry)

        etSearchQuery = findViewById(R.id.etSearchQuery)
        imgBackArrow = findViewById(R.id.imgBackArrow)
        imgOverflowDot = findViewById(R.id.imgOverflowDot)
        swipeStackLoader = findViewById(R.id.swipeStackLoader)
        lvSearchQuery = findViewById(R.id.lvSearchQuery)
        imgClose = findViewById(R.id.imgClose)

        relLayoutSearchConstant = findViewById(R.id.relLayoutSearchConstant)
        tvSearchTextValue = findViewById(R.id.tvSearchTextValue)

        rlSearchLoadMoreContent = findViewById(R.id.rlSearchLoadMoreContent)
        btnSearchLoadMore = findViewById(R.id.btnSearchLoadMore)
//        adViewBanner = findViewById(R.id.adViewBanner)
        fabNewsDetailSearch = findViewById(R.id.fabNewsDetailSearch)

        //Share View News initialization here.....
        title = findViewById(R.id.titleShare)
        desc = findViewById(R.id.tvDescptShare)
        source = findViewById(R.id.sourceShare)
        author = findViewById(R.id.authorTV)
        publishedAt = findViewById(R.id.publishedAt)
        time = findViewById(R.id.timeShare)
        urlLink = findViewById(R.id.urlLinkShare)

        newsImgView = findViewById(R.id.newsImgViewShare)
        mProgBar = findViewById(R.id.progress_photo_load)
        rlScreenshotShare = findViewById(R.id.rlScreenshotShare)
        tvShareScreenShot = findViewById(R.id.tvShareScreenShot)
        tvShareScreenCancel = findViewById(R.id.tvShareScreenCancel)

        //Add News Item View initialize here....
        btnSparkAnimate = findViewById(R.id.btnSparkAnimateSearch)
        tvCountFavNews = findViewById(R.id.tvCountFavNewsSearch)
        rlFavouriteFab = findViewById(R.id.rlFavouriteFabSearch)
        fabAddNewsItem = findViewById(R.id.fabAddNewsSearch)
    }

    override fun onClick(v: View) {
     when(v.id){

         R.id.imgBackArrow -> {
             startActivity(Intent(this@SearchActivity, MainActivity::class.java))
             finish()
         }
         R.id.imgOverflowDot -> {
             val bundle = Bundle()
             val bottomSheetDialogFragment = SettingBottomSheetFragment()
             bundle.putString("queryValue", etSearchQuery?.text.toString())
             bottomSheetDialogFragment.arguments = bundle
             bottomSheetDialogFragment.show(this@SearchActivity.supportFragmentManager,
                     "Bottom Sheet Dialog Fragment !!")
         }

         R.id.imgClose -> {
             etSearchQuery!!.setText("")
         }

         R.id.btnSearchLoadMore -> {
             pageNo++
             loadJsonData(etSearchQuery!!.text.toString(), sharedPrefs!!.selectDateFrom!!,
                     sharedPrefs!!.selectDateTo!!,pageNo, sharedPrefs!!.newsSortsBy!!)

             rlSearchLoadMoreContent.visibility = View.GONE
         }
     }
    }

    override fun onBackPressed() {
        if(rlScreenshotShare.visibility == View.VISIBLE){
            rlScreenshotShare.visibility = View.GONE
        }else{
            super.onBackPressed()
        }
    }
    private fun onShowLoading() {
        findViewById<View>(R.id.shimmerCardLoading).visibility = View.VISIBLE
    }

    private fun onHideLoading() {
        findViewById<View>(R.id.shimmerCardLoading).visibility = View.GONE
    }

    private fun showErrorMessage(ImgId: Int, title: String, msg: String) {

        Log.e("showError :: ", "$ImgId :: $title :: $msg")
        if (errorLayout!!.visibility == View.GONE) {
            errorLayout!!.visibility = View.VISIBLE
        }

        errorImageView!!.setImageResource(ImgId)
        errorTitle!!.text = title
        errorMessage!!.text = msg

        retryButton.setOnClickListener { loadJsonData(etSearchQuery!!.text.toString(), sharedPrefs!!.selectDateFrom!!,
                sharedPrefs!!.selectDateTo!!,pageNo, sharedPrefs!!.newsSortsBy!!) }
    }

    fun loadJsonData(keyword: String, dateFrom:String, dateTo:String,pageNo:Int, sortBy:String) {

        onShowLoading()
        if (!articleList!!.isEmpty()) {
            articleList = ArrayList()
            swipeStackLoader!!.resetStack()
        }

        //        errorLayout.setVisibility(View.GONE);
        Log.e("adapter size :: ", "" + articleList!!.size)
        val apiInterface = ApiClient.apiClient.create<ApiInterface>(ApiInterface::class.java)

        val utils = Utils(this@SearchActivity)
        val country = utils.country
        val language = utils.language
        Log.e("lang :: ", language)

        //        This call is used for calling RESTFUL API HERE.....
       /* val call: Call<News>
        call = apiInterface.getNewsSearchByQuery(keyword,dateFrom,dateTo,sharedPrefs!!.langCode!!,
                sortBy,pageNo,10, Constant.API_KEY)*/

                    /*sortBy
            The order to sort the articles in. Possible options: relevancy, popularity, publishedAt.
            relevancy = articles more closely related to q come first.
            popularity = articles from popular sources and publishers come first.
            publishedAt = newest articles come first.
            Default: publishedAt*/

//        Log.e("call :: ", "" + call)
        Log.e("country :: ", "" + country)
//        Log.e("api_key :: ", "" + Constant.API_KEY)

        /*call.enqueue(object : Callback<News> {
            override fun onResponse(call: Call<News>, response: Response<News>) {

//                swipeStackLoader!!.resetStack()
                Log.e("response :: ", "" + response)
                Log.e("totolResult :: ", "" + response.body()?.totalResults)
                if (response.isSuccessful && response.body()!!.articleList != null) {

                    onHideLoading()
                    if (!articleList!!.isEmpty()) {
                        articleList = ArrayList()
                    }

                    articleList = response.body()!!.articleList
                    swipeStackAdapter = NewsSwipeStackAdapter(this@SearchActivity, articleList!!, "SearchActivity")
                    swipeStackLoader!!.adapter = swipeStackAdapter
                    swipeStackAdapter!!.notifyDataSetChanged()
                    fabNewsDetailSearch.show()

                    // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
//                    MobileAds.initialize(this, getString(R.string.banner_ad_unit_id));
//                    val adRequest = AdRequest.Builder().build()
//                    adViewBanner.loadAd(adRequest)
                    swipeStackAdapter!!.setShareNewsListener(this@SearchActivity)
                    rlSearchLoadMoreContent.visibility = View.GONE
                } else {
                    val errorCode: String
                    Log.e("Error Code :: ", "" + response.code())
                    when (response.code()) {
                        404 -> {
                            errorCode = "404 not found"
                        }
                        500 -> {
                            errorCode = "500 server broken"
                        }
                        else -> errorCode = "Unknown error"
                    }
                    showErrorMessage(R.drawable.no_result, "No Result", "Please Try Again \n"+errorCode)
                    fabNewsDetailSearch.hide()
                    rlSearchLoadMoreContent.visibility = View.GONE
                }
            }

            override fun onFailure(call: Call<News>, t: Throwable) {
                Log.e("Error :: ", "" + t.toString())
                onHideLoading()
                showErrorMessage(R.drawable.oops, "Oops...", "Network Failure, \nPlease Try Again ")
                fabNewsDetailSearch.hide()
                rlSearchLoadMoreContent.visibility = View.GONE
            }
        })*/
    }
}
