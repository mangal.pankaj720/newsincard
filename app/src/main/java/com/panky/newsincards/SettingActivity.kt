package com.panky.newsincards

import android.app.AlertDialog
import android.app.TaskStackBuilder
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.annotation.StyleRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.*

import com.panky.newsincards.SharedPreference.SharedPrefs

import java.io.File
import java.math.RoundingMode
import java.text.DecimalFormat
import android.util.Base64.NO_WRAP
import com.panky.newsincards.view.MainPage.view.MainActivity


class SettingActivity : AppCompatActivity(), View.OnClickListener {

    private val TAG = MainActivity::class.java.getSimpleName()
    private lateinit var settingToolbar: Toolbar
    private lateinit var switchMode: Switch
    private lateinit var settingLLayout: RelativeLayout
    private lateinit var countryLLayout: LinearLayout
    private lateinit var languageLLayout:LinearLayout
    private lateinit var privacyPolicyLLayout:LinearLayout
    private var sharedPrefs: SharedPrefs? = null
    private var buttonToggle: Button? = null
    private lateinit var tvCacheSize:TextView
    private lateinit var tvClearCache:TextView
    private lateinit var contactLLayout : LinearLayout
    private lateinit var tvRateMeLink:TextView
    private lateinit var tvShowLang:TextView
    private lateinit var tvCountryName:TextView

    //Font size views initialize here...
    private lateinit var tvSmallFont:TextView
    private lateinit var tvMediumFont:TextView
    private lateinit var tvLargeFont:TextView

    override fun onCreate(savedInstanceState: Bundle?) {

        sharedPrefs = SharedPrefs(this@SettingActivity)
        if (sharedPrefs!!.isNightModeEnabled) {
            setAppTheme(R.style.AppTheme_Base_Night)
        } else {
            setAppTheme(R.style.AppTheme_Base_Light)
        }

        try {
            // Get the font size value from SharedPreferences.
            val settings = getSharedPreferences("com.example.YourAppPackage", Context.MODE_PRIVATE)

            // Get the font size option.  We use "FONT_SIZE" as the key.
            // Make sure to use this key when you set the value in SharedPreferences.
            // We specify "Medium" as the default value, if it does not exist.
            val fontSizePref = settings.getString("FONT_SIZE", "Medium")

            // Select the proper theme ID.
            // These will correspond to your theme names as defined in themes.xml.
            var themeID = R.style.FontSizeMedium
            if (fontSizePref === "Small") {
                themeID = R.style.FontSizeSmall
            } else if (fontSizePref === "Large") {
                themeID = R.style.FontSizeLarge
            }

            // Set the theme for the activity.
            setTheme(themeID)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }


        Log.e(TAG + " :: ", "Main Setting onCreate called !!")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        initViews()

        // GOOGLE PLAY APP SIGNING SHA-1 KEY:- 1A:02:95:54:B2:16:95:3E:E3:37:DA:B5:91:EA:2A:30:69:3F:30:AB
        val sha1 = byteArrayOf(
            0x1A,
            0x02,
            0x95.toByte(),
            0x54,
            0xB2.toByte(),
            0x16,
            0x95.toByte(),
            0x3E,
            0xE3.toByte(),
            0x37,
            0xDA.toByte(),
            0xB5.toByte(),
            0x91.toByte(),
            0xEA.toByte(),
            0x2A,
            0x30,
            0x69,
            0x3F,
            0x30,
            0xAB.toByte()
        )
        Log.e("keyhashGoogSignIn : ", " :: "+ Base64.encodeToString(sha1, NO_WRAP))

        setSupportActionBar(settingToolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setTitle(R.string.settingAct)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        tvShowLang.text = sharedPrefs!!.langNameSetup
        tvCountryName.text = sharedPrefs!!.countryName

        // Get state from preferences
        switchMode.isChecked = sharedPrefs!!.isNightModeEnabled

        switchMode.setOnCheckedChangeListener { _, b ->
            if (sharedPrefs!!.isNightModeEnabled) {
                sharedPrefs!!.isNightModeEnabled = false
                setAppTheme(R.style.AppTheme_Base_Light)
//                switchMode!!.setText(R.string.night_mode)
                TaskStackBuilder.create(applicationContext)
                        .addNextIntent(Intent(applicationContext, MainActivity::class.java))
                        .addNextIntent(intent)
                        .startActivities()
            } else {
                sharedPrefs!!.isNightModeEnabled = true
                setAppTheme(R.style.AppTheme_Base_Night)
//                switchMode!!.setText(R.string.day_mode)
                TaskStackBuilder.create(applicationContext)
                        .addNextIntent(Intent(applicationContext, MainActivity::class.java))
                        .addNextIntent(intent)
                        .startActivities()
            }
        }

        val value = initializeCache()
        tvCacheSize.text = value
        tvClearCache.setOnClickListener { showCacheAlertDialog(resources.getString(R.string.clearCacheText)) }
        countryLLayout.setOnClickListener(this)
        languageLLayout.setOnClickListener(this)
        contactLLayout.setOnClickListener(this)
        tvRateMeLink.setOnClickListener(this)
        privacyPolicyLLayout.setOnClickListener(this)
        tvSmallFont.setOnClickListener(this)
        tvLargeFont.setOnClickListener(this)
        tvMediumFont.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){

            R.id.privacyPolicyLLayout ->{
                val intent = Intent(this@SettingActivity, PrivacyPolicyPage::class.java)
//                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }

            R.id.languageLLayout -> {
                val intentLang = Intent(this@SettingActivity, LanguagePage::class.java)
                intentLang.putExtra("getType","changeSetting")
                startActivity(intentLang)
            }

            R.id.countryLLayout -> {
                val intentCountry = Intent(this@SettingActivity, ChangeCountryPage::class.java)
                intentCountry.putExtra("fromClass", "SettingPage")
                startActivity(intentCountry)
            }

            R.id.tvSmallFont -> {
                tvSmallFont.setTextColor(resources.getColor(R.color.colorWhite))
                tvSmallFont.setBackgroundResource(R.drawable.small_font_draw)

                tvLargeFont.setTextColor(resources.getColor(R.color.colorBlack))
                tvLargeFont.setBackgroundResource(R.drawable.large_font_empty)
                tvMediumFont.setTextColor(resources.getColor(R.color.colorBlack))
                tvMediumFont.setBackgroundResource(R.drawable.medium_font_empty)
            }

            R.id.tvLargeFont -> {
                tvSmallFont.setTextColor(resources.getColor(R.color.colorBlack))
                tvSmallFont.setBackgroundResource(R.drawable.small_font_empty)

                tvLargeFont.setTextColor(resources.getColor(R.color.colorWhite))
                tvLargeFont.setBackgroundResource(R.drawable.large_font_draw)
                tvMediumFont.setTextColor(resources.getColor(R.color.colorBlack))
                tvMediumFont.setBackgroundResource(R.drawable.medium_font_empty)

            }

            R.id.tvMediumFont -> {

                tvSmallFont.setTextColor(resources.getColor(R.color.colorBlack))
                tvSmallFont.setBackgroundResource(R.drawable.small_font_empty)

                tvLargeFont.setTextColor(resources.getColor(R.color.colorBlack))
                tvLargeFont.setBackgroundResource(R.drawable.large_font_empty)
                tvMediumFont.setTextColor(resources.getColor(R.color.colorWhite))
                tvMediumFont.setBackgroundResource(R.drawable.medium_font_draw)
            }
            R.id.contactLLayout -> {
                sendEmailFeedback()
            }

            R.id.tvRateMeLink ->{
                val uri: Uri = Uri.parse("market://details?id=" + packageName);
                val goToMarket: Intent = Intent(Intent.ACTION_VIEW, uri)
                goToMarket.addFlags(
                    Intent.FLAG_ACTIVITY_NO_HISTORY or
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK
                )
                try {
                    startActivity(goToMarket)
                } catch (e: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())
                        )
                    )
                }
            }
        }
    }

    private fun sendEmailFeedback(){

        val emailIntent = Intent(
            Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "kumar.tony720640@gmail.com", null
            )
        )
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "News Pro App Feedback")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Please describe your any issue/suggestions related to News In Card App !!")
        startActivity(Intent.createChooser(emailIntent, "Send Feedback..."))
    }

    private fun showCacheAlertDialog(msg: String) {
        val descAlert = AlertDialog.Builder(this@SettingActivity)
        descAlert.setTitle("Clear Cache")
            .setMessage(msg)
            .setCancelable(true)
            .setPositiveButton("Clear") { _, _ ->
                try {
                    trimCache(this)
                    TaskStackBuilder.create(applicationContext)
                        .addNextIntent(Intent(applicationContext, MainActivity::class.java))
                        .addNextIntent(intent)
                        .startActivities()
                    // Toast.makeText(this,"onDestroy " ,Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            .setNegativeButton("Cancel"){ dialog, _ ->  dialog.dismiss()}

        descAlert.show()
    }

    private fun initializeCache(): String {
        var size: Long = 0
        size += getDirSize(this.cacheDir)
//        size += getDirSize(this.externalCacheDir)

        Log.e("size of cache :: ", ""+size)
        val value = calculateCacheSize(size).toString()
        return value
    }

    fun calculateCacheSize(size : Long) : Double{
        val mbSize:Double = (size * 0.000008)

        val df = DecimalFormat("#0.00")
        df.roundingMode = RoundingMode.CEILING

        val valueInString = df.format(mbSize).replace(",",".")
        Log.e("valueInString :: ", ""+valueInString)
        return valueInString.toDouble()
    }

    fun getDirSize(dir: File?): Long {
        var size: Long = 0
        for (file in dir!!.listFiles()) {
            if (file != null && file.isDirectory()) {
                size += getDirSize(file)
            } else if (file != null && file.isFile()) {
                size += file.length()
            }
        }
        return size
    }

    fun trimCache(context: Context) {
        try {
            val dir = context.getCacheDir()
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun deleteDir(dir: File?): Boolean {
        if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (i in children.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
        }
        // The directory is now empty so delete it
        return dir!!.delete()
    }

    private fun initViews() {
        settingToolbar = findViewById(R.id.settingToolbar)
        switchMode = findViewById(R.id.switchMode)
        settingLLayout = findViewById(R.id.settingLLayout)
        countryLLayout = findViewById(R.id.countryLLayout)
        buttonToggle = findViewById(R.id.button_toggle)
        tvCacheSize = findViewById(R.id.tvCacheSize)
        tvClearCache = findViewById(R.id.tvClearCache)
        contactLLayout = findViewById(R.id.contactLLayout)

        tvRateMeLink = findViewById(R.id.tvRateMeLink)
        languageLLayout = findViewById(R.id.languageLLayout)
        tvShowLang = findViewById(R.id.tvShowLang)
        tvCountryName = findViewById(R.id.tvCountryName)
        privacyPolicyLLayout = findViewById(R.id.privacyPolicyLLayout)

        tvSmallFont = findViewById(R.id.tvSmallFont)
        tvMediumFont = findViewById(R.id.tvMediumFont)
        tvLargeFont = findViewById(R.id.tvLargeFont)

    }

    private fun restartApp() {
        startActivity(Intent(this@SettingActivity, MainActivity::class.java))
        finish()
    }

    private fun setAppTheme(@StyleRes style: Int) {
        setTheme(style)
    }

    override fun onStart() {
        super.onStart()
        Log.e("$TAG :: ", "Main Setting onStart called !!")
    }

    override fun onResume() {
        super.onResume()
        Log.e("$TAG :: ", "Main Setting onResume called !!")
    }

    override fun onPause() {
        super.onPause()
        Log.e("$TAG :: ", "Main Setting onPause called !!")
    }

    override fun onStop() {
        super.onStop()
        Log.e("$TAG :: ", "Main Setting onStop called !!")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("$TAG :: ", "Main Setting onDestroy called !!")
    }

    override fun onRestart() {
        super.onRestart()
        Log.e("$TAG :: ", "Main Setting onRestart called !!")
    }
}
