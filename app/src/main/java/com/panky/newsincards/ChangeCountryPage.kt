package com.panky.newsincards

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.google.firebase.database.*
import com.panky.newsincards.Adapter.CountryWithLangRVAdapter
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.models.CountryLangModel
import com.panky.newsincards.view.MainPage.view.MainActivity

class ChangeCountryPage : AppCompatActivity(), CountryWithLangRVAdapter.countrySelectedListener {

    private val TAG : String = ChangeCountryPage::class.java.simpleName
    private var layoutMangager: androidx.recyclerview.widget.RecyclerView.LayoutManager? = null
    private lateinit var rvCountryChange: androidx.recyclerview.widget.RecyclerView
    private var countryLangModel : ArrayList<CountryLangModel>? = null
    private lateinit var pbLoadingCountryData : ProgressBar
    private lateinit var imgBackCountry:ImageView
    private lateinit var tvCountrySelect:TextView
    private lateinit var toolbarChangeCountry:Toolbar
    private lateinit var llFinishCountrySetup:LinearLayout

    private lateinit var dbReferenceCountry: DatabaseReference
    private lateinit var countryWithLandRVAdapter: CountryWithLangRVAdapter

    private lateinit var sharedPrefs: SharedPrefs
    private var langValue:String? = null
    private var langName:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        sharedPrefs = SharedPrefs(this@ChangeCountryPage)

        Log.e("$TAG :: ", "onCreate Called")
        if (sharedPrefs.isNightModeEnabled) {
            setTheme(R.style.AppTheme_Base_Night)
        } else {
            setTheme(R.style.AppTheme_Base_Light)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_country_page)
        initViews()
        setUpRecyclerView()

        setSupportActionBar(toolbarChangeCountry)

        sharedPrefs = SharedPrefs(this@ChangeCountryPage)
        countryLangModel = ArrayList()
        dbReferenceCountry = FirebaseDatabase.getInstance().reference.child("CountryData")

        //Get Intent value here....
        val intent = intent
        val getReference = intent.getStringExtra("fromClass")

        langName = sharedPrefs.langNameSetup
        tvCountrySelect.text = sharedPrefs.langNameSetup
        getCountryDataWithLangCode(sharedPrefs.langCode)
        Log.e("code ", " :: "+sharedPrefs.langCode)
        imgBackCountry.setOnClickListener {

            if(getReference.equals("MainPage")){
                val intentCountry = Intent(this@ChangeCountryPage, MainActivity::class.java)
                intentCountry.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intentCountry)
                finish()
            }else {
                val intentCountry = Intent(this@ChangeCountryPage, SettingActivity::class.java)
                intentCountry.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intentCountry)
                finish()
            }
        }

        llFinishCountrySetup.setOnClickListener {
            if(getReference.equals("MainPage")){
                val intentCountry = Intent(this@ChangeCountryPage, MainActivity::class.java)
                intentCountry.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK

                recreate()
                startActivity(intentCountry)
                finish()
            }else {
                val intentCountry = Intent(this@ChangeCountryPage, SettingActivity::class.java)
                intentCountry.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK

                recreate()
                startActivity(intentCountry)
                finish()
            }
        }
    }

    private fun getCountryDataWithLangCode(langValue: String?) {
        dbReferenceCountry.child(langValue?:"").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (postSnapshot in dataSnapshot.children) {

                    pbLoadingCountryData.visibility = View.GONE
                    val countryLangModels:CountryLangModel = postSnapshot.getValue<CountryLangModel>(CountryLangModel::class.java)!!
                    countryLangModel?.add(countryLangModels)
                }

//                dataSnapshot.children.mapNotNullTo(countryLangModel) { it.getValue<CountryLangModel>(CountryLangModel::class.java) }
                countryWithLandRVAdapter = CountryWithLangRVAdapter(this@ChangeCountryPage, countryLangModel)
                rvCountryChange.adapter = countryWithLandRVAdapter
                countryWithLandRVAdapter.setSelectedViewListener(this@ChangeCountryPage)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                pbLoadingCountryData.visibility = View.GONE
                Log.e("db error :: ", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        })

    }

    override fun getPosition(position: Int) {
        val countryLang = countryLangModel!!.get(position)

        Log.e("country data ", ""+countryLang.CountryName+" :: "+countryLang.CountryCode)

        llFinishCountrySetup.visibility = View.VISIBLE
        sharedPrefs.langCountryCode = countryLang.CountryCode
//        sharedPrefs.langCode = langValue
        sharedPrefs.countryCode = countryLang.CountryCode
        sharedPrefs.countryName = countryLang.CountryName

        tvCountrySelect.text = ""
        tvCountrySelect.text = String.format(langName + " ("+ countryLang.CountryName+")")
    }

    private fun initViews(){
        toolbarChangeCountry = findViewById(R.id.toolbarChangeCountry)
        rvCountryChange = findViewById(R.id.rvCountryChange)
        pbLoadingCountryData = findViewById(R.id.pbLoadingCountryData)
        imgBackCountry = findViewById(R.id.imgBackCountry)
        tvCountrySelect = findViewById(R.id.tvCountrySelect)
        llFinishCountrySetup = findViewById(R.id.llFinishCountrySetup)
    }

    private fun setUpRecyclerView(){
        layoutMangager = androidx.recyclerview.widget.GridLayoutManager(
            this@ChangeCountryPage,
            2
        )
        rvCountryChange.layoutManager = layoutMangager
        rvCountryChange.setHasFixedSize(true)
        rvCountryChange.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
    }
}
