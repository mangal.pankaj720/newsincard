package com.panky.newsincards

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.github.ybq.android.spinkit.style.Circle
import com.google.firebase.auth.FirebaseAuth
import com.panky.newsincards.Constants.Constants
import com.panky.newsincards.SharedPreference.SharedPrefs
import com.panky.newsincards.models.UserData
import com.panky.newsincards.view.MainPage.view.MainActivity
import java.util.*

class FinishUpDialogFrag : androidx.fragment.app.DialogFragment() {

    internal lateinit var view : View
    private lateinit var sharedPrefs:SharedPrefs
    private lateinit var pbFinshUpLoaders:ProgressBar
    private var mAuth: FirebaseAuth? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        view = inflater.inflate(R.layout.fragment_finish_up_dialog, container, false)
        sharedPrefs = SharedPrefs(context!!)
        initViews()
        mAuth = FirebaseAuth.getInstance()

        val rotatingPlane = Circle()
        pbFinshUpLoaders.indeterminateDrawable = rotatingPlane
        updateResources(context!!, sharedPrefs.langCode!!)
        return view
    }

    private fun initViews(){
        pbFinshUpLoaders = view.findViewById(R.id.pbFinshUpLoaders)
    }

    private fun updateResources(context: Context, language: String) {
        val locale = Locale(language)
        Locale.setDefault(locale)

        val resources = context.resources
        val configuration = resources.configuration

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale)
        } else {
            configuration.locale = locale
        }

        Handler().postDelayed({
            resources.updateConfiguration(configuration, resources.displayMetrics)
            gotoMainClass()
        }, 3000)

    }

    private fun gotoMainClass(){

        val user = mAuth!!.currentUser
        Log.e("userData ", " :: "+user)
        sharedPrefs.categoryPos = 0

        if(user != null){
            val userData = UserData()
            userData.name = user.displayName
            userData.email = user.email
            userData.photoUrl = user.photoUrl.toString()
            userData.socialType = "google"
            Constants.userDataObject.userData = userData


            val intentLang = Intent(context, MainActivity::class.java)
            intentLang.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            intentLang.putExtra("countryCode", sharedPrefs.langCountryCode)
            intentLang.putExtra("langCode", sharedPrefs.langCode)
            startActivity(intentLang)
            activity!!.finish()
        }else {
            val intentLang = Intent(context, MainActivity::class.java)
            intentLang.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            intentLang.putExtra("countryCode", sharedPrefs.langCountryCode)
            intentLang.putExtra("langCode", sharedPrefs.langCode)
            startActivity(intentLang)
            activity!!.finish()
        }

    }
}
